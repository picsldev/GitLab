"""
VENTANA LOGIN DE FARMACIA
"""
import pymysql
import os
import sys
import re
import hashlib
# --------------------------------------
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
# --------------------------------------
import sistema
# --------------------------------------


class VLogin(QDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self, parent)

        self.setWindowTitle("Sistema de Entrada")
        self.setWindowIcon(QIcon("icon/Pumps-48.png"))
        self.setFixedSize(300, 300)
        self.move(QDesktopWidget().availableGeometry().center() - self.frameGeometry().center())

        labelFondo = QLabel(self)
        labelFondo.setPixmap(QPixmap("icon/wallpaper.jpg"))
        labelFondo.setGeometry(0, 0, 300, 300)

        labelIcon = QLabel(self)
        labelIcon.setPixmap(QPixmap("icon/enfermera.png"))
        labelIcon.setGeometry(100, 20, 100, 100)
        self.botones()
# ----------------------------------------------------------------------------

    def botones(self):
        label1 = QLabel("Usuario:", self)
        label1.move(130, 128)
        self.line1 = QLineEdit(self)
        self.line1.setAlignment(Qt.AlignCenter)
        self.line1.setGeometry(90, 143, 120, 20)
        self.line1.setStyleSheet("background-image: url(:/icon/enfer.png)")
        self.line1.textChanged.connect(self.ValidarUsuario)

        label2 = QLabel("Clave:", self)
        label2.move(134, 178)
        self.line2 = QLineEdit(self)
        self.line2.setAlignment(Qt.AlignCenter)
        self.line2.setEchoMode(QLineEdit.Password)
        self.line2.setGeometry(90, 193, 120, 20)
        self.line2.textChanged.connect(self.ValidarPassword)

        self.l = QLabel(self)
        self.l.setGeometry(30, 270, 250, 20)

        boton = QPushButton("ENTRAR", self)
        boton.move(113, 225,)
        boton.setIcon(QIcon(QPixmap("icon/Ok_48px.png")))
        boton.clicked.connect(self.Validar_Formularios)

# -------------------------------------------------------------------------

    def ValidarUsuario(self):
        validar = re.match('^[a-z[0-9\sáéíóúàèìòùäëïöüñ]+$', self.line1.text(), re.I)
        if self.line1.text() == "":
            self.line1.setStyleSheet("border: 2px solid yellow;")
            self.l.setText("")
            return False
        elif not validar:
            self.line1.setStyleSheet("border: 2px solid red;")
            self.l.setText("La casilla usuario, solo se permite letras y numeros")
            self.l.setStyleSheet("color: red")
            return False
        else:
            self.line1.setStyleSheet("border: 2px solid cyan;")
            self.l.setText("")
            return True

    def ValidarPassword(self):

        if self.line2.text() == "":
            self.line2.setStyleSheet("border: 3px solid yellow;")
            return False
        else:
            self.line2.setStyleSheet("border: 3px solid cyan;")
            return True

    def Validar_Formularios(self):
        if self.ValidarUsuario() and self.ValidarPassword():
            usuario = hashlib.sha512(self.line1.text().encode('utf-8')).hexdigest()
            password = hashlib.sha512(self.line2.text().encode('utf-8')).hexdigest()
            try:
                db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="admin")
                query = db.cursor()
                if (query.execute("SELECT * FROM Registros WHERE Usuario='" + usuario + "' AND Clave='" + password + "'")):
                    db.commit()
                    QMessageBox.information(self, 'Informacion', 'Bienvenido ' +
                                            self.line1.text(), QMessageBox.Ok)
                    self.Ven2()
                else:
                    QMessageBox.warning(
                        self, 'Alerta', 'Usuario y Clave son invalido o no existen.', QMessageBox.Ok)
                    self.line1.setText("")
                    self.line2.setText("")
                    self.line1.setStyleSheet("border: 2px solid red;")
                    self.line2.setStyleSheet("border: 2px solid red;")
            except:
                QMessageBox.critical(self, 'Fallo', 'Error al conectar con la SQL.', QMessageBox.Ok)
                self.line1.setText("")
                self.line2.setText("")

# ----------------------------------------------------------------------------
    def Ven2(self):
        self.close()
        V2 = sistema.Ventana(self)
        V2.show()


# ----------------------------------------------------------------------------
if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = VLogin()
    ex.show()
    sys.exit(app.exec_())

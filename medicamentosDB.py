"""
VENTANA DE PAGOS
"""
import os
import sys
import re
import pymysql
import random
import time
# --------------------------------------
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.pdfmetrics import registerFontFamily
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib import colors
# --------------------------------------
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtMultimedia import *
# --------------------------------------
import sistema

# ---------------------------------------------------------------------------------------


class ingresoDeMedicamentosDB(QDialog):
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.setWindowTitle("Almacenar")
        self.setWindowIcon(QIcon("icon/medicamentos.png"))
        self.setFixedSize(800, 500)
        self.move(QDesktopWidget().availableGeometry().center()
                  - self.frameGeometry().center())

        self.labelFondo = QLabel(self)
        self.labelFondo.setPixmap(QPixmap("icon/wallpaper.jpg"))
        self.labelFondo.setGeometry(0, 0, 800, 500)

        grop = QGroupBox("Inforamcion", self)
        grop.setGeometry(70, 10, 650, 80)

        self.la1 = QLabel("Codigo:", self)
        self.la1.setGeometry(90, 50, 120, 15)

        self.li1 = QLineEdit(self)
        self.li1.setAlignment(Qt.AlignCenter)
        self.li1.setPlaceholderText("Codigo de barra")
        self.li1.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li1.setGeometry(140, 50, 150, 20)

        self.la2 = QLabel("Nombre:", self)
        self.la2.setGeometry(300, 50, 120, 13)

        self.li2 = QLineEdit(self)
        self.li2.setAlignment(Qt.AlignCenter)
        self.li2.setPlaceholderText("Nombre del medicamento")
        self.li2.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li2.setGeometry(360, 50, 350, 20)
# ---------------------------------------------------------------------------------------
        grop = QGroupBox("Presio", self)
        grop.setGeometry(70, 90, 120, 145)

        self.la3 = QLabel("Uni:", self)
        self.la3.setGeometry(90, 120, 120, 15)

        self.li3 = QLineEdit(self)
        self.li3.setAlignment(Qt.AlignCenter)
        self.li3.setPlaceholderText("$ 0.0")
        self.li3.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li3.setGeometry(120, 118, 50, 20)

        self.la4 = QLabel("Tab:", self)
        self.la4.setGeometry(90, 140, 120, 15)

        self.li4 = QLineEdit(self)
        self.li4.setAlignment(Qt.AlignCenter)
        self.li4.setPlaceholderText("$ 0.0")
        self.li4.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li4.setGeometry(120, 138, 50, 20)

        self.la5 = QLabel("Caj:", self)
        self.la5.setGeometry(90, 160, 120, 15)

        self.li5 = QLineEdit(self)
        self.li5.setAlignment(Qt.AlignCenter)
        self.li5.setPlaceholderText("$ 0.0")
        self.li5.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li5.setGeometry(120, 158, 50, 20)

        self.la6 = QLabel("Bol:", self)
        self.la6.setGeometry(90, 180, 120, 15)

        self.li6 = QLineEdit(self)
        self.li6.setAlignment(Qt.AlignCenter)
        self.li6.setPlaceholderText("$ 0.0")
        self.li6.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li6.setGeometry(120, 178, 50, 20)

        self.la7 = QLabel("Fra:", self)
        self.la7.setGeometry(90, 200, 120, 15)

        self.li7 = QLineEdit(self)
        self.li7.setAlignment(Qt.AlignCenter)
        self.li7.setPlaceholderText("$ 0.0")
        self.li7.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li7.setGeometry(120, 198, 50, 20)
# ---------------------------------------------------------------------------------------
        grop = QGroupBox("Informe", self)
        grop.setGeometry(200, 90, 130, 150)

        self.la8 = QLabel("Medida:", self)
        self.la8.setGeometry(242, 120, 70, 15)

        self.comb1 = QComboBox(self)
        self.comb1.setGeometry(205, 140, 120, 20)
        self.comb1.addItems(("",
                             "æg/m3",
                             "æg",
                             "µg",
                             "µG",
                             "ui",
                             "ng",
                             "mmol",
                             "ml",
                             "millones de UI",
                             "millones",
                             "million unit",
                             "million IU",
                             "million CFU",
                             "miligramos",
                             "mg/parche",
                             "mg/ml",
                             "mg.",
                             "mg",
                             "mg (titer)",
                             "mcg/h",
                             "mcg.",
                             "mL.",
                             "mEq/ml",
                             "mEq",
                             "mCi",
                             "m3",
                             "m",
                             "log10 TCID50",
                             "gramos",
                             "g/l",
                             "g..",
                             "g",
                             "billion organisms",
                             "Ul",
                             "UT",
                             "USP",
                             "UI",
                             "UFC"
                             "U/ml",
                             "U.I.",
                             "U Ph.Eu",
                             "U",
                             "TCID50/dose",
                             "PFU",
                             "Mega UI",
                             "MU",
                             "MILLON  UI",
                             "MG",
                             "MCG",
                             "MBq/ml",
                             "MBq.",
                             "M.U.I.",
                             "LfU",
                             "Lf",
                             "IU (milllones)",
                             "m",
                             "IU",
                             "IR (U/Reactividad)",
                             "GBq/ml",
                             "GBq",
                             "G",
                             "ELISA unit/dose",
                             "ELISA unit",
                             "CCID50/dose",
                             "CCID50",
                             "AgU",
                             "-",
                             "% (W/W)",
                             "% (W/V)"
                             "% (V/V)",
                             "%"))

        self.la9 = QLabel("Concentracion:", self)
        self.la9.setGeometry(220, 170, 120, 15)

        self.comb2 = QComboBox(self)
        self.comb2.move(245, 190)
        self.comb2.addItems(("",
                             "A",
                             "B",
                             "C",
                             "D",
                             "E",))
# ---------------------------------------------------------------------------------------
        grop = QGroupBox(self)
        grop.setGeometry(340, 90, 380, 170)

        self.la10 = QLabel("Laboratorio:", self)
        self.la10.setGeometry(480, 120, 120, 15)

        self.li10 = QLineEdit(self)
        self.li10.setAlignment(Qt.AlignCenter)
        self.li10.setPlaceholderText("Nombre del laboratorio")
        self.li10.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li10.setGeometry(350, 140, 360, 20)

        self.la11 = QLabel("F.Expedicion:", self)
        self.la11.setGeometry(360, 170, 120, 15)

        self.li11 = QLineEdit(self)
        self.li11.setAlignment(Qt.AlignCenter)
        self.li11.setPlaceholderText("Año-Mes-Dia")
        self.li11.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li11.setGeometry(350, 190, 100, 20)

        self.la12 = QLabel("F.Vencimiento:", self)
        self.la12.setGeometry(465, 170, 120, 15)

        self.li12 = QLineEdit(self)
        self.li12.setAlignment(Qt.AlignCenter)
        self.li12.setPlaceholderText("Año-Mes-Dia")
        self.li12.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li12.setGeometry(460, 190, 100, 20)

        self.laLo = QLabel("Lote:", self)
        self.laLo.setGeometry(380, 213, 120, 15)

        self.liLo = QLineEdit(self)
        self.liLo.setAlignment(Qt.AlignCenter)
        self.liLo.setPlaceholderText("Codigo de Lote")
        self.liLo.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.liLo.setGeometry(350, 230, 100, 20)

        self.la13 = QLabel("Administracion:", self)
        self.la13.setGeometry(590, 170, 120, 15)

        self.comb3 = QComboBox(self)
        self.comb3.setGeometry(570, 190, 140, 20)
        self.comb3.addItems(("",
                             "BUCAL",
                             "CONJUNTIVAL",
                             "DENTAL",
                             "INHALACION",
                             "INTRAVENOSA",
                             "INFILTRATIVA - BLOQUEOS",
                             "INTRAOCULAR",
                             "INTRAMUSCULAR",
                             "INTRANASAL",
                             "INTRATECAL",
                             "OFTÁLMICA",
                             "ORAL",
                             "OTICO AURICULAR",
                             "PARENTERAL",
                             "RECTAL",
                             "SUBCUTANEA",
                             "TOPICA (EXTERNA)",
                             "VAGINAL",))
# ---------------------------------------------------------------------------------------
        grop = QGroupBox("Descripcion", self)
        grop.setGeometry(200, 260, 520, 200)

        self.la15 = QLabel("UTC:", self)
        self.la15.setGeometry(210, 300, 120, 15)

        self.li15 = QLineEdit(self)
        self.li15.setAlignment(Qt.AlignCenter)
        self.li15.setPlaceholderText("Descripcion del ATC")
        self.li15.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li15.setGeometry(250, 297, 400, 20)

        self.la16 = QLabel("Inforamcion del medicamento:", self)
        self.la16.setGeometry(210, 340, 187, 15)

        self.lista1 = QTextEdit(self)
        self.lista1.setGeometry(210, 340, 250, 100)
        self.lista1.setPlaceholderText("Descripcion del medicamento y sus funciones.")

        self.la16 = QLabel("Presentacion:", self)
        self.la16.setGeometry(550, 320, 180, 15)

        self.comb4 = QComboBox(self)
        self.comb4.setGeometry(470, 340, 240, 20)
        self.comb4.addItems((" ",

                             "AEROSOLES",
                             "CAPSULA BLANDA",
                             "CAPSULA CON CUBIERTA ENTERICA",
                             "CAPSULA DE LIBERACION PROLONGADA",
                             "CAPSULA DURA",
                             "CAPSULAS DURAS Y BLANDAS",
                             "CAPSULA VAGINAL",
                             "COMPRIMIDO",
                             "CREMAS, GELES, UNGUENTOS Y PASTAS",
                             "CREMA TOPICA",
                             "CREMA VAGINAL",
                             "CÁPSULA BLANDA DE GELATINA CON CUBIERTA ENTÉRICA",
                             "CÁPSULA BLANDATWISTOFF",
                             "ELIXIR",
                             "EMULSION (CHAMPOO)",
                             "EMULSION INYECTABLE",
                             "EMULSION ORAL",
                             "EMULSIÓN",
                             "ESPONJA MEDICADA",
                             "GAS",
                             "GEL ESTERILINTRA OCULAR",
                             "GEL ORAL",
                             "GEL TOPICO",
                             "GEL VAGINAL",
                             "GRANULOS",
                             "GRANULOS EFERVESCENTES",
                             "IMPLANTE",
                             "IMPLANTES Y SISTEMAS INTRAUTERINOS E INTRAOCULARES",
                             "INYECTABLES",
                             "JABON LIQUIDO",
                             "JALEA",
                             "JARABE",
                             "LAMINAS DISPERSABLES",
                             "LIOFILIZADO ORAL DE DISPERSION RAPIDA",
                             "LIQUIDO ESTERIL PARA INYECCION (AGUA)",
                             "LIQUIDOOLEOSO (Aceite)",
                             "LOCION",
                             "MEDICAMENTOS NO ESTERILES SOLIDOS - POLVOS Y GRANULADOS",
                             "ORAL",
                             "OVULO",
                             "PASTA",
                             "POLVO",
                             "POLVO ESTERIL PARA RECONSTITUIR A SOLUCION INYECTABLE",
                             "POLVO LIOFILIZADO",
                             "POLVO LIOFILIZADO PARA RECONSTITUIR A SOLUCION INYECTABLE",
                             "POLVO MEDICADO PARA USO TOPICO",
                             "POLVO PARA INHALACION",
                             "POLVO PARA RECONSTITUIR A SOLUCION ORAL",
                             "POLVO PARA RECONSTITUIR A SUSPENSION ORAL",
                             "POMADA",
                             "SISTEMAS DE LIBERACION",
                             "SOLUCION BUCAL",
                             "SOLUCION BUCOFARINGUEA",
                             "SOLUCION ESTERIL PARA DIÁLISIS PERITONEAL",
                             "SOLUCION ESTERIL PARA IRRIGACION",
                             "SOLUCION NASAL",
                             "SOLUCION INYECTABLE",
                             "SOLUCION OFTALMICA",
                             "SOLUCION ORAL",
                             "SOLUCION OTICA",
                             "SOLUCION PARA ADMINISTRAR POR VIA RECTAL",
                             "SOLUCION PARA USO TRANSDERMICO(PARCHE)",
                             "SOLUCION PARA INHALACION",
                             "SOLUCION PARA PRESERVACION DE ORGANOS EN TRANSPLANTES",
                             "SOLUCION TOPICA",
                             "SOLUCIÓN CONCENTRADA PARA INFUSIÓN",
                             "SOLUCIÓN PARA INFUSIÓN",
                             "SOLUCIÓN SUBLINGUAL",
                             "SUPOSITORIO",
                             "SUSPENSIONES",
                             "SUSPENSION INYECTABLE",
                             "SUSPENSION NASAL",
                             "SUSPENSION OFTALMICA",
                             "SUSPENSION ORAL",
                             "SUSPENSION OTICA",
                             "SUSPENSION PARA ADMINISTRAR PORVIARECTAL",
                             "SUSPENSION PARA INHALACION",
                             "SUSPENSION PARA NEBULIZACION",
                             "SUSPENSION TOPICA",
                             "TABLETA",
                             "TABLETA CON CUBIERTA ENTERICA CON PELICULA",
                             "TABLETA CON RECUBRIMIENTO ENTÉRICO",
                             "TABLETA CUBIERTA (GRAGEA)",
                             "TABLETA CUBIERTA CON PELICULA",
                             "TABLETA EFERVESCENTE",
                             "TABLETA DE LIBERACION PROLONGADA",
                             "TABLETA DE LIBERACIÓN MODIFICADA",
                             "TABLETA DE LIBERACION RETARDADA",
                             "TABLETA DISPERSABLE",
                             "TABLETA MASTICABLE",
                             "TABLETA ORODISPERSABLE",
                             "TABLETA RECUBIERTA",
                             "TABLETA SUBLINGUAL",
                             "TABLETA VAGINAL",
                             "TRANSDERMICOS",
                             "UNGUENTO OFTALMICO",
                             "UNGUENTO PROCTOLOGICO",
                             "UNGUENTO TOPICO",))
# ---------------------------------------------------------------------------------------

        grop = QGroupBox("Cantidad", self)
        grop.setGeometry(70, 240, 120, 135)

        self.la14_0 = QLabel("Uni:/:", self)
        self.la14_0.setGeometry(90, 265, 120, 15)

        self.li14_0 = QLineEdit(self)
        self.li14_0.setAlignment(Qt.AlignCenter)
        self.li14_0.setPlaceholderText("0")
        self.li14_0.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li14_0.setGeometry(125, 265, 50, 20)

        self.la14_1 = QLabel("Tab:", self)
        self.la14_1.setGeometry(90, 285, 120, 15)

        self.li14_1 = QLineEdit(self)
        self.li14_1.setAlignment(Qt.AlignCenter)
        self.li14_1.setPlaceholderText("0")
        self.li14_1.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li14_1.setGeometry(125, 285, 50, 20)

        self.la14_2 = QLabel("Caj:", self)
        self.la14_2.setGeometry(90, 305, 120, 15)

        self.li14_2 = QLineEdit(self)
        self.li14_2.setAlignment(Qt.AlignCenter)
        self.li14_2.setPlaceholderText("0")
        self.li14_2.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li14_2.setGeometry(125, 305, 50, 20)

        self.la14_3 = QLabel("Bol:", self)
        self.la14_3.setGeometry(90, 325, 120, 15)

        self.li14_3 = QLineEdit(self)
        self.li14_3.setAlignment(Qt.AlignCenter)
        self.li14_3.setPlaceholderText("0")
        self.li14_3.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li14_3.setGeometry(125, 325, 50, 20)

        self.la14_4 = QLabel("Fra:", self)
        self.la14_4.setGeometry(90, 345, 120, 15)

        self.li14_4 = QLineEdit(self)
        self.li14_4.setAlignment(Qt.AlignCenter)
        self.li14_4.setPlaceholderText("0")
        self.li14_4.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li14_4.setGeometry(125, 345, 50, 20)
# ---------------------------------------------------------------------------------------
        self.laF = QLabel(self)
        self.laF.setGeometry(80, 380, 100, 100)
        self.laF.setAlignment(Qt.AlignCenter)
        self.laF.setStyleSheet("border: 2px solid white;")

        self.bot1 = QPushButton(self)
        self.bot1.setGeometry(150, 450, 30, 30)
        self.bot1.setIcon(QIcon(QPixmap("icon/photo-48.png")))
        self.bot1.setIconSize(QSize(30, 30))
        self.bot1.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bot1.setFlat(True)
        self.bot1.clicked.connect(self.insertarFoto)

        self.bot2 = QPushButton(self)
        self.bot2.setGeometry(575, 385, 48, 48)
        self.bot2.setIcon(QIcon(QPixmap("icon/Database-48.png")))
        self.bot2.setIconSize(QSize(48, 48))
        self.bot2.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bot2.setFlat(True)
        self.bot2.clicked.connect(self.insertarMedicamentos)

    def insertarFoto(self):
        try:
            filtro = "All"
            self.foto = QFileDialog.getOpenFileName(self, "Guardar", filtro)
            photo = QImage(self.foto[0])
            pixmap = QPixmap(photo.scaledToWidth(100))
            self.laF.setPixmap(pixmap)
        except OSError as err:
            print("Error OS: {0}".format(err))
        except ValueError:
            print("No pude convertir el dato a un entero.")
        except:
            print("Error inesperado:", sys.exc_info()[0])
            raise

    def insertarMedicamentos(self):
        if len(self.li1.text()) == 0:
            QMessageBox.warning(self, "Informacion", "Por favor ingrese su codigo.", QMessageBox.Ok)
            pass
        elif len(self.li2.text()) == 0:
            QMessageBox.warning(self, 'Informacion', 'Por favor ingrese su nombre.', QMessageBox.Ok)
            pass
        elif len(self.li3.text()) == 0:
            QMessageBox.warning(self, 'Informacion',
                                'Por favor ingrese presio de unidad.', QMessageBox.Ok)
            pass
        elif len(self.li10.text()) == 0:
            QMessageBox.warning(self, 'Informacion',
                                'Por favor ingrese el nombre del laboratorio.', QMessageBox.Ok)
            pass
        elif len(self.li11.text()) == 0:
            QMessageBox.warning(self, 'Informacion',
                                'Por favor ingrese fecha de expedicion.', QMessageBox.Ok)
            pass
        elif len(self.li12.text()) == 0:
            QMessageBox.warning(self, 'Informacion',
                                'Por favor ingrese fecha de vencimiento.', QMessageBox.Ok)
            pass
        elif len(self.li14_0.text()) == 0:
            QMessageBox.warning(self, 'Informacion',
                                'Por favor ingrese la cantidad de Unidades.', QMessageBox.Ok)
            pass
        elif len(self.li15.text()) == 0:
            QMessageBox.warning(self, 'Informacion', 'Por favor ingrese el UTC!', QMessageBox.Ok)
            pass
        else:
            codigo = int(self.li1.text())
            nombre = self.li2.text()

            if self.li3.text() == "":
                presiocl = 0
                presiou = 0
            else:
                presiocl = float(self.li3.text())
                presiou = float(self.li3.text())

            if self.li4.text() == "":
                presiot = 0
            else:
                presiot = float(self.li4.text())

            if self.li5.text() == "":
                presioc = 0
            else:
                presioc = float(self.li5.text())

            if self.li6.text() == "":
                presiobl = 0
            else:
                presiobl = float(self.li6.text())

            if self.li7.text() == "":

                presiobt = 0
            else:
                presiobt = float(self.li7.text())

            tipo = self.comb1.currentText()
            concentra = self.comb2.currentText()

            laborator = self.li10.text()

            fexpedi = self.li11.text()
            fvencim = self.li12.text()
            adminis = self.comb3.currentText()

            if self.li14_0.text() == "":
                unidad = 0
            else:
                unidad = int(self.li14_0.text())

            if self.li14_1.text() == "":
                table = 0
            else:
                table = int(self.li14_1.text())

            if self.li14_2.text() == "":
                caja = 0
            else:
                caja = int(self.li14_2.text())

            if self.li14_3.text() == "":
                bol = 0
            else:
                bol = int(self.li14_3.text())

            if self.li14_4.text() == "":
                fras = 0
            else:
                fras = int(self.li14_4.text())
            if self.liLo.text() == "":
                lote = 0
            else:
                lote = self.liLo.text()

            atc = self.li15.text()
            info = str(self.lista1.toPlainText())
            farmaceu = self.comb4.currentText()

            if caja == 0:
                totalUnidades = unidad * bol
            else:
                totalUnidades = (unidad * table) * caja

            try:
                with open(self.foto[0], "rb") as imageFile:
                    conver = imageFile.read()
                    byte = bytearray(conver)
            except:
                byte = ""

            db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
            query = db.cursor()
            try:
                if (query.execute("""INSERT INTO Almacen
					(CodigoBarra, Nombre, Laboratorio,
					Lote, Fecha_Expedicion, Fecha_Vencimiento,
					Tipo, Descripcion_ATC, VIA_Administracion,
					Concentracion, Cajas, Unidades, Tabletas,
					Bolsa, Frasco, Forma_Farmaceutica, Presio_Cliente,
					Presio_Unidad, Presio_Tableta, Presio_Caja, Presio_Bolsa,
					Presio_Botella, Informacion, Foto)
					VALUES
					(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                                  (codigo, nombre, laborator, lote, fexpedi, fvencim, tipo, atc, adminis, concentra, caja, totalUnidades, table, bol, fras, farmaceu, presiou, presiou, presiot, presioc, presiobl, presiobt, info, byte))):
                    db.commit()
                    QMessageBox.information(self, 'Informacion',
                                            'Registro Completo.', QMessageBox.Ok)
                else:
                    QMessageBox.warning(
                        self, 'Warning', 'Problema al ingresar la inforamcion.', QMessageBox.Ok)
            except:
                QMessageBox.critical(self, 'Alerta', 'Fallo al conectar con la DB.', QMessageBox.Ok)

            query.execute("INSERT INTO Info (ID, Nombre, Cajas, Unidades, Tabletas, Bolsas, Frascos) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                          (codigo, nombre, caja, totalUnidades, table, bol, fras))
            db.commit()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    x = ingresoDeMedicamentosDB()
    x.show()
    sys.exit(app.exec_())

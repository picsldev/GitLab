"""
VENTANA DE PAGOS
"""
import os
import sys
import re
import pymysql
import random
import time

# --------------------------------------
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.pdfmetrics import registerFontFamily
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib import colors
# --------------------------------------
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtMultimedia import *
# --------------------------------------
from sistema import *

# -----------------------------------------------------------------------------


class VentanaCajero(QDialog):
    def __init__(self, args, parent=None):
        QDialog.__init__(self, parent)
        self.setWindowTitle("CAJERO")
        self.setWindowIcon(QIcon("icon/cash_register-48.png"))
        self.setFixedSize(500, 300)
        self.move(QDesktopWidget().availableGeometry().center() -
                  self.frameGeometry().center())
        self.labelFondo = QLabel(self)
        self.labelFondo.setPixmap(QPixmap("icon/wallpaper.jpg"))
        self.labelFondo.setGeometry(0, -340, 1024, 640)
# Datos--------------------------------------

        self.line1 = args[0]
        self.line3 = args[1]
        self.line33 = args[2]
        self.line4 = args[3]
        self.line = args[4]

        self.table = args[5]

        self.table0_0 = args[6]
        self.table0_1 = args[7]
        self.table0_3 = args[8]
        self.table0_4 = args[9]
        self.table0_5 = args[10]

        self.table1_0 = args[11]
        self.table1_1 = args[12]
        self.table1_3 = args[13]
        self.table1_4 = args[14]
        self.table1_5 = args[15]

        self.table2_0 = args[16]
        self.table2_1 = args[17]
        self.table2_3 = args[18]
        self.table2_4 = args[19]
        self.table2_5 = args[20]

        self.table3_0 = args[21]
        self.table3_1 = args[22]
        self.table3_3 = args[23]
        self.table3_4 = args[24]
        self.table3_5 = args[25]

        self.table4_0 = args[26]
        self.table4_1 = args[27]
        self.table4_3 = args[28]
        self.table4_4 = args[29]
        self.table4_5 = args[30]

        self.table5_0 = args[31]
        self.table5_1 = args[32]
        self.table5_3 = args[33]
        self.table5_4 = args[34]
        self.table5_5 = args[35]

        self.table6_0 = args[36]
        self.table6_1 = args[37]
        self.table6_3 = args[38]
        self.table6_4 = args[39]
        self.table6_5 = args[40]

        self.table7_0 = args[41]
        self.table7_1 = args[42]
        self.table7_3 = args[43]
        self.table7_4 = args[44]
        self.table7_5 = args[45]

        self.table8_0 = args[46]
        self.table8_1 = args[47]
        self.table8_3 = args[48]
        self.table8_4 = args[49]
        self.table8_5 = args[50]

        self.table9_0 = args[51]
        self.table9_1 = args[52]
        self.table9_3 = args[53]
        self.table9_4 = args[54]
        self.table9_5 = args[55]

        self.table10_0 = args[56]
        self.table10_1 = args[57]
        self.table10_3 = args[58]
        self.table10_4 = args[59]
        self.table10_5 = args[60]

        self.table11_0 = args[61]
        self.table11_1 = args[62]
        self.table11_3 = args[63]
        self.table11_4 = args[64]
        self.table11_5 = args[65]

        self.table12_0 = args[66]
        self.table12_1 = args[67]
        self.table12_3 = args[68]
        self.table12_4 = args[69]
        self.table12_5 = args[70]

        self.table13_0 = args[71]
        self.table13_1 = args[72]
        self.table13_3 = args[73]
        self.table13_4 = args[74]
        self.table13_5 = args[75]

        self.table14_0 = args[76]
        self.table14_1 = args[77]
        self.table14_3 = args[78]
        self.table14_4 = args[79]
        self.table14_5 = args[80]

        self.table15_0 = args[81]
        self.table15_1 = args[82]
        self.table15_3 = args[83]
        self.table15_4 = args[84]
        self.table15_5 = args[85]

        self.table16_0 = args[86]
        self.table16_1 = args[87]
        self.table16_3 = args[88]
        self.table16_4 = args[89]
        self.table16_5 = args[90]

        self.table17_0 = args[91]
        self.table17_1 = args[92]
        self.table17_3 = args[93]
        self.table17_4 = args[94]
        self.table17_5 = args[95]

        self.table18_0 = args[96]
        self.table18_1 = args[97]
        self.table18_3 = args[98]
        self.table18_4 = args[99]
        self.table18_5 = args[100]

        self.table19_0 = args[101]
        self.table19_1 = args[102]
        self.table19_3 = args[103]
        self.table19_4 = args[104]
        self.table19_5 = args[105]

        self.labelT = args[106]

        self.cajeroVirtual()
# Botones--------------------------------------

    def cajeroVirtual(self):
        self.labeS1 = QLabel("0", self)
        self.labeS1.setFont(QFont('SansSerif', 20))
        self.labeS1.setStyleSheet("color: green")
        self.labeS1.setGeometry(140, 0, 140, 100)
        self.labeS1.setText("$ " + self.labelT)

        self.labeS = QLabel("SUB:", self)
        self.labeS.setFont(QFont('SansSerif', 20))
        self.labeS.setStyleSheet("color: green")
        self.labeS.setGeometry(20, 0, 70, 100)

        self.labeI = QLabel("IVA:", self)
        self.labeI.setFont(QFont('SansSerif', 20))
        self.labeI.setStyleSheet("color: green")
        self.labeI.setGeometry(20, 30, 130, 100)

        self.labeT1 = QLabel("0", self)
        self.labeT1.setFont(QFont('SansSerif', 20))
        self.labeT1.setStyleSheet("color: green")
        self.labeT1.setGeometry(140, 90, 140, 50)
        self.labeT1.setText("$ " + self.labelT)

        self.labeT = QLabel("TOTAL:", self)
        self.labeT.setFont(QFont('SansSerif', 20))
        self.labeT.setStyleSheet("color: green")
        self.labeT.setGeometry(20, 90, 95, 50)

        self.labeLinea = QLabel(
            "________________________________________________________", self)
        self.labeLinea.setGeometry(20, 110, 250, 30)
        self.labeLinea.setStyleSheet("color: green")

        self.labeV = QLabel("VUELTO:", self)
        self.labeV.setFont(QFont('SansSerif', 20))
        self.labeV.setStyleSheet("color: RED")
        self.labeV.setGeometry(20, 120, 150, 50)

        self.labeV1 = QLabel("   0", self)
        self.labeV1.setFont(QFont('SansSerif', 20))
        self.labeV1.setStyleSheet("color: red")
        self.labeV1.setGeometry(140, 120, 140, 50)

        self.labeF = QLabel("FOTO", self)
        self.labeF.setGeometry(310, 10, 150, 240)
        self.labeF.setPixmap(QPixmap("icon/recibo.png"))

        self.labeP = QLabel(self)
        self.labeP.setGeometry(310, 70, 150, 100)

        self.lineP = QLineEdit(self)
        self.lineP.setAlignment(Qt.AlignCenter)
        self.lineP.setPlaceholderText("Dinero")
        self.lineP.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.lineP.setGeometry(20, 180, 120, 20)
        self.lineP.textChanged.connect(self.ValidarDinero)

        self.boton = QPushButton(self)
        self.boton.setGeometry(54, 200, 50, 50)
        self.boton.setIcon(QIcon(QPixmap("icon/Cash_in_Hand-48.png")))
        self.boton.setIconSize(QSize(50, 50))
        self.boton.setStyleSheet("background: rgb(255,255,255,0);")
        self.boton.setFlat(True)
        self.boton.clicked.connect(self.vuelto)

    def ValidarDinero(self):
        validar = re.match('^[0-9\sáéíóúàèìòùäëïöüñ]+$',
                           self.lineP.text(), re.I)
        if self.lineP.text() == "":
            self.lineP.setStyleSheet("border: 2px solid yellow;")
            return False
        elif not validar:
            self.lineP.setStyleSheet("border: 2px solid red;")
            return False
        else:
            self.lineP.setStyleSheet("border: 2px solid cyan;")
            return True

    def vuelto(self):
        if self.ValidarDinero():
            self.numeroDeGuia = str(random.randrange(999999999))
            self.numeroDeVenta = str(random.randrange(999999999))

            db = pymysql.connect(host="localhost", user="root",
                                 passwd="Lb104572", db="inventario")
            query = db.cursor()
            if (query.execute("SELECT * FROM Recibo WHERE CodigoG ='" + self.numeroDeGuia + "' AND CodigoV = '" + self.numeroDeVenta + "'")):
                db.commit()
                QMessageBox.information(
                    self, 'Informacion', 'Codigo de guia o Numero de venta ya esta registradas, se generara otro codigo automatica mente para evitar problemas.', QMessageBox.Ok)
            else:
                nguia = self.numeroDeGuia
                nventa = self.numeroDeVenta
                cliente = self.line1
                total = float(self.labelT)

                dato = [nguia, nventa, cliente,
                        self.table0_1, self.table1_1, self.table2_1, self.table3_1, self.table4_1,
                        self.table5_1, self.table6_1, self.table7_1, self.table8_1, self.table9_1,
                        self.table10_1, self.table11_1, self.table12_1, self.table13_1, self.table14_1,
                        self.table15_1, self.table16_1, self.table17_1, self.table18_1, self.table19_1, total]

                if float(self.lineP.text()) >= float(self.labelT):
                    rest = float(self.lineP.text()) - float(self.labelT)
                    self.labeV1.setText("$ " + str(rest))

                    db = pymysql.connect(
                        host="localhost", user="root", passwd="Lb104572", db="inventario")
                    query = db.cursor()
                    if (query.execute("""INSERT INTO Recibo(CodigoG,CodigoV,Cliente,M1,M2,M3,M4,M5,M6,M7,M8,M9,M10,M11,M12,M13,M14,M15,M16,M17,M18,M19,M20,Total) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                                      (dato[0], dato[1], dato[2], dato[3], dato[4], dato[5], dato[6], dato[7], dato[8], dato[9], dato[10], dato[11], dato[12], dato[13], dato[14], dato[15], dato[16], dato[17], dato[18], dato[19], dato[20], dato[21], dato[22], dato[23]))):
                        db.commit()
                        db.close()
                        query.close()
                        QSound.play("song/caja.wav")
                        entro = QMessageBox.information(
                            self, 'Informacion', 'La venta se a efectuado!', QMessageBox.Ok)
                        self.lineP.setText("")
                        if entro == QMessageBox.Ok:
                            # Formato de Fecha 00/00/00
                            self.fecha = time.strftime("%d/%m/%y")
                            self.hora = time.strftime(
                                "%I:%M:%S")  # Formato de 12 horas

                            aux = canvas.Canvas(
                                "pdf/" + self.numeroDeGuia + ".pdf", pagesize=A4)
                            aux.setFont('Helvetica', 13)
                            aux.drawImage("pdf/GUIA/GUIA.png", 0, 0, 595, 842)
                            aux.drawImage("pdf/GUIA/sello1.png",
                                          40, 10, 150, 100)

                            # PDF CLIENTE
                            aux.drawString(440, 726, self.line1)
                            aux.drawString(
                                423, 714, self.line3 + "  -  " + self.line33)
                            aux.drawString(462, 702, self.line4)
                            aux.drawString(420, 689, self.line)
                            aux.setFont('Helvetica', 10)
                            aux.drawString(55, 644, str(self.numeroDeVenta))
                            aux.drawString(205, 644, str(self.fecha))
                            aux.drawString(355, 644, str(self.hora))
                            aux.setFillColorRGB(255, 0, 0)
                            aux.setFont('Helvetica', 15)
                            aux.drawString(505, 644, str(self.numeroDeGuia))
                            aux.setFillColorRGB(0, 0, 0)
                            aux.setFont('Helvetica', 10)

                            if self.table > 0:
                                aux.drawString(24, 600,  self.table0_0)
                                aux.drawString(85, 600,  self.table0_1)
                                aux.drawString(420, 600, self.table0_3)
                                aux.drawString(466, 600, self.table0_4)
                                aux.drawString(520, 600, self.table0_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table0_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table0_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table0_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 1:
                                aux.drawString(24, 580,  self.table1_0)
                                aux.drawString(85, 580,  self.table1_1)
                                aux.drawString(420, 580, self.table1_3)
                                aux.drawString(466, 580, self.table1_4)
                                aux.drawString(520, 580, self.table1_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table1_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table1_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table1_0+"'")
                                db.commit()
                                db.close()
                                query.close()

                            if self.table > 2:
                                aux.drawString(24, 560,  self.table2_0)
                                aux.drawString(85, 560,  self.table2_1)
                                aux.drawString(420, 560, self.table2_3)
                                aux.drawString(466, 560, self.table2_4)
                                aux.drawString(520, 560, self.table2_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table2_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table2_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table2_0+"'")
                                db.commit()
                                db.close()
                                query.close()

                            if self.table > 3:
                                aux.drawString(24, 540,  self.table3_0)
                                aux.drawString(85, 540,  self.table3_1)
                                aux.drawString(420, 540, self.table3_3)
                                aux.drawString(466, 540, self.table3_4)
                                aux.drawString(520, 540, self.table3_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table3_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table3_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table3_0+"'")
                                db.commit()
                                db.close()
                                query.close()

                            if self.table > 4:
                                aux.drawString(24, 520,  self.table4_0)
                                aux.drawString(85, 520,  self.table4_1)
                                aux.drawString(420, 520, self.table4_3)
                                aux.drawString(466, 520, self.table4_4)
                                aux.drawString(520, 520, self.table4_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table4_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table4_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table4_0+"'")
                                db.commit()
                                db.close()
                                query.close()

                            if self.table > 5:
                                aux.drawString(24, 500,  self.table5_0)
                                aux.drawString(85, 500,  self.table5_1)
                                aux.drawString(420, 500, self.table5_3)
                                aux.drawString(466, 500, self.table5_4)
                                aux.drawString(520, 500, self.table5_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table5_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table5_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table5_0+"'")
                                db.commit()
                                db.close()
                                query.close()

                            if self.table > 6:
                                aux.drawString(24, 480,  self.table6_0)
                                aux.drawString(85, 480,  self.table6_1)
                                aux.drawString(420, 480, self.table6_3)
                                aux.drawString(466, 480, self.table6_4)
                                aux.drawString(520, 480, self.table6_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table6_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table6_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table6_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 7:
                                aux.drawString(24, 460,  self.table7_0)
                                aux.drawString(85, 460,  self.table7_1)
                                aux.drawString(420, 460, self.table7_3)
                                aux.drawString(466, 460, self.table7_4)
                                aux.drawString(520, 460, self.table7_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table7_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table7_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table7_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 8:
                                aux.drawString(24, 440,  self.table8_0)
                                aux.drawString(85, 440,  self.table8_1)
                                aux.drawString(420, 440, self.table8_3)
                                aux.drawString(466, 440, self.table8_4)
                                aux.drawString(520, 440, self.table8_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table8_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table8_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table8_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 9:
                                aux.drawString(24, 420,  self.table9_0)
                                aux.drawString(85, 420,  self.table9_1)
                                aux.drawString(420, 420, self.table9_3)
                                aux.drawString(466, 420, self.table9_4)
                                aux.drawString(520, 420, self.table9_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table9_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table9_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table9_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 10:
                                aux.drawString(24, 400,  self.table10_0)
                                aux.drawString(85, 400,  self.table10_1)
                                aux.drawString(420, 400, self.table10_3)
                                aux.drawString(466, 400, self.table10_4)
                                aux.drawString(520, 400, self.table10_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table10_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table10_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table10_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 11:
                                aux.drawString(24, 380,  self.table11_0)
                                aux.drawString(85, 380,  self.table11_1)
                                aux.drawString(420, 380, self.table11_3)
                                aux.drawString(466, 380, self.table11_4)
                                aux.drawString(520, 380, self.table11_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table11_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table11_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table11_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 12:
                                aux.drawString(24, 360,  self.table12_0)
                                aux.drawString(85, 360,  self.table12_1)
                                aux.drawString(420, 360, self.table12_3)
                                aux.drawString(466, 360, self.table12_4)
                                aux.drawString(520, 360, self.table12_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table12_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table12_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table12_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 13:
                                aux.drawString(24, 340,  self.table13_0)
                                aux.drawString(85, 340,  self.table13_1)
                                aux.drawString(420, 340, self.table13_3)
                                aux.drawString(466, 340, self.table13_4)
                                aux.drawString(520, 340, self.table13_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table13_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table13_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table13_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 14:
                                aux.drawString(24, 320,  self.table14_0)
                                aux.drawString(85, 320,  self.table14_1)
                                aux.drawString(420, 320, self.table14_3)
                                aux.drawString(466, 320, self.table14_4)
                                aux.drawString(520, 320, self.table14_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table14_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table14_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table14_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 15:
                                aux.drawString(24, 300,  self.table15_0)
                                aux.drawString(85, 300,  self.table15_1)
                                aux.drawString(420, 300, self.table15_3)
                                aux.drawString(466, 300, self.table15_4)
                                aux.drawString(520, 300, self.table15_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table15_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table15_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table15_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 16:
                                aux.drawString(24, 280,  self.table16_0)
                                aux.drawString(85, 280,  self.table16_1)
                                aux.drawString(420, 280, self.table16_3)
                                aux.drawString(466, 280, self.table16_4)
                                aux.drawString(520, 280, self.table16_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table16_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table16_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table16_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 17:
                                aux.drawString(24, 260,  self.table17_0)
                                aux.drawString(85, 260,  self.table17_1)
                                aux.drawString(420, 260, self.table17_3)
                                aux.drawString(466, 260, self.table17_4)
                                aux.drawString(520, 260, self.table17_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table17_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table17_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table17_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 18:
                                aux.drawString(24, 240,  self.table18_0)
                                aux.drawString(85, 240,  self.table18_1)
                                aux.drawString(420, 240, self.table18_3)
                                aux.drawString(466, 240, self.table18_4)
                                aux.drawString(520, 240, self.table18_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table18_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table18_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table18_0+"'")
                                db.commit()
                                db.close()
                                query.close()
                            if self.table > 19:
                                aux.drawString(24, 220,  self.table19_0)
                                aux.drawString(85, 220,  self.table19_1)
                                aux.drawString(420, 220, self.table19_3)
                                aux.drawString(466, 220, self.table19_4)
                                aux.drawString(520, 220, self.table19_5)
                                db = pymysql.connect(
                                    host="localhost", user="root", passwd="Lb104572", db="inventario")
                                query = db.cursor()
                                if (query.execute("SELECT Unidades FROM Almacen WHERE ID ='"+self.table19_0+"'")):
                                    for row in query:
                                        uni = int(row[0])
                                        resta = uni - int(self.table19_3)
                                        total = str(resta)
                                query.execute(
                                    "UPDATE Almacen SET Unidades = '"+total+"' WHERE ID='"+self.table19_0+"'")
                                db.commit()
                                db.close()
                                query.close()

                            aux.drawString(520, 90, self.labelT)
                            aux.drawString(520, 70, "0")
                            aux.setFillColorRGB(255, 0, 0)
                            aux.setFont('Helvetica', 15)
                            aux.drawString(520, 35, self.labelT)
                            aux.save()
                            self.labeP.setPixmap(QPixmap("pdf/sello.png"))

                            rec = QMessageBox.information(
                                self, 'Inforamcion', 'Se a creado un PDF', QMessageBox.Ok)

                            if rec == QMessageBox.Ok:
                                self.reciboCreator()
                        else:
                            QMessageBox.warning(
                                self, 'Warning', 'Problema al generar PDF.', QMessageBox.Ok)
                    else:
                        QMessageBox.warning(
                            self, 'Warning', 'Problema al ingresar los datos.', QMessageBox.Ok)
                else:
                    QMessageBox.information(
                        self, 'Inforamcion', 'Tiene que ser un valor elevado al presio.', QMessageBox.Ok)
        else:
            QMessageBox.warning(
                self, 'Warning', 'No se permite letras.', QMessageBox.Ok)

    def reciboCreator(self):
        recibo = canvas.Canvas("recibo/" + self.numeroDeGuia + ".pdf")
        recibo.setPageSize((180, 400))
        recibo.drawImage("recibo/GUIA/GUIA.jpg", 0, 0, 180, 400)

        pdfmetrics.registerFont(TTFont('Arial', 'arial.ttf'))

        recibo.setFont('Arial', 10)
        recibo.drawString(55, 308, "FARMACIA LA 40")
        recibo.setFont('Arial', 8)
        recibo.drawString(49, 300, "Siempre estamos contigo.")
        recibo.setFont('Arial', 7)
        recibo.drawString(10, 290, "Cliente:")
        recibo.drawString(36, 290, self.line1)

        recibo.setFont('Arial', 4)
        recibo.drawString(
            10, 285, "===|======================================|=========|=======|==========")
        recibo.setFont('Arial', 4)

        recibo.setFont('Arial', 4)
        recibo.drawString(10, 280,  "N.")
        recibo.drawString(30, 280,  "NOMBRE DEL MEDICAMTNO")
        recibo.drawString(110, 280, "UNIDAD")
        recibo.drawString(134, 280, "TIPO")
        recibo.drawString(153, 280, "COSTO")

        recibo.drawString(
            10, 275, "===|======================================|=========|=======|==========")

        recibo.setFont('Arial', 6)
        if self.table > 0:
            recibo.drawString(10, 260,  "1.")
            recibo.drawString(20, 260,  self.table0_1)
            recibo.drawString(117, 260, self.table0_3)
            recibo.drawString(130, 260, self.table0_4)
            recibo.drawString(149, 260, self.table0_5)

        if self.table > 1:
            recibo.drawString(10, 250,  "2.")
            recibo.drawString(20, 250,  self.table1_1)
            recibo.drawString(117, 250, self.table1_3)
            recibo.drawString(130, 250, self.table1_4)
            recibo.drawString(149, 250, self.table1_5)

        if self.table > 2:
            recibo.drawString(10, 240,  "3.")
            recibo.drawString(20, 240,  self.table2_1)
            recibo.drawString(117, 240, self.table2_3)
            recibo.drawString(130, 240, self.table2_4)
            recibo.drawString(149, 240, self.table2_5)

        if self.table > 3:
            recibo.drawString(10, 230,  "4.")
            recibo.drawString(20, 230,  self.table3_1)
            recibo.drawString(117, 230, self.table3_3)
            recibo.drawString(130, 230, self.table3_4)
            recibo.drawString(149, 230, self.table3_5)

        if self.table > 4:
            recibo.drawString(10, 220,  "5.")
            recibo.drawString(20, 220,  self.table4_1)
            recibo.drawString(117, 220, self.table4_3)
            recibo.drawString(130, 220, self.table4_4)
            recibo.drawString(149, 220, self.table4_5)

        if self.table > 5:
            recibo.drawString(10, 210,   "6.")
            recibo.drawString(20, 210,  self.table5_1)
            recibo.drawString(117, 210, self.table5_3)
            recibo.drawString(130, 210, self.table5_4)
            recibo.drawString(149, 210, self.table5_5)

        if self.table > 6:
            recibo.drawString(10, 200,   "7.")
            recibo.drawString(20, 200,  self.table6_1)
            recibo.drawString(117, 200, self.table6_3)
            recibo.drawString(130, 200, self.table6_4)
            recibo.drawString(149, 200, self.table6_5)
        if self.table > 7:
            recibo.drawString(10, 190,   "8.")
            recibo.drawString(20, 190,  self.table7_1)
            recibo.drawString(117, 190, self.table7_3)
            recibo.drawString(130, 190, self.table7_4)
            recibo.drawString(149, 190, self.table7_5)
        if self.table > 8:
            recibo.drawString(10, 180,   "9.")
            recibo.drawString(20, 180,  self.table8_1)
            recibo.drawString(117, 180, self.table8_3)
            recibo.drawString(130, 180, self.table8_4)
            recibo.drawString(149, 180, self.table8_5)
        if self.table > 9:
            recibo.drawString(10, 170,   "10.")
            recibo.drawString(20, 170,  self.table9_1)
            recibo.drawString(117, 170, self.table9_3)
            recibo.drawString(130, 170, self.table9_4)
            recibo.drawString(149, 170, self.table9_5)
        if self.table > 10:
            recibo.drawString(10, 160,   "11.")
            recibo.drawString(20, 160,  self.table10_1)
            recibo.drawString(117, 160, self.table10_3)
            recibo.drawString(130, 160, self.table10_4)
            recibo.drawString(149, 160, self.table10_5)
        if self.table > 11:
            recibo.drawString(10, 150,   "12.")
            recibo.drawString(20, 150,  self.table11_1)
            recibo.drawString(117, 150, self.table11_3)
            recibo.drawString(130, 150, self.table11_4)
            recibo.drawString(149, 150, self.table11_5)
        if self.table > 12:
            recibo.drawString(10, 140,   "13.")
            recibo.drawString(20, 140,  self.table12_1)
            recibo.drawString(117, 140, self.table12_3)
            recibo.drawString(130, 140, self.table12_4)
            recibo.drawString(149, 140, self.table12_5)
        if self.table > 13:
            recibo.drawString(10, 130,   "14.")
            recibo.drawString(20, 130,  self.table13_1)
            recibo.drawString(117, 130, self.table13_3)
            recibo.drawString(130, 130, self.table13_4)
            recibo.drawString(149, 130, self.table13_5)
        if self.table > 14:
            recibo.drawString(10, 120,   "15.")
            recibo.drawString(20, 120,  self.table14_1)
            recibo.drawString(117, 120, self.table14_3)
            recibo.drawString(130, 120, self.table14_4)
            recibo.drawString(149, 120, self.table14_5)
        if self.table > 15:
            recibo.drawString(10, 110,   "16.")
            recibo.drawString(20, 110,  self.table15_1)
            recibo.drawString(117, 110, self.table15_3)
            recibo.drawString(130, 110, self.table15_4)
            recibo.drawString(149, 110, self.table15_5)
        if self.table > 16:
            recibo.drawString(10, 100,   "17.")
            recibo.drawString(20, 100,  self.table16_1)
            recibo.drawString(117, 100, self.table16_3)
            recibo.drawString(130, 100, self.table16_4)
            recibo.drawString(149, 100, self.table16_5)
        if self.table > 17:
            recibo.drawString(10, 90,   "18.")
            recibo.drawString(20, 90,  self.table17_1)
            recibo.drawString(117, 90, self.table17_3)
            recibo.drawString(130, 90, self.table17_4)
            recibo.drawString(149, 90, self.table17_5)
        if self.table > 18:
            recibo.drawString(10, 80,   "19.")
            recibo.drawString(20, 80,  self.table18_1)
            recibo.drawString(117, 80, self.table18_3)
            recibo.drawString(130, 80, self.table18_4)
            recibo.drawString(149, 80, self.table18_5)
        if self.table > 19:
            recibo.drawString(10, 70,   "20.")
            recibo.drawString(20, 70,  self.table19_1)
            recibo.drawString(117, 70, self.table19_3)
            recibo.drawString(130, 70, self.table19_4)
            recibo.drawString(149, 70, self.table19_5)

        recibo.setFont('Arial', 7)
        recibo.drawString(10, 42, "Guia:")
        recibo.drawString(30, 42, self.numeroDeGuia)
        recibo.setFont('Arial', 4)
        recibo.drawString(
            10, 53, "====================================================================")
        recibo.setFont('Arial', 8)
        recibo.drawString(100, 42, "TOTAL: ")
        recibo.drawString(130, 42, self.labelT)
        recibo.setFont('Arial', 4)
        recibo.drawString(
            10, 33, "====================================================================")

        recibo.setFont('Arial', 5)
        recibo.drawString(60, 25, "Nombre: Sr. Gloria Patricia")
        recibo.drawString(73, 20, "Tel: 3006642294")
        recibo.drawString(
            40, 15, "Direccion: Cra 2b # 20a - 39 Soledad - Atlantico")
        recibo.drawString(70, 10, "RUD: xxxxxxxxxxxxx")
        recibo.showPage()
        recibo.save()

        try:
            x = os.path.abspath("recibo")
            os.startfile(x+"/" + self.numeroDeGuia + ".pdf")
        except:
            os.system("epdfview recibo/" + self.numeroDeGuia + ".pdf")

"""
VENTANA REGISTRO DEL ADMIN
"""
import pymysql
import os
import sys
import re
import hashlib
# --------------------------------------
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
# --------------------------------------


class RAVentana(QDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self, parent)

        self.setWindowTitle("Registro ADM")
        self.setWindowIcon(QIcon("icon/Pumps-48.png"))
        self.setFixedSize(300, 380)
        self.move(QDesktopWidget().availableGeometry().center() - self.frameGeometry().center())

        labelFondo = QLabel(self)
        labelFondo.setPixmap(QPixmap("icon/wallpaper.jpg"))
        labelFondo.setGeometry(0, 0, 300, 380)

        labelIcon = QLabel(self)
        labelIcon.setPixmap(QPixmap("icon/enfermera.png"))
        labelIcon.setGeometry(100, 20, 100, 100)

        label1 = QLabel("[NOMBRE]", self)
        label1.move(54, 130)
        self.line1 = QLineEdit(self)
        self.line1.setAlignment(Qt.AlignCenter)
        self.line1.setGeometry(20, 143, 120, 20)

        label2 = QLabel("[APELLIDO]", self)
        label2.move(185, 130)
        self.line2 = QLineEdit(self)
        self.line2.setAlignment(Qt.AlignCenter)
        self.line2.setGeometry(160, 143, 120, 20)

        label = QLabel("[CEDULA]", self)
        label.move(122, 180)
        self.line = QLineEdit(self)
        self.line.setAlignment(Qt.AlignCenter)
        self.line.setGeometry(90, 193, 120, 20)

        label3 = QLabel("[USUARIO]", self)
        label3.move(120, 228)
        self.line3 = QLineEdit(self)
        self.line3.setAlignment(Qt.AlignCenter)
        self.line3.setGeometry(90, 240, 120, 20)

        label4 = QLabel("[CLAVE]", self)
        label4.move(128, 275)
        self.line4 = QLineEdit(self)
        self.line4.setAlignment(Qt.AlignCenter)
        self.line4.setEchoMode(QLineEdit.Password)
        self.line4.setGeometry(90, 288, 120, 20)

        boton = QPushButton("REGISTRAR", self)
        boton.setIcon(QIcon(QPixmap("icon/Ok_48px.png")))
        boton.move(110, 320)
        boton.clicked.connect(self.Validar)

    def Validar(self):
        if len(self.line1.text()) == 0:
            QMessageBox.warning(self, "Informacion", "Por favor ingrese su Nombre!", QMessageBox.Ok)
            pass
        elif len(self.line.text()) == 0:
            QMessageBox.warning(self, 'Informacion', 'Por favor ingrese su Cedula!', QMessageBox.Ok)
            pass
        elif len(self.line2.text()) == 0:
            QMessageBox.warning(self, 'Informacion', 'Por favor ingrese su Apellido!', QMessageBox.Ok)
            pass
        elif len(self.line3.text()) == 0:
            QMessageBox.warning(self, 'Informacion', 'Por favor ingrese su Usuario!', QMessageBox.Ok)
            pass
        elif len(self.line4.text()) == 0:
            QMessageBox.warning(self, 'Informacion', 'Por favor ingrese su Clave!', QMessageBox.Ok)
            pass
        else:
            usuario = hashlib.sha512(self.line3.text().encode('utf-8')).hexdigest()
            password = hashlib.sha512(self.line4.text().encode('utf-8')).hexdigest()
            try:
                db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="admin")
                query = db.cursor()
                if (query.execute("INSERT INTO Registros (Codigo,Nombre,Apellido,Usuario,Clave) VALUES ('"+self.line.text()+"','"+self.line1.text()+"','"+self.line2.text()+"','"+usuario+"','"+password+"')")):
                    db.commit()
                    QMessageBox.information(self, 'Informacion', 'Registro Completo', QMessageBox.Ok)
                    self.limpiar()
                else:
                    QMessageBox.warning(self, 'Alerta', 'Problema', QMessageBox.Ok)
            except:
                QMessageBox.critical(self, 'Fallo', 'Error al conectar con la SQL.', QMessageBox.Ok)

    def limpiar(self):
        self.line.setText("")
        self.line1.setText("")
        self.line2.setText("")
        self.line3.setText("")
        self.line4.setText("")
        pass


if __name__ == "__main__":
    app = QApplication(sys.argv)
    vr = RAVentana()
    vr.show()
    sys.exit(app.exec_())

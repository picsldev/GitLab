"""
VENTANA REGISTRO DEL USER
"""
import pymysql
import os
import sys
import re
# --------------------------------------
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
# --------------------------------------


class RUVentana(QDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self, parent)

        self.setWindowTitle("Registro cliente")
        self.setWindowIcon(QIcon("icon/Pumps-48.png"))
        self.setFixedSize(300, 380)
        self.move(QDesktopWidget().availableGeometry().center() - self.frameGeometry().center())

        self.labelFondo = QLabel(self)
        self.labelFondo.setPixmap(QPixmap("icon/wallpaper.jpg"))
        self.labelFondo.setGeometry(0, 0, 300, 380)

        self.labelIcon = QLabel(self)
        self.labelIcon.setPixmap(QPixmap("icon/Group-Woman-Man-96.png"))
        self.labelIcon.setGeometry(100, 10, 100, 100)

        self.label = QLabel("[C.C]", self)
        self.label.move(20, 120)
        self.line = QLineEdit(self)
        self.line.setAlignment(Qt.AlignCenter)
        self.line.setGeometry(60, 118, 80, 20)

        self.label1 = QLabel("[NOMBRE]", self)
        self.label1.move(20, 150)
        self.line1 = QLineEdit(self)
        self.line1.setAlignment(Qt.AlignCenter)
        self.line1.setGeometry(20, 163, 120, 20)

        self.label2 = QLabel("[CIUDAD]", self)
        self.label2.move(200, 147)
        self.combo = QComboBox(self)
        self.combo.setGeometry(180, 160, 100, 25)
        self.combo.addItems(("Bogotá",
                             "Medellín",
                             "Cali",
                             "Barranquilla",
                             "Cartagena",
                             "Soledad",
                             "Cúcuta",
                             "Ibagué",
                             "Soacha",
                             "Bucaramanga",
                             "Villavicencio",
                             "Bello",
                             "Valledupar",
                             "Pereira",
                             "Buenaventura",
                             "Pasto",
                             "Manizales",
                             "Montería",
                             "Neiva",
                             ))

        self.label3 = QLabel("[DIRECCION]", self)
        self.label3.move(115, 200)
        self.line3 = QLineEdit(self)
        self.line3.setAlignment(Qt.AlignCenter)
        self.line3.setGeometry(90, 213, 120, 20)

        self.label4 = QLabel("[TELEFONO]", self)
        self.label4.move(116, 240)
        self.line4 = QLineEdit(self)
        self.line4.setAlignment(Qt.AlignCenter)
        self.line4.setGeometry(90, 253, 120, 20)

        self.label5 = QLabel("[CELULAR]", self)
        self.label5.move(120, 280)
        self.line5 = QLineEdit(self)
        self.line5.setAlignment(Qt.AlignCenter)
        self.line5.setGeometry(90, 293, 120, 20)

        self.boton = QPushButton("REGISTRAR", self)
        self.boton.setIcon(QIcon(QPixmap("icon/Ok_48px.png")))
        self.boton.move(113, 320)
        self.boton.clicked.connect(self.Validar)

    def Validar(self):
        if len(self.line.text()) == 0:
            QMessageBox.warning(self, "Informacion", "Por favor ingrese su Codigo!", QMessageBox.Ok)
            pass
        elif len(self.line1.text()) == 0:
            QMessageBox.warning(self, 'Informacion', 'Por favor ingrese su Nombre!', QMessageBox.Ok)
            pass
        elif len(self.line3.text()) == 0:
            QMessageBox.warning(self, 'Informacion', 'Por favor ingrese su Direccion!', QMessageBox.Ok)
            pass
        elif len(self.line4.text()) == 0:
            QMessageBox.warning(self, 'Informacion', 'Por favor ingrese su Telefono!', QMessageBox.Ok)
            pass
        else:
            try:
                db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="cliente")
                query = db.cursor()
                if (query.execute("INSERT INTO Registros (Codigo,Nombre,Ciudad,Direccion,Fijo,Celular) VALUES ('"+self.line.text()+"','"+self.line1.text()+"','"+self.combo.currentText()+"','"+self.line3.text()+"','"+self.line4.text()+"','"+self.line5.text()+"')")):
                    db.commit()
                    QMessageBox.information(self, 'Informacion', 'Registro Completo', QMessageBox.Ok)
                    self.limpiar()
                else:
                    QMessageBox.warning(self, 'Alerta', 'Problema', QMessageBox.Ok)
            except:
                QMessageBox.critical(self, 'Fallo', 'Error al conectar con la SQL.', QMessageBox.Ok)

    def limpiar(self):
        self.line.setText("")
        self.line1.setText("")
        self.line3.setText("")
        self.line4.setText("")
        self.line5.setText("")
        pass


if __name__ == "__main__":
    app = QApplication(sys.argv)
    vu = RUVentana()
    vu.show()
    sys.exit(app.exec_())

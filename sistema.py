import os
import sys
import re
import subprocess
import pymysql
import random
import time
import datetime
import reportlab
import sistema
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
import xlsxwriter
import openpyxl
import pandas
# --------------------------------------
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtMultimedia import *
# --------------------------------------
import registroADM
import registroUSER
import login
import cajero
import medicamentosDB
# ---------------------------------------------------------------------------------------
# Ventana de Sistema QMainWIndow


class Ventana(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)

        self.setWindowTitle("Sistema de Ventas")
        self.setWindowIcon(QIcon("icon/Pumps-48.png"))
        self.setFixedSize(1024, 640)
        self.move(QDesktopWidget().availableGeometry().center()
                  - self.frameGeometry().center())
        self.labelFondo = QLabel(self)
        self.labelFondo.setPixmap(QPixmap("icon/wallpaper.jpg"))
        self.labelFondo.setGeometry(0, 0, 1024, 640)
        self.toolBar()
        self.mBar()

        self.stackwidgetPRO()
        self.ventas()
        self.generadorDeRecibo()
        self.buscardorDeM()
        self.inventario()
        self.diagramaLineal()
        self.libroControl()
# ---------------------------------------------------------------------------------------
# MenuBar

    def mBar(self):
        bar = self.menuBar()
        bar.setStyleSheet("background-color: rgba(255, 255, 255, 50%);")
        Iinicio = bar.addMenu("Inicio")

        rUser = QAction(QIcon("icon/user_group_man_woman-48.png"), "Registrar Admin", self)
        rUser.setShortcut('Ctrl+R')
        rUser.triggered.connect(self.registerA)

        rDes = QAction(QIcon("icon/Disconnected-48.png"), "Desconectar", self)
        rDes.setShortcut('Ctrl+D')
        rDes.triggered.connect(self.desconectar)

        Iinicio.addActions((rUser, rDes))

    def registerA(self):
        vra = registroADM.RAVentana(self)
        vra.show()
# ---------------------------------------------------------------------------------------
# ToolBar

    def toolBar(self):
        ventas = QAction(QIcon("icon/cash_register-48.png"), "Vender medicamento", self)
        ventas.triggered.connect(self.pagina1)
        buscador = QAction(QIcon("icon/Search-48.png"), "Buscar medicamento", self)
        buscador.triggered.connect(self.pagina2)
        resivo = QAction(QIcon("icon/resume-48.png"), "Resivo de pago", self)
        resivo.triggered.connect(self.pagina3)
        clienteR = QAction(QIcon("icon/user_group_man_woman-48.png"), "Registro de cliente", self)
        clienteR.triggered.connect(self.registerU)
        mayorista = QAction(QIcon("icon/Permanent_Job-48.png"), "Mayorista", self)
        mayorista.triggered.connect(self.pagina4)
        inventario = QAction(QIcon("icon/clipboard-48"), "Inventario", self)
        inventario.triggered.connect(self.pagina5)
        diagrama = QAction(QIcon("icon/combo_chart-48.png"), "Diagrama", self)
        diagrama.triggered.connect(self.pagina6)
        libroAuxiliar = QAction(QIcon("icon/dossier_folder-48.png"), "Libro de Control", self)
        libroAuxiliar.triggered.connect(self.pagina7)
        salir = QAction(QIcon("icon/Exit-48.png"), "Salir", self)
        salir.triggered.connect(self.desconectar)

        self.toolBar = QToolBar(self)
        #self.toolBar.setStyleSheet("background: rgb(255, 255, 255, 0)")
        self.toolBar.setOrientation(Qt.Vertical)
        self.addToolBar(Qt.LeftToolBarArea, self.toolBar)
        self.toolBar.setIconSize(QSize(50, 50))
        self.toolBar.addActions((ventas,
                                 buscador,
                                 resivo,
                                 clienteR,
                                 mayorista,
                                 inventario,
                                 diagrama,
                                 libroAuxiliar,
                                 salir))
# ---------------------------------------------------------------------------------------
# Sistema

    def ventas(self):
        # Buscas Usuario
        self.label = QLabel("[CEDULA]", self.page)
        self.label.setGeometry(90, 36, 120, 13)

        self.line = QLineEdit(self.page)
        self.line.setAlignment(Qt.AlignCenter)
        self.line.setPlaceholderText("Cedula.....")
        self.line.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.line.setGeometry(90, 50, 120, 20)
        self.line.textChanged.connect(self.ValidarCC)

        self.boton = QPushButton(self.page)
        self.boton.setGeometry(220, 43, 30, 30)
        self.boton.setIcon(QIcon(QPixmap("icon/Search-48.png")))
        self.boton.setIconSize(QSize(30, 30))
        self.boton.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.boton.setFlat(True)
        self.boton.clicked.connect(self.buscarClienteEnDB)

        self.labell = QLabel(self.page)
        self.labell.setPixmap(QPixmap("icon/ID_Card-48.png"))
        self.labell.move(30, 35)

        # Buscar Medicamento
        self.labell = QLabel("[COD.BARRA]", self.page)
        self.labell.setGeometry(90, 85, 110, 13)

        self.linel = QLineEdit(self.page)
        self.linel.setAlignment(Qt.AlignCenter)
        self.linel.setPlaceholderText("Codigo de Barra.....")
        self.linel.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.linel.setGeometry(90, 100, 120, 20)
        self.linel.textChanged.connect(self.ValidarBarra)

        self.boton1 = QPushButton(self.page)
        self.boton1.setGeometry(220, 93, 30, 30)
        self.boton1.setIcon(QIcon(QPixmap("icon/Search-48.png")))
        self.boton1.setIconSize(QSize(30, 60))
        self.boton1.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.boton1.setFlat(True)
        self.boton1.clicked.connect(self.buscarMedicamento)

        self.label55 = QLabel(self.page)
        self.label55.setPixmap(QPixmap("icon/barcode_scanner-48.png"))
        self.label55.move(30, 80)

        # Inforamcion del Usuario
        self.label1 = QLabel("[NOMBRE]", self.page)
        self.label1.move(415, 37)

        self.line1 = QLineEdit(self.page)
        self.line1.setEnabled(False)
        self.line1.setAlignment(Qt.AlignCenter)
        self.line1.setGeometry(325, 50, 230, 20)

        self.label2 = QLabel("[CIUDAD]", self.page)
        self.label2.move(619, 37)

        self.line2 = QLineEdit(self.page)
        self.line2.setEnabled(False)
        self.line2.setAlignment(Qt.AlignCenter)
        self.line2.setGeometry(580, 50, 135, 20)

        # Inforamcion del Usuario
        self.label3 = QLabel("[TELEFONO]", self.page)
        self.label3.move(385, 87)

        self.line3 = QLineEdit(self.page)
        self.line3.setEnabled(False)
        self.line3.setAlignment(Qt.AlignCenter)
        self.line3.setGeometry(325, 100, 90, 20)

        self.label33 = QLabel(" - ", self.page)
        self.label33.move(416, 100)

        self.line33 = QLineEdit(self.page)
        self.line33.setEnabled(False)
        self.line33.setAlignment(Qt.AlignCenter)
        self.line33.setGeometry(430, 100, 90, 20)

        self.label4 = QLabel("[DIRECCION]", self.page)
        self.label4.move(612, 87)
        self.line4 = QLineEdit(self.page)
        self.line4.setEnabled(False)
        self.line4.setAlignment(Qt.AlignCenter)
        self.line4.setGeometry(580, 100, 135, 20)

        self.label5 = QLabel("[PRODUCTO]", self.page)
        self.label5.move(60, 160)
        self.line5 = QLineEdit(self.page)
        self.line5.setEnabled(False)
        self.line5.setStyleSheet("color: black;")
        self.line5.setGeometry(60, 173, 350, 20)

        self.line6 = QLineEdit(self.page)
        self.line6.setEnabled(False)
        self.line6.setAlignment(Qt.AlignCenter)
        self.line6.setGeometry(438, 173, 30, 20)

        self.labelff = QLabel("[F.FABRICACION]", self.page)
        self.labelff.move(500, 160)
        self.lineff = QLineEdit(self.page)
        self.lineff.setEnabled(False)
        self.lineff.setAlignment(Qt.AlignCenter)
        self.lineff.setGeometry(500, 173, 100, 20)

        self.labelfv = QLabel("[F.VENCIMIENTO]", self.page)
        self.labelfv.move(500, 210)
        self.linefv = QLineEdit(self.page)
        self.linefv.setEnabled(False)
        self.linefv.setAlignment(Qt.AlignCenter)
        self.linefv.setGeometry(500, 223, 100, 20)

        self.label7 = QLabel("[LABORATOIO]", self.page)
        self.label7.move(60, 210)
        self.line7 = QLineEdit(self.page)
        self.line7.setEnabled(False)
        self.line7.setGeometry(60, 223, 300, 20)

        self.label8 = QLabel("[VIA]", self.page)
        self.label8.move(415, 210)
        self.line8 = QLineEdit(self.page)
        self.line8.setAlignment(Qt.AlignCenter)
        self.line8.setEnabled(False)
        self.line8.setGeometry(365, 223, 130, 20)

        self.labeUR = QLabel("UNIDADES RESTANTES:", self.page)
        self.labeUR.move(340, 258)
        self.lineUR = QLineEdit(self.page)
        self.lineUR.setAlignment(Qt.AlignCenter)
        self.lineUR.setEnabled(False)
        self.lineUR.setGeometry(495, 253, 50, 20)

        self.labelFoto = QLabel("FOTO", self.page)
        self.labelFoto.setAlignment(Qt.AlignCenter)
        self.labelFoto.setGeometry(610, 170, 100, 100)
        self.labelFoto.setStyleSheet("border: 2px solid white;")

        self.labelU = QLabel("[UNIDAD]", self.page)
        self.labelU.move(68, 263)
        self.lineU = QLineEdit(self.page)
        self.lineU.setEnabled(False)
        self.lineU.setAlignment(Qt.AlignCenter)
        self.lineU.setGeometry(60, 275, 70, 20)

        self.labelT = QLabel("[TABLETA]", self.page)
        self.labelT.move(153, 263)
        self.lineT = QLineEdit(self.page)
        self.lineT.setEnabled(False)
        self.lineT.setAlignment(Qt.AlignCenter)
        self.lineT.setGeometry(150, 275, 70, 20)

        self.labelC = QLabel("[CAJA]", self.page)
        self.labelC.move(260, 263)
        self.lineC = QLineEdit(self.page)
        self.lineC.setEnabled(False)
        self.lineC.setAlignment(Qt.AlignCenter)
        self.lineC.setGeometry(245, 275, 70, 20)

        self.labelB = QLabel("[BOLSA]", self.page)
        self.labelB.move(110, 300)
        self.lineB = QLineEdit(self.page)
        self.lineB.setEnabled(False)
        self.lineB.setAlignment(Qt.AlignCenter)
        self.lineB.setGeometry(100, 313, 70, 20)

        self.labelFR = QLabel("[FRASCO]", self.page)
        self.labelFR.move(183, 300)
        self.lineFR = QLineEdit(self.page)
        self.lineFR.setEnabled(False)
        self.lineFR.setAlignment(Qt.AlignCenter)
        self.lineFR.setGeometry(180, 313, 70, 20)

        self.label10 = QLabel("[PRECIO]", self.page)
        self.label10.move(80, 340)
        self.line10 = QLineEdit(self.page)
        self.line10.setEnabled(False)
        self.line10.setAlignment(Qt.AlignCenter)
        self.line10.setGeometry(60, 353, 90, 20)

        self.label9 = QLabel(self.page)
        self.label9.setGeometry(175, 337, 80, 20)
        self.line9 = QLineEdit(self.page)
        self.line9.setEnabled(False)
        self.line9.setAlignment(Qt.AlignCenter)
        self.line9.setGeometry(170, 353, 40, 20)
        self.line9.textChanged.connect(self.cambiarPresioDeVenta)

        self.labelC = QLabel("[CANTIDAD]", self.page)
        self.labelC.move(240, 340)

        self.combo = QComboBox(self.page)
        self.combo.move(235, 353)
        self.combo.addItems(("OTROS",
                             "1",
                             "2",
                             "3",
                             "4",
                             "5",
                             "TABLETA",
                             "CAJA",
                             "BOLSA",
                             "FRASCO", ))
        self.combo.currentIndexChanged.connect(self.elegirTipoDeVenta)

        self.lista = QTextEdit(self.page)
        self.lista.setGeometry(330, 280, 380, 100)

        self.boton2 = QPushButton(self.page)
        self.boton2.setGeometry(60, 375, 50, 50)
        self.boton2.setIcon(QIcon(QPixmap("icon/Shopping_Cart-Loaded-48.png")))
        self.boton2.setIconSize(QSize(50, 50))
        self.boton2.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.boton2.setFlat(True)
        self.boton2.clicked.connect(self.insertarVenta)

        self.labelLogo = QLabel(self.page)
        self.labelLogo.setGeometry(740, -50, 300, 300)
        self.labelLogo.setPixmap(QPixmap("icon/logo.png"))

        self.boton3 = QPushButton(self.page)
        self.boton3.setGeometry(770, 425, 150, 150)
        self.boton3.setIcon(QIcon(QPixmap("icon/dinero-icono-01.png")))
        self.boton3.setIconSize(QSize(150, 150))
        self.boton3.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.boton3.setFlat(True)
        self.boton3.clicked.connect(self.cajeroV)

        self.boton4 = QPushButton(self.page)
        self.boton4.setGeometry(55, 563, 50, 50)
        self.boton4.setIcon(QIcon(QPixmap("icon/Empty_Trash-48.png")))
        self.boton4.setIconSize(QSize(50, 50))
        self.boton4.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.boton4.setFlat(True)
        self.boton4.clicked.connect(self.borrarVentas)

        self.table = QTableWidget(0, 6, self.page)
        self.table.setGeometry(60, 420, 650, 150)
        self.table.setHorizontalHeaderLabels(
            ['CODIGO', 'NOMBRE', 'FABRICANTE', 'CANTIDAD', 'TIPO', 'PRECIO'])
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.table.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.verticalHeader().setDefaultSectionSize(18)                 # Vertical height of rows
        self.table.setShowGrid(True)
        self.table.setDragDropOverwriteMode(False)
        self.table.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.table.setStyleSheet(
            "QTableView {background-color: #CEECF5;selection-background-color: #FF0000;}")

        self.label11 = QLabel("TOTAL: $ ", self.page)
        self.label11.move(590, 570)
        self.label11.setStyleSheet("color: red")

        self.labelT = QLabel(self.page)
        self.labelT.setStyleSheet("color: red")
        self.labelT.setGeometry(650, 552, 100, 50)
        self.labelT.setText("0")

        self.l2 = QLabel(self.page)
        self.l2.setGeometry(38, 10, 300, 20)

        self.num = 0
        self.suma = 0
        self.contar = 0

    def ValidarCC(self):
        validar = re.match('^[0-9\sáéíóúàèìòùäëïöüñ]+$', self.line.text(), re.I)
        if self.line.text() == "":
            self.line.setStyleSheet("border: 2px solid yellow;")
            self.l2.setText("")
            return False
        elif not validar:
            self.line.setStyleSheet("border: 2px solid red;")
            self.l2.setText("La casilla de cliente, solo se permite numeros.")
            self.l2.setStyleSheet("color: red")
            return False
        else:
            self.line.setStyleSheet("border: 2px solid cyan;")
            self.l2.setText("")
            return True

    def ValidarBarra(self):
        validar = re.match('^[0-9\sáéíóúàèìòùäëïöüñ]+$', self.linel.text(), re.I)
        if self.linel.text() == "":
            self.linel.setStyleSheet("border: 2px solid yellow;")
            self.l2.setText("")
            return False
        elif not validar:
            self.linel.setStyleSheet("border: 2px solid red;")
            self.l2.setText("La casilla codigo barra, solo se permite numeros")
            self.l2.setStyleSheet("color: red")
            return False
        else:
            self.linel.setStyleSheet("border: 2px solid cyan;")
            self.l2.setText("")
            return True

    def buscarClienteEnDB(self):
        if self.ValidarCC():
            if len(self.line.text()) == 0:
                QMessageBox.warning(self, "Informacion",
                                    "Ingrese la cedula del cliente!", QMessageBox.Ok)
                self.line.setStyleSheet("border: 1px solid yellow;")
                pass
            else:
                try:
                    db = pymysql.connect(host="localhost", user="root",
                                         passwd="Lb104572", db="cliente")
                    query = db.cursor()
                    if (query.execute("SELECT * FROM Registros WHERE Codigo='" + self.line.text() + "'")):
                        db.commit()
                        for row in query:
                            self.line1.setText(row[1])
                            self.line2.setText(row[2])
                            self.line3.setText(row[4])
                            self.line33.setText(row[5])
                            self.line4.setText(row[3])
                            self.line1.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.line2.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.line3.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.line33.setStyleSheet("border: 2px solid green;font-size: 12px;color: rgb(0,0,0)")
                            self.line4.setStyleSheet("border: 2px solid green;font-size: 12px;color: rgb(0,0,0)")

                    else:
                        QMessageBox.warning(self, 'Warning', 'La cedula no existe.', QMessageBox.Ok)
                        self.line.setText("")
                        self.line1.setText("")
                        self.line2.setText("")
                        self.line3.setText("")
                        self.line33.setText("")
                        self.line4.setText("")
                        self.line1.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.line2.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.line3.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.line33.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.line4.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")

                except:
                    QMessageBox.critical(
                        self, 'Fallo', 'Error al conectar con la db.', QMessageBox.Ok)
        else:
            QMessageBox.warning(self, "Informacion", "No se permiten letras", QMessageBox.Ok)

    def buscarMedicamento(self):
        if self.ValidarBarra():
            if len(self.linel.text()) == 0:
                QMessageBox.information(self, "Informacion", "Ingrese un codigo en el medicamento!",
                                        QMessageBox.Ok)
                self.linel.setStyleSheet("border: 1px solid yellow;")
                pass
            else:
                try:
                    db = pymysql.connect(host="localhost", user="root",
                                         passwd="Lb104572", db="inventario")
                    query = db.cursor()
                    if (query.execute("SELECT * FROM Almacen WHERE CodigoBarra='" + self.linel.text() + "'")):
                        db.commit()
                        for row in query:
                            self.line5.setText(row[2])  # Nombre
                            self.line6.setText(row[7])  # caja
                            self.line7.setText(row[3])  # laboratio
                            self.line8.setText(row[9])   # via
                            self.lineff.setText(row[5])  # f fabricacion
                            self.linefv.setText(row[6])  # f vencimiento
                            self.lineUR.setText(str(row[12]))  # unidades

                            self.lineU.setText(str(row[18]))  # presio Unidad
                            self.lineT.setText(str(row[19]))  # presio Tableta
                            self.lineC.setText(str(row[20]))  # Presio Caja
                            self.lineB.setText(str(row[21]))  # presio Bolsa
                            self.lineFR.setText(str(row[22]))  # Presio Fraco
                            self.line10.setText(str(row[17]))  # Presio Cliente
                            self.lista.setText(row[23])

                            image_data = row[24]
                            qimg = QImage.fromData(image_data)
                            pix = QPixmap.fromImage(qimg)
                            pixmap = QPixmap(pix.scaledToWidth(150))
                            self.labelFoto.setPixmap(pixmap)

                            self.line5.setStyleSheet("border: 2px solid green; font-size: 15px;color: rgb(0,0,0)")
                            self.line6.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.line7.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.line8.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.lineff.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.linefv.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.lineU.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.lineT.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.lineC.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.line10.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.lista.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.lineB.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.lineFR.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.labelFoto.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                            self.lineUR.setStyleSheet("border: 2px solid green;font-size: 15px;color: rgb(0,0,0)")
                    else:
                        QMessageBox.warning(self.page, 'Warning',
                                            'Codigo del barra existe.', QMessageBox.Ok)

                        self.line5.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.line6.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.line7.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.line8.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.lineff.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.linefv.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.lineU.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.lineT.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.lineC.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.line10.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.lista.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.lineB.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.lineFR.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")
                        self.labelFoto.setStyleSheet("border: 2px solid yellow;font-size: 15px;color: rgb(0,0,0)")

                        self.linel.setText("")
                        self.line5.setText("")  # Nombre
                        self.line6.setText("")  # caja
                        self.line7.setText("")  # laboratio
                        self.line8.setText("")   # via
                        self.lineff.setText("")  # f fabricacion
                        self.linefv.setText("")  # f vencimiento

                        self.lineU.setText("")  # presio Unidad
                        self.lineT.setText("")  # presio Tableta
                        self.lineC.setText("")  # Presio Caja
                        self.lineB.setText("")  # presio Bolsa
                        self.lineFR.setText("")  # Presio Fraco
                        self.line10.setText("")  # Presio Cliente
                        self.lista.setText("")
                        self.labelFoto.setText("Foto")

                except:
                    QMessageBox.critical(self.page, 'Fallo',
                                         'Error al conectar con la DB.', QMessageBox.Ok)
        else:
            QMessageBox.warning(self, "Informacion", "No se permiten letras", QMessageBox.Ok)

    def elegirTipoDeVenta(self):
        try:
            if self.lineU.text() == "":
                QMessageBox.information(self, 'Informacion',
                                        'No existe ningun valor.', QMessageBox.Ok)
            elif self.lineT.text() == "":
                QMessageBox.information(self, 'Informacion',
                                        'No existe ningun valor.', QMessageBox.Ok)
            elif self.lineC.text() == "":
                QMessageBox.information(self, 'Informacion',
                                        'No existe ningun valor.', QMessageBox.Ok)
            elif self.lineB.text() == "":
                QMessageBox.information(self, 'Informacion',
                                        'No existe ningun valor.', QMessageBox.Ok)
            elif self.lineFR.text() == "":
                QMessageBox.information(self, 'Informacion',
                                        'No existe ningun valor.', QMessageBox.Ok)
            else:
                if self.combo.currentText() == "OTROS":
                    self.label9.setText("[UNI]")
                    self.line9.setEnabled(True)
                    self.line9.setText("1")
                    ot = int(self.line9.text())
                    totalT = float(self.lineT.text()) * ot
                    self.line10.setText(str(totalT))
                    self.line9.setStyleSheet("border: 2px solid green;")
                elif self.combo.currentText() == "1":
                    self.label9.setText("[UNI]")
                    self.line9.setEnabled(False)
                    self.line9.setText("1")
                    x1 = 1
                    total = float(self.lineU.text()) * x1
                    self.line10.setText(str(total))
                    self.line9.setStyleSheet("border: 2px solid green;")
                elif self.combo.currentText() == "2":
                    self.label9.setText("[UNI]")
                    self.line9.setText("2")
                    self.line9.setEnabled(False)
                    x2 = 2
                    total2 = float(self.lineU.text()) * x2
                    self.line10.setText(str(total2))
                    self.line9.setStyleSheet("border: 2px solid green;")
                elif self.combo.currentText() == "3":
                    self.label9.setText("[UNI]")
                    self.line9.setText("3")
                    self.line9.setEnabled(False)
                    x3 = 3
                    total3 = float(self.lineU.text()) * x3
                    self.line10.setText(str(total3))
                    self.line9.setStyleSheet("border: 2px solid green;")
                elif self.combo.currentText() == "4":
                    self.label9.setText("[UNI]")
                    self.line9.setText("4")
                    self.line9.setEnabled(False)
                    x4 = 4
                    total4 = float(self.lineU.text()) * x4
                    self.line10.setText(str(total4))
                    self.line9.setStyleSheet("border: 2px solid green;")
                elif self.combo.currentText() == "5":
                    self.label9.setText("[UNI]")
                    self.line9.setText("5")
                    self.line9.setEnabled(False)
                    x5 = 5
                    total5 = float(self.lineU.text()) * x5
                    self.line10.setText(str(total5))
                    self.line9.setStyleSheet("border: 2px solid green;")
                elif self.combo.currentText() == "TABLETA":
                    self.label9.setText("[TAB]")
                    self.line9.setEnabled(True)
                    self.line9.setText("1")
                    T = int(self.line9.text())
                    totalT = float(self.lineT.text()) * T
                    self.line10.setText(str(totalT))
                    self.line9.setStyleSheet("border: 2px solid green;")
                elif self.combo.currentText() == "CAJA":
                    self.label9.setText("[CAJ]")
                    self.line9.setEnabled(True)
                    self.line9.setText("1")
                    C = int(self.line9.text())
                    totalC = float(self.lineC.text()) * C
                    self.line10.setText(str(totalC))
                    self.line9.setStyleSheet("border: 2px solid green;")
                elif self.combo.currentText() == "BOLSA":
                    self.label9.setText("[BOL]")
                    self.line9.setEnabled(True)
                    self.line9.setText("1")
                    BL = int(self.line9.text())
                    totalBL = float(self.lineB.text()) * BL
                    self.line10.setText(str(totalBL))
                    self.line9.setStyleSheet("border: 2px solid green;")
                elif self.combo.currentText() == "FRASCO":
                    self.label9.setText("[FRA]")
                    self.line9.setEnabled(True)
                    self.line9.setText("1")
                    FR = int(self.line9.text())
                    totalFR = float(self.lineFR.text()) * FR
                    self.line10.setText(str(totalFR))
                    self.line9.setStyleSheet("border: 2px solid green;")

        except:
            QMessageBox.critical(
                self, 'Fallo', 'Fallo al selecionar un tipo de venta', QMessageBox.Ok)

    def cambiarPresioDeVenta(self):
        validar = re.match('^[0-9\sáéíóúàèìòùäëïöüñ]+$', self.line9.text(), re.I)

        if self.line9.text() == "":
            self.line9.setStyleSheet("border: 2px solid red;")
            self.line10.setStyleSheet("border: 2px solid red;")
            return False
        elif not validar:
            self.line9.setStyleSheet("border: 2px solid red;")
            return False
        else:
            if self.combo.currentText() == "TABLETA":
                T = int(self.line9.text())
                totalT = float(self.lineT.text()) * T
                self.line10.setText(str(totalT))
                self.line10.setStyleSheet("border: 2px solid green;")
                self.line9.setStyleSheet("border: 2px solid green;")
            elif self.combo.currentText() == "CAJA":
                C = int(self.line9.text())
                totalC = float(self.lineC.text()) * C
                self.line10.setText(str(totalC))
                self.line10.setStyleSheet("border: 2px solid green;")
                self.line9.setStyleSheet("border: 2px solid green;")
            elif self.combo.currentText() == "BOLSA":
                BL = int(self.line9.text())
                totalBL = float(self.lineB.text()) * BL
                self.line10.setText(str(totalBL))
                self.line10.setStyleSheet("border: 2px solid green;")
                self.line9.setStyleSheet("border: 2px solid green;")
            elif self.combo.currentText() == "FRASCO":
                FR = int(self.line9.text())
                totalFR = float(self.lineFR.text()) * FR
                self.line10.setText(str(totalFR))
                self.line10.setStyleSheet("border: 3px solid green;")

    def insertarVenta(self):
        if self.num > 19:
            QMessageBox.warning(self, 'Warning', 'Solo se permite 20 productos.', QMessageBox.Ok)
        else:
            try:
                total = float(self.line10.text())
                self.table.insertRow(self.table.rowCount())
                self.table.setItem(self.num, 0, QTableWidgetItem(self.linel.text()))
                self.table.setItem(self.num, 1, QTableWidgetItem(self.line5.text()))
                self.table.setItem(self.num, 2, QTableWidgetItem(self.line7.text()))
                self.table.setItem(self.num, 3, QTableWidgetItem(self.line9.text()))
                self.table.setItem(self.num, 4, QTableWidgetItem(self.label9.text()))
                self.table.setItem(self.num, 5, QTableWidgetItem(str(total)))

                self.valor = total

                self.num += 1
                self.suma = self.suma + self.valor
                self.labelT.setText(str(self.suma))
            except:
                QMessageBox.information(self, 'Informacion',
                                        'Por favor ingrese almenos una venta.', QMessageBox.Ok)

    def borrarVentas(self):
        for i in reversed(range(self.table.rowCount())):
            self.table.removeRow(i)
            self.num = 0
            self.labelT.setText("0")
            self.suma = 0

    def cajeroV(self):
        if self.labelT.text() == "0":
            QMessageBox.information(self, 'Informacion',
                                    'No se ha insertado una venta a un', QMessageBox.Ok)
        else:
            if self.table.rowCount() > 0:
                codigo0_0 = self.table.item(0, 0).text()
                codigo0_1 = self.table.item(0, 1).text()
                codigo0_3 = self.table.item(0, 3).text()
                codigo0_4 = self.table.item(0, 4).text()
                codigo0_5 = self.table.item(0, 5).text()
            else:
                codigo0_0 = ""
                codigo0_1 = ""
                codigo0_3 = ""
                codigo0_4 = ""
                codigo0_5 = ""
            if self.table.rowCount() > 1:
                codigo1_0 = self.table.item(1, 0).text()
                codigo1_1 = self.table.item(1, 1).text()
                codigo1_3 = self.table.item(1, 3).text()
                codigo1_4 = self.table.item(1, 4).text()
                codigo1_5 = self.table.item(1, 5).text()
            else:
                codigo1_0 = ""
                codigo1_1 = ""
                codigo1_3 = ""
                codigo1_4 = ""
                codigo1_5 = ""
            if self.table.rowCount() > 2:
                codigo2_0 = self.table.item(2, 0).text()
                codigo2_1 = self.table.item(2, 1).text()
                codigo2_3 = self.table.item(2, 3).text()
                codigo2_4 = self.table.item(2, 4).text()
                codigo2_5 = self.table.item(2, 5).text()
            else:
                codigo2_0 = ""
                codigo2_1 = ""
                codigo2_3 = ""
                codigo2_4 = ""
                codigo2_5 = ""
            if self.table.rowCount() > 3:
                codigo3_0 = self.table.item(3, 0).text()
                codigo3_1 = self.table.item(3, 1).text()
                codigo3_3 = self.table.item(3, 3).text()
                codigo3_4 = self.table.item(3, 4).text()
                codigo3_5 = self.table.item(3, 5).text()
            else:
                codigo3_0 = ""
                codigo3_1 = ""
                codigo3_3 = ""
                codigo3_4 = ""
                codigo3_5 = ""
            if self.table.rowCount() > 4:
                codigo4_0 = self.table.item(4, 0).text()
                codigo4_1 = self.table.item(4, 1).text()
                codigo4_3 = self.table.item(4, 3).text()
                codigo4_4 = self.table.item(4, 4).text()
                codigo4_5 = self.table.item(4, 5).text()
            else:
                codigo4_0 = ""
                codigo4_1 = ""
                codigo4_3 = ""
                codigo4_4 = ""
                codigo4_5 = ""
            if self.table.rowCount() > 5:
                codigo5_0 = self.table.item(5, 0).text()
                codigo5_1 = self.table.item(5, 1).text()
                codigo5_3 = self.table.item(5, 3).text()
                codigo5_4 = self.table.item(5, 4).text()
                codigo5_5 = self.table.item(5, 5).text()
            else:
                codigo5_0 = ""
                codigo5_1 = ""
                codigo5_3 = ""
                codigo5_4 = ""
                codigo5_5 = ""
            if self.table.rowCount() > 6:
                codigo6_0 = self.table.item(6, 0).text()
                codigo6_1 = self.table.item(6, 1).text()
                codigo6_3 = self.table.item(6, 3).text()
                codigo6_4 = self.table.item(6, 4).text()
                codigo6_5 = self.table.item(6, 5).text()
            else:
                codigo6_0 = ""
                codigo6_1 = ""
                codigo6_3 = ""
                codigo6_4 = ""
                codigo6_5 = ""
            if self.table.rowCount() > 7:
                codigo7_0 = self.table.item(7, 0).text()
                codigo7_1 = self.table.item(7, 1).text()
                codigo7_3 = self.table.item(7, 3).text()
                codigo7_4 = self.table.item(7, 4).text()
                codigo7_5 = self.table.item(7, 5).text()
            else:
                codigo7_0 = ""
                codigo7_1 = ""
                codigo7_3 = ""
                codigo7_4 = ""
                codigo7_5 = ""
            if self.table.rowCount() > 8:
                codigo8_0 = self.table.item(8, 0).text()
                codigo8_1 = self.table.item(8, 1).text()
                codigo8_3 = self.table.item(8, 3).text()
                codigo8_4 = self.table.item(8, 4).text()
                codigo8_5 = self.table.item(8, 5).text()
            else:
                codigo8_0 = ""
                codigo8_1 = ""
                codigo8_3 = ""
                codigo8_4 = ""
                codigo8_5 = ""
            if self.table.rowCount() > 9:
                codigo9_0 = self.table.item(9, 0).text()
                codigo9_1 = self.table.item(9, 1).text()
                codigo9_3 = self.table.item(9, 3).text()
                codigo9_4 = self.table.item(9, 4).text()
                codigo9_5 = self.table.item(9, 5).text()
            else:
                codigo9_0 = ""
                codigo9_1 = ""
                codigo9_3 = ""
                codigo9_4 = ""
                codigo9_5 = ""
            if self.table.rowCount() > 10:
                codigo10_0 = self.table.item(10, 0).text()
                codigo10_1 = self.table.item(10, 1).text()
                codigo10_3 = self.table.item(10, 3).text()
                codigo10_4 = self.table.item(10, 4).text()
                codigo10_5 = self.table.item(10, 5).text()
            else:
                codigo10_0 = ""
                codigo10_1 = ""
                codigo10_3 = ""
                codigo10_4 = ""
                codigo10_5 = ""
            if self.table.rowCount() > 11:
                codigo11_0 = self.table.item(11, 0).text()
                codigo11_1 = self.table.item(11, 1).text()
                codigo11_3 = self.table.item(11, 3).text()
                codigo11_4 = self.table.item(11, 4).text()
                codigo11_5 = self.table.item(11, 5).text()
            else:
                codigo11_0 = ""
                codigo11_1 = ""
                codigo11_3 = ""
                codigo11_4 = ""
                codigo11_5 = ""
            if self.table.rowCount() > 12:
                codigo12_0 = self.table.item(12, 0).text()
                codigo12_1 = self.table.item(12, 1).text()
                codigo12_3 = self.table.item(12, 3).text()
                codigo12_4 = self.table.item(12, 4).text()
                codigo12_5 = self.table.item(12, 5).text()
            else:
                codigo12_0 = ""
                codigo12_1 = ""
                codigo12_3 = ""
                codigo12_4 = ""
                codigo12_5 = ""
            if self.table.rowCount() > 13:
                codigo13_0 = self.table.item(13, 0).text()
                codigo13_1 = self.table.item(13, 1).text()
                codigo13_3 = self.table.item(13, 3).text()
                codigo13_4 = self.table.item(13, 4).text()
                codigo13_5 = self.table.item(13, 5).text()
            else:
                codigo13_0 = ""
                codigo13_1 = ""
                codigo13_3 = ""
                codigo13_4 = ""
                codigo13_5 = ""
            if self.table.rowCount() > 14:
                codigo14_0 = self.table.item(14, 0).text()
                codigo14_1 = self.table.item(14, 1).text()
                codigo14_3 = self.table.item(14, 3).text()
                codigo14_4 = self.table.item(14, 4).text()
                codigo14_5 = self.table.item(14, 5).text()
            else:
                codigo14_0 = ""
                codigo14_1 = ""
                codigo14_3 = ""
                codigo14_4 = ""
                codigo14_5 = ""
            if self.table.rowCount() > 15:
                codigo15_0 = self.table.item(15, 0).text()
                codigo15_1 = self.table.item(15, 1).text()
                codigo15_3 = self.table.item(15, 3).text()
                codigo15_4 = self.table.item(15, 4).text()
                codigo15_5 = self.table.item(15, 5).text()
            else:
                codigo15_0 = ""
                codigo15_1 = ""
                codigo15_3 = ""
                codigo15_4 = ""
                codigo15_5 = ""
            if self.table.rowCount() > 16:
                codigo16_0 = self.table.item(16, 0).text()
                codigo16_1 = self.table.item(16, 1).text()
                codigo16_3 = self.table.item(16, 3).text()
                codigo16_4 = self.table.item(16, 4).text()
                codigo16_5 = self.table.item(16, 5).text()
            else:
                codigo16_0 = ""
                codigo16_1 = ""
                codigo16_3 = ""
                codigo16_4 = ""
                codigo16_5 = ""
            if self.table.rowCount() > 17:
                codigo17_0 = self.table.item(17, 0).text()
                codigo17_1 = self.table.item(17, 1).text()
                codigo17_3 = self.table.item(17, 3).text()
                codigo17_4 = self.table.item(17, 4).text()
                codigo17_5 = self.table.item(17, 5).text()
            else:
                codigo17_0 = ""
                codigo17_1 = ""
                codigo17_3 = ""
                codigo17_4 = ""
                codigo17_5 = ""
            if self.table.rowCount() > 18:
                codigo18_0 = self.table.item(18, 0).text()
                codigo18_1 = self.table.item(18, 1).text()
                codigo18_3 = self.table.item(18, 3).text()
                codigo18_4 = self.table.item(18, 4).text()
                codigo18_5 = self.table.item(18, 5).text()
            else:
                codigo18_0 = ""
                codigo18_1 = ""
                codigo18_3 = ""
                codigo18_4 = ""
                codigo18_5 = ""
            if self.table.rowCount() > 19:
                codigo19_0 = self.table.item(19, 0).text()
                codigo19_1 = self.table.item(19, 1).text()
                codigo19_3 = self.table.item(19, 3).text()
                codigo19_4 = self.table.item(19, 4).text()
                codigo19_5 = self.table.item(19, 5).text()
            else:
                codigo19_0 = ""
                codigo19_1 = ""
                codigo19_3 = ""
                codigo19_4 = ""
                codigo19_5 = ""
            if self.table.rowCount() > 20:
                codigo20_0 = self.table.item(20, 0).text()
                codigo20_1 = self.table.item(20, 1).text()
                codigo20_3 = self.table.item(20, 3).text()
                codigo20_4 = self.table.item(20, 4).text()
                codigo20_5 = self.table.item(20, 5).text()
            else:
                codigo20_0 = ""
                codigo20_1 = ""
                codigo20_3 = ""
                codigo20_4 = ""
                codigo20_5 = ""
            if self.table.rowCount() > 20:
                QMessageBox.information(
                    self, 'Informe', 'Solo puede vender 20 productos por recibo!', QMessageBox.Ok)
                QMessageBox.information(
                    self, 'Informe', 'Se borrara la informacion completa para evitar problemas!', QMessageBox.Ok)
                for i in reversed(range(self.table.rowCount())):
                    self.table.removeRow(i)
                    self.num = 0
                    self.labelT.setText("0")
                    self.suma = 0
            else:
                self.lista = [self.line1.text(),
                              self.line3.text(),
                              self.line33.text(),
                              self.line4.text(),
                              self.line.text(),

                              self.table.rowCount(),

                              codigo0_0,
                              codigo0_1,
                              codigo0_3,
                              codigo0_4,
                              codigo0_5,
                              codigo1_0,
                              codigo1_1,
                              codigo1_3,
                              codigo1_4,
                              codigo1_5,
                              codigo2_0,
                              codigo2_1,
                              codigo2_3,
                              codigo2_4,
                              codigo2_5,
                              codigo3_0,
                              codigo3_1,
                              codigo3_3,
                              codigo3_4,
                              codigo3_5,
                              codigo4_0,
                              codigo4_1,
                              codigo4_3,
                              codigo4_4,
                              codigo4_5,
                              codigo5_0,
                              codigo5_1,
                              codigo5_3,
                              codigo5_4,
                              codigo5_5,
                              codigo6_0,
                              codigo6_1,
                              codigo6_3,
                              codigo6_4,
                              codigo6_5,
                              codigo7_0,
                              codigo7_1,
                              codigo7_3,
                              codigo7_4,
                              codigo7_5,
                              codigo8_0,
                              codigo8_1,
                              codigo8_3,
                              codigo8_4,
                              codigo8_5,
                              codigo9_0,
                              codigo9_1,
                              codigo9_3,
                              codigo9_4,
                              codigo9_5,
                              codigo10_0,
                              codigo10_1,
                              codigo10_3,
                              codigo10_4,
                              codigo10_5,
                              codigo11_0,
                              codigo11_1,
                              codigo11_3,
                              codigo11_4,
                              codigo11_5,
                              codigo12_0,
                              codigo12_1,
                              codigo12_3,
                              codigo12_4,
                              codigo12_5,
                              codigo13_0,
                              codigo13_1,
                              codigo13_3,
                              codigo13_4,
                              codigo13_5,
                              codigo14_0,
                              codigo14_1,
                              codigo14_3,
                              codigo14_4,
                              codigo14_5,
                              codigo15_0,
                              codigo15_1,
                              codigo15_3,
                              codigo15_4,
                              codigo15_5,
                              codigo16_0,
                              codigo16_1,
                              codigo16_3,
                              codigo16_4,
                              codigo16_5,
                              codigo17_0,
                              codigo17_1,
                              codigo17_3,
                              codigo17_4,
                              codigo17_5,
                              codigo18_0,
                              codigo18_1,
                              codigo18_3,
                              codigo18_4,
                              codigo18_5,
                              codigo18_0,
                              codigo19_1,
                              codigo19_3,
                              codigo19_4,
                              codigo19_5,
                              self.labelT.text()]
                cajero.VentanaCajero(self.lista, self).exec_()
# ---------------------------------------------------------------------------------------
# Resibo

    def generadorDeRecibo(self):
        self.laGui = QLabel("Codigo de Guia:", self.page3)
        self.laGui.setGeometry(102, 36, 120, 13)

        self.liGui = QLineEdit(self.page3)
        self.liGui.setAlignment(Qt.AlignCenter)
        self.liGui.setPlaceholderText("Codigo de Barra")
        self.liGui.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.liGui.setGeometry(90, 50, 120, 20)

        self.b1 = QPushButton(self.page3)
        self.b1.setGeometry(220, 48, 30, 30)
        self.b1.setIcon(QIcon(QPixmap("icon/Search-48.png")))
        self.b1.setIconSize(QSize(30, 60))
        self.b1.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.b1.setFlat(True)
        self.b1.clicked.connect(self.codigoDeGuia)

        self.tb1 = QTableWidget(0, 25, self.page3)
        self.tb1.setGeometry(60, 100, 870, 200)
        self.tb1.setHorizontalHeaderLabels(['GUIA', 'NUMERO DE VENTA', 'CLIENTE', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7',
                                            'M8', 'M9', 'M10', 'M11', 'M12', 'M13', 'M14', 'M15', 'M16', 'M17', 'M18', 'M19', 'M20', 'TOTAL', 'FECHA'])
        self.tb1.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tb1.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tb1.horizontalHeader().setStretchLastSection(True)
        self.tb1.verticalHeader().setDefaultSectionSize(18)                 # Vertical height of rows
        self.tb1.setShowGrid(True)
        self.tb1.setDragDropOverwriteMode(False)
        self.tb1.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tb1.setStyleSheet(
            "QTableView {background-color: #CEECF5;selection-background-color: #FF0000;}")
        self.tb1.cellClicked.connect(self.generarRecibo)

        self.progress4 = QProgressBar(self.page3)
        self.progress4.setGeometry(60, 90, 870, 10)

        grop = QGroupBox("[GENERAR RECIBO]:", self.page3)
        grop.setGeometry(60, 320, 700, 270)

        self.laN = QLabel("Nombre del Cliente:", self.page3)
        self.laN.setGeometry(70, 340, 300, 20)

        self.laNV = QLabel("Numero de Venta:", self.page3)
        self.laNV.setGeometry(70, 360, 300, 20)

        self.laM1 = QLabel("M1", self.page3)
        self.laM1.setGeometry(70, 380, 300, 20)

        self.laM2 = QLabel("M2", self.page3)
        self.laM2.setGeometry(70, 400, 300, 20)

        self.laM3 = QLabel("M3", self.page3)
        self.laM3.setGeometry(70, 420, 300, 20)

        self.laM4 = QLabel("M4", self.page3)
        self.laM4.setGeometry(70, 440, 300, 20)

        self.laM5 = QLabel("M5", self.page3)
        self.laM5.setGeometry(70, 460, 300, 20)

        self.laM6 = QLabel("M6", self.page3)
        self.laM6.setGeometry(70, 480, 300, 20)

        self.laM7 = QLabel("M7", self.page3)
        self.laM7.setGeometry(70, 500, 300, 20)

        self.laM8 = QLabel("M8", self.page3)
        self.laM8.setGeometry(70, 520, 300, 20)

        self.laM9 = QLabel("M9", self.page3)
        self.laM9.setGeometry(70, 540, 300, 20)

        self.laM10 = QLabel("M10", self.page3)
        self.laM10.setGeometry(70, 560, 300, 20)

        self.laM11 = QLabel("M11", self.page3)
        self.laM11.setGeometry(450, 380, 300, 20)

        self.laM12 = QLabel("M12", self.page3)
        self.laM12.setGeometry(450, 400, 300, 20)

        self.laM13 = QLabel("M13", self.page3)
        self.laM13.setGeometry(450, 420, 300, 20)

        self.laM14 = QLabel("M14", self.page3)
        self.laM14.setGeometry(450, 440, 300, 20)

        self.laM15 = QLabel("M15", self.page3)
        self.laM15.setGeometry(450, 460, 300, 20)

        self.laM16 = QLabel("M16", self.page3)
        self.laM16.setGeometry(450, 480, 300, 20)

        self.laM17 = QLabel("M17", self.page3)
        self.laM17.setGeometry(450, 500, 300, 20)

        self.laM18 = QLabel("M18", self.page3)
        self.laM18.setGeometry(450, 520, 300, 20)

        self.laM19 = QLabel("M19", self.page3)
        self.laM19.setGeometry(450, 540, 300, 20)

        self.laM20 = QLabel("M20", self.page3)
        self.laM20.setGeometry(450, 560, 300, 20)

        self.laMT = QLabel("Total:", self.page3)
        self.laMT.setGeometry(660, 570, 150, 20)

        self.laMTotal = QLabel("", self.page3)
        self.laMTotal.setGeometry(700, 570, 150, 20)

        self.laMFecha = QLabel("Fecha", self.page3)
        self.laMFecha.setGeometry(620, 340, 150, 20)

        self.lfoto = QLabel(self.page3)
        self.lfoto.setPixmap(QPixmap("recibo/GUIA/GUIA.jpg").scaledToWidth(150))
        self.lfoto.setGeometry(780, 280, 150, 300)

        self.b2 = QPushButton(self.page3)
        self.b2.setGeometry(840, 550, 48, 48)
        self.b2.setIcon(QIcon(QPixmap("icon/receipt-48.png")))
        self.b2.setIconSize(QSize(48, 48))
        self.b2.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.b2.setFlat(True)
        self.b2.clicked.connect(self.abrirRecibo)

    def codigoDeGuia(self):
        if len(self.liGui.text()) == 0:
            QMessageBox.information(self, 'Inforamcion', 'Esta vacio!', QMessageBox.Ok)
        elif self.liGui.text() == "all":
            for i in reversed(range(self.tb1.rowCount())):
                self.tb1.removeRow(i)
                self.contar = 0
            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("SELECT * FROM Recibo")):
                    contador = 0
                    for row in query:
                        self.progress4.setValue(contador)
                        self.tb1.insertRow(self.tb1.rowCount())
                        self.tb1.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                        self.tb1.setItem(self.contar, 1, QTableWidgetItem(str(row[1])))
                        self.tb1.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                        self.tb1.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                        self.tb1.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                        self.tb1.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                        self.tb1.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                        self.tb1.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                        self.tb1.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                        self.tb1.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                        self.tb1.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                        self.tb1.setItem(self.contar, 11, QTableWidgetItem(row[11]))
                        self.tb1.setItem(self.contar, 12, QTableWidgetItem(row[12]))
                        self.tb1.setItem(self.contar, 13, QTableWidgetItem(row[13]))
                        self.tb1.setItem(self.contar, 14, QTableWidgetItem(row[14]))
                        self.tb1.setItem(self.contar, 15, QTableWidgetItem(row[15]))
                        self.tb1.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                        self.tb1.setItem(self.contar, 17, QTableWidgetItem(row[17]))
                        self.tb1.setItem(self.contar, 18, QTableWidgetItem(row[18]))
                        self.tb1.setItem(self.contar, 19, QTableWidgetItem(row[19]))
                        self.tb1.setItem(self.contar, 20, QTableWidgetItem(row[20]))
                        self.tb1.setItem(self.contar, 21, QTableWidgetItem(row[21]))
                        self.tb1.setItem(self.contar, 22, QTableWidgetItem(row[22]))
                        self.tb1.setItem(self.contar, 23, QTableWidgetItem(str(row[23])))
                        self.tb1.setItem(self.contar, 24, QTableWidgetItem(str(row[24])))
                        self.contar += 1
                        contador += 0.01

                    self.progress4.setValue(0)

                else:
                    QMessageBox.information(self, 'Inforamcion',
                                            'Codigo no existe o esta mal escrito.', QMessageBox.Ok)
                db.close()
                query.close()
            except:
                QMessageBox.critical(self, 'Alerta', 'Error en la DB.', QMessageBox.Ok)

        elif self.liGui.text() == self.liGui.text():

            for i in reversed(range(self.tb1.rowCount())):
                self.tb1.removeRow(i)
                self.contar = 0

            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("SELECT * FROM Recibo WHERE CodigoG='" + self.liGui.text() + "'")):
                    contador = 0
                    for contador in range(100):
                        self.progress4.setValue(contador)
                    for row in query:
                        self.tb1.insertRow(self.tb1.rowCount())
                        self.tb1.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                        self.tb1.setItem(self.contar, 1, QTableWidgetItem(str(row[1])))
                        self.tb1.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                        self.tb1.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                        self.tb1.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                        self.tb1.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                        self.tb1.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                        self.tb1.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                        self.tb1.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                        self.tb1.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                        self.tb1.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                        self.tb1.setItem(self.contar, 11, QTableWidgetItem(row[11]))
                        self.tb1.setItem(self.contar, 12, QTableWidgetItem(row[12]))
                        self.tb1.setItem(self.contar, 13, QTableWidgetItem(row[13]))
                        self.tb1.setItem(self.contar, 14, QTableWidgetItem(row[14]))
                        self.tb1.setItem(self.contar, 15, QTableWidgetItem(row[15]))
                        self.tb1.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                        self.tb1.setItem(self.contar, 17, QTableWidgetItem(row[17]))
                        self.tb1.setItem(self.contar, 18, QTableWidgetItem(row[18]))
                        self.tb1.setItem(self.contar, 19, QTableWidgetItem(row[19]))
                        self.tb1.setItem(self.contar, 20, QTableWidgetItem(row[20]))
                        self.tb1.setItem(self.contar, 21, QTableWidgetItem(row[21]))
                        self.tb1.setItem(self.contar, 22, QTableWidgetItem(row[22]))
                        self.tb1.setItem(self.contar, 23, QTableWidgetItem(str(row[23])))
                        self.tb1.setItem(self.contar, 24, QTableWidgetItem(str(row[24])))

                    self.progress4.setValue(0)
                else:
                    QMessageBox.information(self, 'Inforamcion',
                                            'Codigo no existe o esta mal escrito.', QMessageBox.Ok)
                db.close()
                query.close()
            except:
                QMessageBox.critical(self, 'Alerta', 'Error en la DB.', QMessageBox.Ok)
        else:
            QMessageBox.information(self, 'Informacion',
                                    'Fallo en al momento de buscar un recibo.', QMessageBox.Ok)

    def generarRecibo(self):
        row = self.tb1.currentRow()
        column = self.tb1.currentColumn()
        self.codigo = self.tb1.item(row, 0).text()
        try:
            db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
            query = db.cursor()
            if (query.execute("SELECT * FROM Recibo WHERE CodigoG='" + self.codigo + "'")):
                db.commit()
                for row in query:
                    self.laN.setText(row[2])
                    self.laNV.setText("[" + str(row[1]) + "]")
                    self.laM1.setText(row[3])
                    self.laM2.setText(row[4])
                    self.laM3.setText(row[5])
                    self.laM4.setText(row[6])
                    self.laM5.setText(row[7])
                    self.laM6.setText(row[8])
                    self.laM7.setText(row[9])
                    self.laM8.setText(row[10])
                    self.laM9.setText(row[11])
                    self.laM10.setText(row[12])
                    self.laM11.setText(row[13])
                    self.laM12.setText(row[14])
                    self.laM13.setText(row[15])
                    self.laM14.setText(row[16])
                    self.laM15.setText(row[17])
                    self.laM16.setText(row[18])
                    self.laM17.setText(row[19])
                    self.laM18.setText(row[20])
                    self.laM19.setText(row[21])
                    self.laM20.setText(row[22])
                    self.laMTotal.setText(str(row[23]))
                    self.laMFecha.setText(str(row[24]))
            else:
                print("Problemas con la selecion")
        except:
            print("Problema en la DB")

    def abrirRecibo(self):
        try:
            x = os.path.abspath("recibo")
            os.startfile(x+"recibo/" + self.codigo + ".pdf")
        except:
            os.system("epdfview recibo/" + self.codigo + ".pdf")
# ---------------------------------------------------------------------------------------
# Buscar Medicamento

    def buscardorDeM(self):
        self.la1 = QLabel("[CODIGO", self.page2)
        self.la1.setGeometry(90, 36, 120, 13)

        self.li1 = QLineEdit(self.page2)
        self.li1.setAlignment(Qt.AlignCenter)
        self.li1.setPlaceholderText("Codigo de Barra")
        self.li1.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li1.setGeometry(90, 50, 120, 20)

        self.bot1 = QPushButton(self.page2)
        self.bot1.setGeometry(220, 48, 30, 30)
        self.bot1.setIcon(QIcon(QPixmap("icon/Search-48.png")))
        self.bot1.setIconSize(QSize(30, 60))
        self.bot1.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bot1.setFlat(True)
        self.bot1.clicked.connect(self.buscarPorCodigo)

        self.la2 = QLabel("[NOMBRE]", self.page2)
        self.la2.setGeometry(300, 36, 120, 13)

        self.li2 = QLineEdit(self.page2)
        self.li2.setAlignment(Qt.AlignCenter)
        self.li2.setPlaceholderText("Nombre del Medicamento")
        self.li2.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li2.setGeometry(300, 50, 200, 20)

        self.bot2 = QPushButton(self.page2)
        self.bot2.setGeometry(510, 48, 30, 30)
        self.bot2.setIcon(QIcon(QPixmap("icon/Search-48.png")))
        self.bot2.setIconSize(QSize(30, 60))
        self.bot2.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bot2.setFlat(True)
        self.bot2.clicked.connect(self.buscarPorNombre)

        self.comboPr = QComboBox(self.page2)
        self.comboPr.setGeometry(580, 50, 300, 20)
        self.comboPr.addItems(("Presentaciones",
                               "AEROSOLES",
                               "CAPSULA BLANDA",
                               "CAPSULA CON CUBIERTA ENTERICA",
                               "CAPSULA DE LIBERACION PROLONGADA",
                               "CAPSULA DURA",
                               "CAPSULAS DURAS Y BLANDAS",
                               "CAPSULA VAGINAL",
                               "COMPRIMIDO",
                               "CREMAS, GELES, UNGUENTOS Y PASTAS",
                               "CREMA TOPICA",
                               "CREMA VAGINAL",
                               "CÁPSULA BLANDA DE GELATINA CON CUBIERTA ENTÉRICA",
                               "CÁPSULA BLANDATWISTOFF",
                               "ELIXIR",
                               "EMULSION (CHAMPOO)",
                               "EMULSION INYECTABLE",
                               "EMULSION ORAL",
                               "EMULSIÓN",
                               "ESPONJA MEDICADA",
                               "GAS",
                               "GEL ESTERILINTRA OCULAR",
                               "GEL ORAL",
                               "GEL TOPICO",
                               "GEL VAGINAL",
                               "GRANULOS",
                               "GRANULOS EFERVESCENTES",
                               "IMPLANTE",
                               "IMPLANTES Y SISTEMAS INTRAUTERINOS E INTRAOCULARES",
                               "INYECTABLES",
                               "JABON LIQUIDO",
                               "JALEA",
                               "JARABE",
                               "LAMINAS DISPERSABLES",
                               "LIOFILIZADO ORAL DE DISPERSION RAPIDA",
                               "LIQUIDO ESTERIL PARA INYECCION (AGUA)",
                               "LIQUIDOOLEOSO (Aceite)",
                               "LOCION",
                               "MEDICAMENTOS NO ESTERILES SOLIDOS - POLVOS Y GRANULADOS",
                               "ORAL",
                               "OVULO",
                               "PASTA",
                               "POLVO",
                               "POLVO ESTERIL PARA RECONSTITUIR A SOLUCION INYECTABLE",
                               "POLVO LIOFILIZADO",
                               "POLVO LIOFILIZADO PARA RECONSTITUIR A SOLUCION INYECTABLE",
                               "POLVO MEDICADO PARA USO TOPICO",
                               "POLVO PARA INHALACION",
                               "POLVO PARA RECONSTITUIR A SOLUCION ORAL",
                               "POLVO PARA RECONSTITUIR A SUSPENSION ORAL",
                               "POMADA",
                               "SISTEMAS DE LIBERACION",
                               "SOLUCION BUCAL",
                               "SOLUCION BUCOFARINGUEA",
                               "SOLUCION ESTERIL PARA DIÁLISIS PERITONEAL",
                               "SOLUCION ESTERIL PARA IRRIGACION",
                               "SOLUCION NASAL",
                               "SOLUCION INYECTABLE",
                               "SOLUCION OFTALMICA",
                               "SOLUCION ORAL",
                               "SOLUCION OTICA",
                               "SOLUCION PARA ADMINISTRAR POR VIA RECTAL",
                               "SOLUCION PARA USO TRANSDERMICO(PARCHE)",
                               "SOLUCION PARA INHALACION",
                               "SOLUCION PARA PRESERVACION DE ORGANOS EN TRANSPLANTES",
                               "SOLUCION TOPICA",
                               "SOLUCIÓN CONCENTRADA PARA INFUSIÓN",
                               "SOLUCIÓN PARA INFUSIÓN",
                               "SOLUCIÓN SUBLINGUAL",
                               "SUPOSITORIO",
                               "SUSPENSIONES",
                               "SUSPENSION INYECTABLE",
                               "SUSPENSION NASAL",
                               "SUSPENSION OFTALMICA",
                               "SUSPENSION ORAL",
                               "SUSPENSION OTICA",
                               "SUSPENSION PARA ADMINISTRAR PORVIARECTAL",
                               "SUSPENSION PARA INHALACION",
                               "SUSPENSION PARA NEBULIZACION",
                               "SUSPENSION TOPICA",
                               "TABLETA",
                               "TABLETA CON CUBIERTA ENTERICA CON PELICULA",
                               "TABLETA CON RECUBRIMIENTO ENTÉRICO",
                               "TABLETA CUBIERTA (GRAGEA)",
                               "TABLETA CUBIERTA CON PELICULA",
                               "TABLETA EFERVESCENTE",
                               "TABLETA DE LIBERACION PROLONGADA",
                               "TABLETA DE LIBERACIÓN MODIFICADA",
                               "TABLETA DE LIBERACION RETARDADA",
                               "TABLETA DISPERSABLE",
                               "TABLETA MASTICABLE",
                               "TABLETA ORODISPERSABLE",
                               "TABLETA RECUBIERTA",
                               "TABLETA SUBLINGUAL",
                               "TABLETA VAGINAL",
                               "TRANSDERMICOS",
                               "UNGUENTO OFTALMICO",
                               "UNGUENTO PROCTOLOGICO",
                               "UNGUENTO TOPICO", ))
        self.comboPr.currentIndexChanged.connect(self.buscarPorPresentacion)

        self.tab2 = QTableWidget(0, 24, self.page2)
        self.tab2.setGeometry(60, 100, 870, 400)
        self.tab2.setHorizontalHeaderLabels(['CODIGO', 'NOMBRE', 'LABORATORIO', 'LOTE', 'FECHA DE EXPEDICION', 'FECHA DE VENCIMIENTO', 'TIPO', 'DESCRIPCION ATC', 'VIA DE ADMINISTRACION', 'CONCENTRACION',
                                             'UNIDADES', 'TABLETAS', 'CAJAS', 'BOLSA', 'FRASCO', 'FORMA FARMACEUTICA', 'PRESIO', 'PRESIO POR UNIDAD', 'PRESIO TABLETA', 'PRESIO CAJA', 'PRESIO BOLSA', 'PRESIO FRASCO', 'INFORAMCION', 'FECHA'])
        self.tab2.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tab2.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tab2.horizontalHeader().setStretchLastSection(True)
        self.tab2.verticalHeader().setDefaultSectionSize(18)                 # Vertical height of rows
        self.tab2.setShowGrid(True)
        self.tab2.setDragDropOverwriteMode(True)
        self.tab2.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tab2.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tab2.setStyleSheet(
            "QTableView {background-color: #CEECF5;selection-background-color: #FF0000;}")

        self.progress = QProgressBar(self.page2)
        self.progress.setGeometry(60, 90, 870, 10)

    def buscarPorCodigo(self):
        if len(self.li1.text()) == 0:
            QMessageBox.information(self, 'Inforamcion', 'Esta vacio!', QMessageBox.Ok)
        elif self.li1.text() == "all":
            for i in reversed(range(self.tab2.rowCount())):
                self.tab2.removeRow(i)
                self.contar = 0
            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("SELECT * FROM Almacen")):
                    self.contador = -81
                    for row in query:
                        self.progress.setValue(self.contador)
                        self.tab2.insertRow(self.tab2.rowCount())
                        self.tab2.setItem(self.contar, 0, QTableWidgetItem(str(row[1])))
                        self.tab2.setItem(self.contar, 1, QTableWidgetItem(row[2]))
                        self.tab2.setItem(self.contar, 2, QTableWidgetItem(row[3]))
                        self.tab2.setItem(self.contar, 3, QTableWidgetItem(row[4]))
                        self.tab2.setItem(self.contar, 4, QTableWidgetItem(row[5]))
                        self.tab2.setItem(self.contar, 5, QTableWidgetItem(row[6]))
                        self.tab2.setItem(self.contar, 6, QTableWidgetItem(row[7]))
                        self.tab2.setItem(self.contar, 7, QTableWidgetItem(row[8]))
                        self.tab2.setItem(self.contar, 8, QTableWidgetItem(row[9]))
                        self.tab2.setItem(self.contar, 9, QTableWidgetItem(row[10]))
                        self.tab2.setItem(self.contar, 10, QTableWidgetItem(str(row[11])))
                        self.tab2.setItem(self.contar, 11, QTableWidgetItem(str(row[12])))
                        self.tab2.setItem(self.contar, 12, QTableWidgetItem(str(row[13])))
                        self.tab2.setItem(self.contar, 13, QTableWidgetItem(str(row[14])))
                        self.tab2.setItem(self.contar, 14, QTableWidgetItem(str(row[15])))
                        self.tab2.setItem(self.contar, 15, QTableWidgetItem(row[16]))
                        self.tab2.setItem(self.contar, 16, QTableWidgetItem(str(row[17])))
                        self.tab2.setItem(self.contar, 17, QTableWidgetItem(str(row[16])))
                        self.tab2.setItem(self.contar, 18, QTableWidgetItem(str(row[19])))
                        self.tab2.setItem(self.contar, 19, QTableWidgetItem(str(row[20])))
                        self.tab2.setItem(self.contar, 20, QTableWidgetItem(str(row[21])))
                        self.tab2.setItem(self.contar, 21, QTableWidgetItem(str(row[22])))
                        self.tab2.setItem(self.contar, 22, QTableWidgetItem(row[23]))
                        self.tab2.setItem(self.contar, 23, QTableWidgetItem(str(row[25])))
                        self.contar += 1
                        self.contador += 0.01

                    self.progress.setValue(0)

                else:
                    QMessageBox.information(self, 'Inforamcion',
                                            'Codigo del medicamendo no existe!', QMessageBox.Ok)
                db.close()
            except:
                QMessageBox.critical(self, 'Alerta', 'Error en la DB!', QMessageBox.Ok)

        elif self.li1.text() == self.li1.text():

            for i in reversed(range(self.tab2.rowCount())):
                self.tab2.removeRow(i)
                self.contar = 0

            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("SELECT * FROM Almacen WHERE CodigoBarra='" + self.li1.text() + "'")):
                    self.contador = 0
                    while self.contador < 100:
                        self.contador += 1
                        self.progress.setValue(self.contador)

                    for row in query:
                        self.tab2.insertRow(self.tab2.rowCount())
                        self.tab2.setItem(self.contar, 0, QTableWidgetItem(str(row[1])))
                        self.tab2.setItem(self.contar, 1, QTableWidgetItem(row[2]))
                        self.tab2.setItem(self.contar, 2, QTableWidgetItem(row[3]))
                        self.tab2.setItem(self.contar, 3, QTableWidgetItem(row[4]))
                        self.tab2.setItem(self.contar, 4, QTableWidgetItem(row[5]))
                        self.tab2.setItem(self.contar, 5, QTableWidgetItem(row[6]))
                        self.tab2.setItem(self.contar, 6, QTableWidgetItem(row[7]))
                        self.tab2.setItem(self.contar, 7, QTableWidgetItem(row[8]))
                        self.tab2.setItem(self.contar, 8, QTableWidgetItem(row[9]))
                        self.tab2.setItem(self.contar, 9, QTableWidgetItem(row[10]))
                        self.tab2.setItem(self.contar, 10, QTableWidgetItem(str(row[11])))
                        self.tab2.setItem(self.contar, 11, QTableWidgetItem(str(row[12])))
                        self.tab2.setItem(self.contar, 12, QTableWidgetItem(str(row[13])))
                        self.tab2.setItem(self.contar, 13, QTableWidgetItem(str(row[14])))
                        self.tab2.setItem(self.contar, 14, QTableWidgetItem(str(row[15])))
                        self.tab2.setItem(self.contar, 15, QTableWidgetItem(row[16]))
                        self.tab2.setItem(self.contar, 16, QTableWidgetItem(str(row[17])))
                        self.tab2.setItem(self.contar, 17, QTableWidgetItem(str(row[16])))
                        self.tab2.setItem(self.contar, 18, QTableWidgetItem(str(row[19])))
                        self.tab2.setItem(self.contar, 19, QTableWidgetItem(str(row[20])))
                        self.tab2.setItem(self.contar, 20, QTableWidgetItem(str(row[21])))
                        self.tab2.setItem(self.contar, 21, QTableWidgetItem(str(row[22])))
                        self.tab2.setItem(self.contar, 22, QTableWidgetItem(row[23]))
                        self.tab2.setItem(self.contar, 23, QTableWidgetItem(str(row[25])))
                    self.progress.setValue(0)

                    if self.tab2.rowCount() == 2:
                        self.tab2.removeRow(1)
                else:
                    QMessageBox.information(self, 'Inforamcion',
                                            'Codigo de Medicamendo no existe.', QMessageBox.Ok)
                db.close()
            except:
                QMessageBox.critical(self, 'Alerta', 'Error en la DB.', QMessageBox.Ok)
        else:
            QMessageBox.information(self, 'Informacion', 'Codigo Invalido.', QMessageBox.Ok)

    def buscarPorNombre(self):
        if len(self.li2.text()) == 0:
            QMessageBox.information(self, 'Inforamcion', 'Esta vacio!', QMessageBox.Ok)
        elif self.li2.text() == "all":
            for i in reversed(range(self.tab2.rowCount())):
                self.tab2.removeRow(i)
                self.contar = 0
            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("SELECT * FROM Almacen")):
                    self.contador = -81
                    for row in query:
                        self.progress.setValue(self.contador)
                        self.tab2.insertRow(self.tab2.rowCount())
                        self.tab2.setItem(self.contar, 0, QTableWidgetItem(str(row[1])))
                        self.tab2.setItem(self.contar, 1, QTableWidgetItem(row[2]))
                        self.tab2.setItem(self.contar, 2, QTableWidgetItem(row[3]))
                        self.tab2.setItem(self.contar, 3, QTableWidgetItem(row[4]))
                        self.tab2.setItem(self.contar, 4, QTableWidgetItem(row[5]))
                        self.tab2.setItem(self.contar, 5, QTableWidgetItem(row[6]))
                        self.tab2.setItem(self.contar, 6, QTableWidgetItem(row[7]))
                        self.tab2.setItem(self.contar, 7, QTableWidgetItem(row[8]))
                        self.tab2.setItem(self.contar, 8, QTableWidgetItem(row[9]))
                        self.tab2.setItem(self.contar, 9, QTableWidgetItem(row[10]))
                        self.tab2.setItem(self.contar, 10, QTableWidgetItem(str(row[11])))
                        self.tab2.setItem(self.contar, 11, QTableWidgetItem(str(row[12])))
                        self.tab2.setItem(self.contar, 12, QTableWidgetItem(str(row[13])))
                        self.tab2.setItem(self.contar, 13, QTableWidgetItem(str(row[14])))
                        self.tab2.setItem(self.contar, 14, QTableWidgetItem(str(row[15])))
                        self.tab2.setItem(self.contar, 15, QTableWidgetItem(row[16]))
                        self.tab2.setItem(self.contar, 16, QTableWidgetItem(str(row[17])))
                        self.tab2.setItem(self.contar, 17, QTableWidgetItem(str(row[16])))
                        self.tab2.setItem(self.contar, 18, QTableWidgetItem(str(row[19])))
                        self.tab2.setItem(self.contar, 19, QTableWidgetItem(str(row[20])))
                        self.tab2.setItem(self.contar, 20, QTableWidgetItem(str(row[21])))
                        self.tab2.setItem(self.contar, 21, QTableWidgetItem(str(row[22])))
                        self.tab2.setItem(self.contar, 22, QTableWidgetItem(row[23]))
                        self.tab2.setItem(self.contar, 23, QTableWidgetItem(str(row[25])))
                        self.contar = self.contar + 1
                        self.contador += 0.01

                    self.progress.setValue(0)
                else:
                    QMessageBox.information(self, 'Inforamcion',
                                            'Codigo del medicamendo no existe!', QMessageBox.Ok)
                db.close()
            except:
                QMessageBox.critical(self, 'Fallo', 'Error en la DB!', QMessageBox.Ok)

        elif self.li2.text() == self.li2.text():

            for i in reversed(range(self.tab2.rowCount())):
                self.tab2.removeRow(i)
                self.contar = 0
            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("SELECT * FROM Almacen WHERE  Nombre LIKE '" + self.li2.text() + "%'")):
                    self.contador = 0
                    while self.contador < 100:
                        self.contador += 1
                        self.progress.setValue(self.contador)
                    for row in query:
                        self.tab2.insertRow(self.tab2.rowCount())
                        self.tab2.setItem(self.contar, 0, QTableWidgetItem(str(row[1])))
                        self.tab2.setItem(self.contar, 1, QTableWidgetItem(row[2]))
                        self.tab2.setItem(self.contar, 2, QTableWidgetItem(row[3]))
                        self.tab2.setItem(self.contar, 3, QTableWidgetItem(row[4]))
                        self.tab2.setItem(self.contar, 4, QTableWidgetItem(row[5]))
                        self.tab2.setItem(self.contar, 5, QTableWidgetItem(row[6]))
                        self.tab2.setItem(self.contar, 6, QTableWidgetItem(row[7]))
                        self.tab2.setItem(self.contar, 7, QTableWidgetItem(row[8]))
                        self.tab2.setItem(self.contar, 8, QTableWidgetItem(row[9]))
                        self.tab2.setItem(self.contar, 9, QTableWidgetItem(str(row[10])))
                        self.tab2.setItem(self.contar, 10, QTableWidgetItem(str(row[11])))
                        self.tab2.setItem(self.contar, 11, QTableWidgetItem(str(row[12])))
                        self.tab2.setItem(self.contar, 12, QTableWidgetItem(str(row[13])))
                        self.tab2.setItem(self.contar, 13, QTableWidgetItem(str(row[14])))
                        self.tab2.setItem(self.contar, 14, QTableWidgetItem(row[15]))
                        self.tab2.setItem(self.contar, 15, QTableWidgetItem(str(row[16])))
                        self.tab2.setItem(self.contar, 16, QTableWidgetItem(str(row[17])))
                        self.tab2.setItem(self.contar, 17, QTableWidgetItem(str(row[18])))
                        self.tab2.setItem(self.contar, 18, QTableWidgetItem(str(row[19])))
                        self.tab2.setItem(self.contar, 19, QTableWidgetItem(str(row[20])))
                        self.tab2.setItem(self.contar, 20, QTableWidgetItem(str(row[21])))
                        self.tab2.setItem(self.contar, 21, QTableWidgetItem(row[22]))
                        self.tab2.setItem(self.contar, 22, QTableWidgetItem(str(row[23])))
                        self.contar = self.contar + 1

                    self.progress.setValue(0)

                    if self.tab2.rowCount() == 2:
                        self.tab2.removeRow(1)
                else:
                    QMessageBox.information(self, 'Inforamcion',
                                            'El nombre no existe.', QMessageBox.Ok)
                db.close()
            except:
                QMessageBox.critical(self, 'Alerta', 'Error en la DB.', QMessageBox.Ok)
        else:
            QMessageBox.information(self, 'Informacion', 'Codigo Invalido.', QMessageBox.Ok)

    def buscarPorPresentacion(self):
        if self.comboPr.currentText() == "Presentaciones":
            QMessageBox.information(self, 'Inforamcion',
                                    'No a elegido una presentacion!', QMessageBox.Ok)
        else:
            for i in reversed(range(self.tab2.rowCount())):
                self.tab2.removeRow(i)
                self.contar = 0
            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("SELECT * FROM Almacen WHERE Forma_Farmaceutica='" + self.comboPr.currentText() + "'")):
                    self.contador = 0
                    for contador in range(110):
                        self.progress.setValue(contador)
                    for row in query:
                        self.progress.setValue(self.contador)
                        self.tab2.insertRow(self.tab2.rowCount())
                        self.tab2.setItem(self.contar, 0, QTableWidgetItem(str(row[1])))
                        self.tab2.setItem(self.contar, 1, QTableWidgetItem(row[2]))
                        self.tab2.setItem(self.contar, 2, QTableWidgetItem(row[3]))
                        self.tab2.setItem(self.contar, 3, QTableWidgetItem(row[4]))
                        self.tab2.setItem(self.contar, 4, QTableWidgetItem(row[5]))
                        self.tab2.setItem(self.contar, 5, QTableWidgetItem(row[6]))
                        self.tab2.setItem(self.contar, 6, QTableWidgetItem(row[7]))
                        self.tab2.setItem(self.contar, 7, QTableWidgetItem(row[8]))
                        self.tab2.setItem(self.contar, 8, QTableWidgetItem(row[9]))
                        self.tab2.setItem(self.contar, 9, QTableWidgetItem(row[10]))
                        self.tab2.setItem(self.contar, 10, QTableWidgetItem(str(row[11])))
                        self.tab2.setItem(self.contar, 11, QTableWidgetItem(str(row[12])))
                        self.tab2.setItem(self.contar, 12, QTableWidgetItem(str(row[13])))
                        self.tab2.setItem(self.contar, 13, QTableWidgetItem(str(row[14])))
                        self.tab2.setItem(self.contar, 14, QTableWidgetItem(str(row[15])))
                        self.tab2.setItem(self.contar, 15, QTableWidgetItem(row[16]))
                        self.tab2.setItem(self.contar, 16, QTableWidgetItem(str(row[17])))
                        self.tab2.setItem(self.contar, 17, QTableWidgetItem(str(row[16])))
                        self.tab2.setItem(self.contar, 18, QTableWidgetItem(str(row[19])))
                        self.tab2.setItem(self.contar, 19, QTableWidgetItem(str(row[20])))
                        self.tab2.setItem(self.contar, 20, QTableWidgetItem(str(row[21])))
                        self.tab2.setItem(self.contar, 21, QTableWidgetItem(str(row[22])))
                        self.tab2.setItem(self.contar, 22, QTableWidgetItem(row[23]))
                        self.tab2.setItem(self.contar, 23, QTableWidgetItem(str(row[25])))
                        self.contar = self.contar + 1
                    self.progress.setValue(0)
            except:
                QMessageBox.critical(self, 'Fallo', 'Error en la DB!', QMessageBox.Ok)
# ---------------------------------------------------------------------------------------
# Inventario

    def inventario(self):
        self.la3 = QLabel("[CODIGO", self.page5)
        self.la3.setGeometry(90, 36, 120, 13)

        self.li3 = QLineEdit(self.page5)
        self.li3.setAlignment(Qt.AlignCenter)
        self.li3.setPlaceholderText("Codigo de Barra")
        self.li3.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li3.setGeometry(90, 50, 120, 20)

        self.bot3 = QPushButton(self.page5)
        self.bot3.setGeometry(220, 48, 30, 30)
        self.bot3.setIcon(QIcon(QPixmap("icon/Search-48.png")))
        self.bot3.setIconSize(QSize(30, 60))
        self.bot3.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bot3.setFlat(True)
        self.bot3.clicked.connect(self.buscarPorCodigo2)

        self.la4 = QLabel("[NOMBRE]", self.page5)
        self.la4.setGeometry(300, 36, 120, 13)

        self.li4 = QLineEdit(self.page5)
        self.li4.setAlignment(Qt.AlignCenter)
        self.li4.setPlaceholderText("Nombre del Medicamento")
        self.li4.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.li4.setGeometry(300, 50, 200, 20)

        self.bot4 = QPushButton(self.page5)
        self.bot4.setGeometry(510, 48, 30, 30)
        self.bot4.setIcon(QIcon(QPixmap("icon/Search-48.png")))
        self.bot4.setIconSize(QSize(30, 30))
        self.bot4.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bot4.setFlat(True)
        self.bot4.clicked.connect(self.buscarPorNombre2)

        self.tab3 = QTableWidget(0, 25, self.page5)
        self.tab3.setGeometry(60, 100, 870, 400)
        self.tab3.setHorizontalHeaderLabels(['N', 'CODIGO', 'NOMBRE', 'LABORATORIO', 'LOTE', 'FECHA DE EXPEDICION', 'FECHA DE VENCIMIENTO', 'TIPO', 'DESCRIPCION ATC', 'VIA DE ADMINISTRACION', 'CONCENTRACION',
                                             'UNIDADES', 'TABLETAS', 'CAJAS', 'BOLSA', 'FRASCO', 'FORMA FARMACEUTICA', 'PRESIO', 'PRESIO POR UNIDAD', 'PRESIO TABLETA', 'PRESIO CAJA', 'PRESIO BOLSA', 'PRESIO FRASCO', 'INFORAMCION', 'FECHA'])
        self.tab3.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tab3.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tab3.horizontalHeader().setStretchLastSection(True)
        self.tab3.verticalHeader().setDefaultSectionSize(18)                 # Vertical height of rows
        self.tab3.setShowGrid(True)
        self.tab3.setDragDropOverwriteMode(True)
        self.tab3.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tab3.setStyleSheet(
            "QTableView {background-color: #CEECF5;selection-background-color: #FF0000;}")

        self.progress2 = QProgressBar(self.page5)
        self.progress2.setGeometry(60, 90, 870, 10)

        self.bot5 = QPushButton(self.page5)
        self.bot5.setGeometry(400, 530, 48, 48)
        self.bot5.setIcon(QIcon(QPixmap("icon/Edit_Row-48.png")))
        self.bot5.setIconSize(QSize(48, 48))
        self.bot5.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bot5.setFlat(True)
        self.bot5.clicked.connect(self.modificar)

        self.bot6 = QPushButton(self.page5)
        self.bot6.setGeometry(548, 530, 48, 48)
        self.bot6.setIcon(QIcon(QPixmap("icon/Delete_Row-48.png")))
        self.bot6.setIconSize(QSize(48, 48))
        self.bot6.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bot6.setFlat(True)
        self.bot6.clicked.connect(self.eliminar)

        self.bot7 = QPushButton(self.page5)
        self.bot7.setGeometry(475, 530, 48, 48)
        self.bot7.setIcon(QIcon(QPixmap("icon/Plus-48.png")))
        self.bot7.setIconSize(QSize(48, 48))
        self.bot7.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bot7.setFlat(True)
        self.bot7.clicked.connect(self.insertar)

    def buscarPorCodigo2(self):
        if len(self.li3.text()) == 0:
            QMessageBox.information(self, 'Inforamcion', 'Esta vacio!', QMessageBox.Ok)
        elif self.li3.text() == "all":
            for i in reversed(range(self.tab3.rowCount())):
                self.tab3.removeRow(i)
                self.contar = 0
            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("SELECT * FROM Almacen")):
                    contador = -81
                    for row in query:
                        self.progress2.setValue(contador)
                        self.tab3.insertRow(self.tab3.rowCount())
                        self.tab3.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                        self.tab3.setItem(self.contar, 1, QTableWidgetItem(str(row[1])))
                        self.tab3.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                        self.tab3.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                        self.tab3.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                        self.tab3.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                        self.tab3.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                        self.tab3.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                        self.tab3.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                        self.tab3.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                        self.tab3.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                        self.tab3.setItem(self.contar, 11, QTableWidgetItem(str(row[11])))
                        self.tab3.setItem(self.contar, 12, QTableWidgetItem(str(row[12])))
                        self.tab3.setItem(self.contar, 13, QTableWidgetItem(str(row[13])))
                        self.tab3.setItem(self.contar, 14, QTableWidgetItem(str(row[14])))
                        self.tab3.setItem(self.contar, 15, QTableWidgetItem(str(row[15])))
                        self.tab3.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                        self.tab3.setItem(self.contar, 17, QTableWidgetItem(str(row[17])))
                        self.tab3.setItem(self.contar, 18, QTableWidgetItem(str(row[18])))
                        self.tab3.setItem(self.contar, 19, QTableWidgetItem(str(row[19])))
                        self.tab3.setItem(self.contar, 20, QTableWidgetItem(str(row[20])))
                        self.tab3.setItem(self.contar, 21, QTableWidgetItem(str(row[21])))
                        self.tab3.setItem(self.contar, 22, QTableWidgetItem(str(row[22])))
                        self.tab3.setItem(self.contar, 23, QTableWidgetItem(row[23]))
                        self.tab3.setItem(self.contar, 24, QTableWidgetItem(str(row[25])))
                        self.contar += 1
                        contador += 0.01

                    self.progress2.setValue(0)

                else:
                    QMessageBox.information(self, 'Inforamcion',
                                            'Codigo de Medicamendo no existe!', QMessageBox.Ok)
                db.close()
                query.close()
            except:
                QMessageBox.critical(self, 'Alerta', 'Error en la DB!', QMessageBox.Ok)

        elif self.li3.text() == self.li3.text():
            for i in reversed(range(self.tab3.rowCount())):
                self.tab3.removeRow(i)
                self.contar = 0

            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("SELECT * FROM Almacen WHERE CodigoBarra='" + self.li3.text() + "'")):
                    contador = 0
                    for contador in range(100):
                        self.progress2.setValue(contador)
                    for row in query:
                        self.tab3.insertRow(self.tab3.rowCount())
                        self.tab3.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                        self.tab3.setItem(self.contar, 1, QTableWidgetItem(str(row[1])))
                        self.tab3.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                        self.tab3.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                        self.tab3.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                        self.tab3.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                        self.tab3.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                        self.tab3.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                        self.tab3.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                        self.tab3.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                        self.tab3.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                        self.tab3.setItem(self.contar, 11, QTableWidgetItem(str(row[11])))
                        self.tab3.setItem(self.contar, 12, QTableWidgetItem(str(row[12])))
                        self.tab3.setItem(self.contar, 13, QTableWidgetItem(str(row[13])))
                        self.tab3.setItem(self.contar, 14, QTableWidgetItem(str(row[14])))
                        self.tab3.setItem(self.contar, 15, QTableWidgetItem(str(row[15])))
                        self.tab3.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                        self.tab3.setItem(self.contar, 17, QTableWidgetItem(str(row[17])))
                        self.tab3.setItem(self.contar, 18, QTableWidgetItem(str(row[18])))
                        self.tab3.setItem(self.contar, 19, QTableWidgetItem(str(row[19])))
                        self.tab3.setItem(self.contar, 20, QTableWidgetItem(str(row[20])))
                        self.tab3.setItem(self.contar, 21, QTableWidgetItem(str(row[21])))
                        self.tab3.setItem(self.contar, 22, QTableWidgetItem(str(row[22])))
                        self.tab3.setItem(self.contar, 24, QTableWidgetItem(row[23]))
                        self.tab3.setItem(self.contar, 25, QTableWidgetItem(str(row[25])))

                    self.progress2.setValue(0)

                    if self.tab3.rowCount() == 2:
                        self.tab3.removeRow(1)
                else:
                    QMessageBox.information(self, 'Inforamcion',
                                            'Codigo de Medicamendo no existe.', QMessageBox.Ok)
                db.close()
                query.close()
            except:
                QMessageBox.critical(self, 'Alerta', 'Error en la DB.', QMessageBox.Ok)
        else:
            QMessageBox.information(self, 'Informacion', 'Codigo invalido.', QMessageBox.Ok)

    def buscarPorNombre2(self):
        if len(self.li4.text()) == 0:
            QMessageBox.information(self, 'Inforamcion', 'Esta vacio.', QMessageBox.Ok)
        elif self.li4.text() == "all":
            for i in reversed(range(self.tab3.rowCount())):
                self.tab3.removeRow(i)
                self.contar = 0
            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("SELECT * FROM Almacen")):
                    self.contador = -81
                    for row in query:
                        self.progress2.setValue(self.contador)
                        self.tab3.insertRow(self.tab3.rowCount())
                        self.tab3.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                        self.tab3.setItem(self.contar, 1, QTableWidgetItem(str(row[1])))
                        self.tab3.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                        self.tab3.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                        self.tab3.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                        self.tab3.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                        self.tab3.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                        self.tab3.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                        self.tab3.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                        self.tab3.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                        self.tab3.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                        self.tab3.setItem(self.contar, 11, QTableWidgetItem(str(row[11])))
                        self.tab3.setItem(self.contar, 12, QTableWidgetItem(str(row[12])))
                        self.tab3.setItem(self.contar, 13, QTableWidgetItem(str(row[13])))
                        self.tab3.setItem(self.contar, 14, QTableWidgetItem(str(row[14])))
                        self.tab3.setItem(self.contar, 15, QTableWidgetItem(str(row[15])))
                        self.tab3.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                        self.tab3.setItem(self.contar, 17, QTableWidgetItem(str(row[17])))
                        self.tab3.setItem(self.contar, 18, QTableWidgetItem(str(row[18])))
                        self.tab3.setItem(self.contar, 19, QTableWidgetItem(str(row[19])))
                        self.tab3.setItem(self.contar, 20, QTableWidgetItem(str(row[20])))
                        self.tab3.setItem(self.contar, 21, QTableWidgetItem(str(row[21])))
                        self.tab3.setItem(self.contar, 22, QTableWidgetItem(str(row[22])))
                        self.tab3.setItem(self.contar, 23, QTableWidgetItem(row[23]))
                        self.tab3.setItem(self.contar, 24, QTableWidgetItem(str(row[25])))
                        self.contar += 1
                        self.contador += 0.01

                    self.progress2.setValue(0)
                else:
                    QMessageBox.information(self, 'Inforamcion',
                                            'Codigo de Medicamendo no existe.', QMessageBox.Ok)
                db.close()
                query.close()
            except:
                QMessageBox.critical(self, 'Alerta', 'Error en la DB!', QMessageBox.Ok)

        elif self.li4.text() == self.li4.text():

            for i in reversed(range(self.tab3.rowCount())):
                self.tab3.removeRow(i)
                self.contar = 0
            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("SELECT * FROM Almacen WHERE  Nombre LIKE '" + self.li4.text() + "%'")):
                    contador = 0
                    for contador in range(110):
                        self.progress2.setValue(contador)
                    for row in query:
                        self.tab3.insertRow(self.tab3.rowCount())
                        self.tab3.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                        self.tab3.setItem(self.contar, 1, QTableWidgetItem(str(row[1])))
                        self.tab3.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                        self.tab3.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                        self.tab3.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                        self.tab3.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                        self.tab3.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                        self.tab3.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                        self.tab3.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                        self.tab3.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                        self.tab3.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                        self.tab3.setItem(self.contar, 11, QTableWidgetItem(str(row[11])))
                        self.tab3.setItem(self.contar, 12, QTableWidgetItem(str(row[12])))
                        self.tab3.setItem(self.contar, 13, QTableWidgetItem(str(row[13])))
                        self.tab3.setItem(self.contar, 14, QTableWidgetItem(str(row[14])))
                        self.tab3.setItem(self.contar, 15, QTableWidgetItem(str(row[15])))
                        self.tab3.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                        self.tab3.setItem(self.contar, 17, QTableWidgetItem(str(row[17])))
                        self.tab3.setItem(self.contar, 18, QTableWidgetItem(str(row[18])))
                        self.tab3.setItem(self.contar, 19, QTableWidgetItem(str(row[19])))
                        self.tab3.setItem(self.contar, 20, QTableWidgetItem(str(row[20])))
                        self.tab3.setItem(self.contar, 21, QTableWidgetItem(str(row[21])))
                        self.tab3.setItem(self.contar, 22, QTableWidgetItem(str(row[22])))
                        self.tab3.setItem(self.contar, 23, QTableWidgetItem(row[23]))
                        self.tab3.setItem(self.contar, 24, QTableWidgetItem(str(row[25])))
                        self.contar += 1

                    self.progress2.setValue(0)

                    if self.tab3.rowCount() == 2:
                        self.tab3.removeRow(1)
                else:
                    QMessageBox.information(self, 'Inforamcion',
                                            'Codigo de Medicamendo no existe.', QMessageBox.Ok)
                db.close()
                query.close()
            except:
                QMessageBox.critical(self, 'Alerta', 'Error en la DB!', QMessageBox.Ok)
        else:
            QMessageBox.critical(self, 'Alerta', 'Codigo Invalido!', QMessageBox.Ok)

    def insertar(self):
        mddb = medicamentosDB.ingresoDeMedicamentosDB(self)
        mddb.show()

    def modificar(self):
        try:
            if self.tab3.rowCount() > 2:
                row = self.tab3.currentRow()
                column = self.tab3.currentColumn()
                codigo = self.tab3.item(row, 0).text()
                valor = self.tab3.currentItem().text()
                columnas = ['ID', 'CodigoBarra', 'Nombre', 'Laboratorio', 'Lote', 'Fecha_Expredicion', 'Fecha_Vencimiento', 'Tipo', 'Descripcion_ATC', 'VIA_Administracion', 'Concentracion', 'Cajas',
                            'Unidades', 'Tabletas', 'Bolsa', 'Frasco', 'Forma_Farmaceutica', 'Presio_Cliente', 'Presio_Unidad', 'Presio_Tableta', 'Presio_Caja', 'Presio_Bolsa', 'Presio_Botella', 'Inforamcion', 'Fecha']
                if codigo == 0:
                    QMessageBox.warning(self, 'Warning', 'El ID no es modificable.', QMessageBox.Ok)
                    try:
                        db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
                        query = db.cursor()
                        if (query.execute("SELECT * FROM Almacen")):
                            self.contador = -81
                            for row in query:
                                self.progress2.setValue(self.contador)
                                self.tab3.insertRow(self.tab3.rowCount())
                                self.tab3.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                                self.tab3.setItem(self.contar, 1, QTableWidgetItem(str(row[1])))
                                self.tab3.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                                self.tab3.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                                self.tab3.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                                self.tab3.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                                self.tab3.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                                self.tab3.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                                self.tab3.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                                self.tab3.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                                self.tab3.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                                self.tab3.setItem(self.contar, 11, QTableWidgetItem(str(row[11])))
                                self.tab3.setItem(self.contar, 12, QTableWidgetItem(str(row[12])))
                                self.tab3.setItem(self.contar, 13, QTableWidgetItem(str(row[13])))
                                self.tab3.setItem(self.contar, 14, QTableWidgetItem(str(row[14])))
                                self.tab3.setItem(self.contar, 15, QTableWidgetItem(str(row[15])))
                                self.tab3.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                                self.tab3.setItem(self.contar, 17, QTableWidgetItem(str(row[17])))
                                self.tab3.setItem(self.contar, 18, QTableWidgetItem(str(row[18])))
                                self.tab3.setItem(self.contar, 19, QTableWidgetItem(str(row[19])))
                                self.tab3.setItem(self.contar, 20, QTableWidgetItem(str(row[20])))
                                self.tab3.setItem(self.contar, 21, QTableWidgetItem(str(row[21])))
                                self.tab3.setItem(self.contar, 22, QTableWidgetItem(str(row[22])))
                                self.tab3.setItem(self.contar, 23, QTableWidgetItem(row[23]))
                                self.tab3.setItem(self.contar, 24, QTableWidgetItem(str(row[25])))
                                self.contar += 1
                                self.contador += 0.01

                            self.progress2.setValue(0)
                        else:
                            QMessageBox.information(
                                self, 'Inforamcion', 'Codigo de Medicamendo no existe.', QMessageBox.Ok)
                        db.close()
                        query.close()
                    except:
                        QMessageBox.critical(self, 'Alerta', 'Error en la DB!', QMessageBox.Ok)
                else:
                    try:
                        db = pymysql.connect(host="localhost", user="root",
                                             passwd="Lb104572", db="inventario")
                        query = db.cursor()
                        if (query.execute("UPDATE Almacen SET " + columnas[column] + " = '" + valor + "' WHERE ID='" + codigo + "'")):
                            db.commit()
                            QMessageBox.information(
                                self, 'Informacion', 'Se a modificado correctamente.', QMessageBox.Ok)
                        else:
                            QMessageBox.warning(
                                self, 'Warning', 'Error al momento de modificar la columna.', QMessageBox.Ok)
                            try:
                                if (query.execute("SELECT * FROM Almacen")):
                                    for i in reversed(range(self.tab3.rowCount())):
                                        self.tab3.removeRow(i)
                                        self.contar = 0
                                    contador = -81
                                    for row in query:
                                        self.progress2.setValue(contador)
                                        self.tab3.insertRow(self.tab3.rowCount())
                                        self.tab3.setItem(
                                            self.contar, 0, QTableWidgetItem(str(row[0])))
                                        self.tab3.setItem(
                                            self.contar, 1, QTableWidgetItem(str(row[1])))
                                        self.tab3.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                                        self.tab3.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                                        self.tab3.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                                        self.tab3.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                                        self.tab3.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                                        self.tab3.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                                        self.tab3.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                                        self.tab3.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                                        self.tab3.setItem(
                                            self.contar, 10, QTableWidgetItem(row[10]))
                                        self.tab3.setItem(
                                            self.contar, 11, QTableWidgetItem(str(row[11])))
                                        self.tab3.setItem(
                                            self.contar, 12, QTableWidgetItem(str(row[12])))
                                        self.tab3.setItem(
                                            self.contar, 13, QTableWidgetItem(str(row[13])))
                                        self.tab3.setItem(
                                            self.contar, 14, QTableWidgetItem(str(row[14])))
                                        self.tab3.setItem(
                                            self.contar, 15, QTableWidgetItem(str(row[15])))
                                        self.tab3.setItem(
                                            self.contar, 16, QTableWidgetItem(row[16]))
                                        self.tab3.setItem(
                                            self.contar, 17, QTableWidgetItem(str(row[17])))
                                        self.tab3.setItem(
                                            self.contar, 18, QTableWidgetItem(str(row[18])))
                                        self.tab3.setItem(
                                            self.contar, 19, QTableWidgetItem(str(row[19])))
                                        self.tab3.setItem(
                                            self.contar, 20, QTableWidgetItem(str(row[20])))
                                        self.tab3.setItem(
                                            self.contar, 21, QTableWidgetItem(str(row[21])))
                                        self.tab3.setItem(
                                            self.contar, 22, QTableWidgetItem(str(row[22])))
                                        self.tab3.setItem(
                                            self.contar, 23, QTableWidgetItem(row[23]))
                                        self.tab3.setItem(
                                            self.contar, 24, QTableWidgetItem(str(row[25])))
                                        self.contar += 1
                                        contador += 0.01
                                    self.progress2.setValue(0)
                                else:
                                    QMessageBox.information(
                                        self, 'Informacion', 'Problema al actualizar la tabla.', QMessageBox.Ok)
                            except:
                                QMessageBox.critical(
                                    self, 'Fallo', 'Fallo al conectar con la DB.', QMessageBox.Ok)
                    except:
                        QMessageBox.critical(
                            self, 'Fallo', 'No se a elegido a un cuadro', QMessageBox.Ok)
            else:
                row = self.tab3.currentRow()
                column = self.tab3.currentColumn()
                codigo = self.tab3.item(row, 0).text()
                valor = self.tab3.currentItem().text()
                columnas = ['ID', 'CodigoBarra', 'Nombre', 'Laboratorio', 'Lote', 'Fecha_Expredicion', 'Fecha_Vencimiento', 'Tipo', 'Descripcion_ATC',
                            'VIA_Administracion', 'Concentracion', 'Cajas', 'Forma_Farmaceutica', 'Presio', 'Presio_Unidad', 'Presio_Tableta', 'Presio_Caja', 'Inforamcion', 'Fecha']
                if codigo == 0:
                    QMessageBox.warning(self, 'Warning', 'El ID no es modificable.', QMessageBox.Ok)
                    try:
                        db = pymysql.connect(host="localhost", user="root",
                                             passwd="Lb104572", db="inventario")
                        query = db.cursor()
                        if (query.execute("SELECT * FROM Almacen WHERE CodigoBarra='" + self.li3.text() + "'")):
                            contador = 0
                            for contador in range(100):
                                self.progress2.setValue(contador)
                            for row in query:
                                self.tab3.insertRow(self.tab3.rowCount())
                                self.tab3.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                                self.tab3.setItem(self.contar, 1, QTableWidgetItem(str(row[1])))
                                self.tab3.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                                self.tab3.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                                self.tab3.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                                self.tab3.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                                self.tab3.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                                self.tab3.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                                self.tab3.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                                self.tab3.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                                self.tab3.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                                self.tab3.setItem(self.contar, 11, QTableWidgetItem(str(row[11])))
                                self.tab3.setItem(self.contar, 12, QTableWidgetItem(str(row[12])))
                                self.tab3.setItem(self.contar, 13, QTableWidgetItem(str(row[13])))
                                self.tab3.setItem(self.contar, 14, QTableWidgetItem(str(row[14])))
                                self.tab3.setItem(self.contar, 15, QTableWidgetItem(str(row[15])))
                                self.tab3.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                                self.tab3.setItem(self.contar, 17, QTableWidgetItem(str(row[17])))
                                self.tab3.setItem(self.contar, 18, QTableWidgetItem(str(row[18])))
                                self.tab3.setItem(self.contar, 19, QTableWidgetItem(str(row[19])))
                                self.tab3.setItem(self.contar, 20, QTableWidgetItem(str(row[20])))
                                self.tab3.setItem(self.contar, 21, QTableWidgetItem(str(row[21])))
                                self.tab3.setItem(self.contar, 22, QTableWidgetItem(str(row[22])))
                                self.tab3.setItem(self.contar, 23, QTableWidgetItem(row[23]))
                                self.tab3.setItem(self.contar, 24, QTableWidgetItem(str(row[25])))

                            self.progress2.setValue(0)

                            if self.tab3.rowCount() == 2:
                                self.tab3.removeRow(1)
                        else:
                            QMessageBox.information(
                                self, 'Inforamcion', 'Codigo de Medicamendo no existe.', QMessageBox.Ok)
                        db.close()
                        query.close()
                    except:
                        QMessageBox.critical(self, 'Alerta', 'Error en la DB.', QMessageBox.Ok)
                else:
                    try:
                        db = pymysql.connect(host="localhost", user="root",
                                             passwd="Lb104572", db="inventario")
                        query = db.cursor()
                        if (query.execute("UPDATE Almacen SET " + columnas[column] + " = '" + valor + "' WHERE ID='" + codigo + "'")):
                            db.commit()
                            QMessageBox.information(
                                self, 'Informacion', 'Se a modificado correctamente.', QMessageBox.Ok)
                        else:
                            QMessageBox.warning(
                                self, 'Warning', 'Error al momento de modificar la columna.', QMessageBox.Ok)
                    except:
                        QMessageBox.critical(
                            self, 'Fallo', 'Error al conectar con la DB.', QMessageBox.Ok)
        except:
            QMessageBox.infor(self, 'Inforamcion',
                              'Por favor selecione un columna.', QMessageBox.Ok)

    def eliminar(self):
        if self.tab3.rowCount() > 2:
            row = self.tab3.currentRow()
            codigo = self.tab3.item(row, 0).text()
            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("DELETE FROM Almacen WHERE ID= '" + codigo + "'")):
                    db.commit()
                    QMessageBox.information(self, 'Informacion',
                                            'Se elimino correctamente', QMessageBox.Ok)
                else:
                    QMessageBox.warning(
                        self, 'Alerta', 'Error al momento de eliminar el codigo: ' + codigo, QMessageBox.Ok)
            except:
                QMessageBox.critical(self, 'Fallo', 'Error al conectar con la DB', QMessageBox.Ok)
            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("SELECT * FROM Almacen")):
                    for i in reversed(range(self.tab3.rowCount())):
                        self.tab3.removeRow(i)
                        self.contar = 0
                    contador = -81
                    for row in query:
                        self.progress2.setValue(contador)
                        self.tab3.insertRow(self.tab3.rowCount())
                        self.tab3.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                        self.tab3.setItem(self.contar, 1, QTableWidgetItem(str(row[1])))
                        self.tab3.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                        self.tab3.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                        self.tab3.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                        self.tab3.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                        self.tab3.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                        self.tab3.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                        self.tab3.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                        self.tab3.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                        self.tab3.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                        self.tab3.setItem(self.contar, 11, QTableWidgetItem(str(row[11])))
                        self.tab3.setItem(self.contar, 12, QTableWidgetItem(str(row[12])))
                        self.tab3.setItem(self.contar, 13, QTableWidgetItem(str(row[13])))
                        self.tab3.setItem(self.contar, 14, QTableWidgetItem(str(row[14])))
                        self.tab3.setItem(self.contar, 15, QTableWidgetItem(str(row[15])))
                        self.tab3.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                        self.tab3.setItem(self.contar, 17, QTableWidgetItem(str(row[17])))
                        self.tab3.setItem(self.contar, 18, QTableWidgetItem(str(row[18])))
                        self.tab3.setItem(self.contar, 19, QTableWidgetItem(str(row[19])))
                        self.tab3.setItem(self.contar, 20, QTableWidgetItem(str(row[20])))
                        self.tab3.setItem(self.contar, 21, QTableWidgetItem(str(row[21])))
                        self.tab3.setItem(self.contar, 22, QTableWidgetItem(str(row[22])))
                        self.tab3.setItem(self.contar, 23, QTableWidgetItem(row[23]))
                        self.tab3.setItem(self.contar, 24, QTableWidgetItem(str(row[25])))
                        self.contar += 1
                        contador += 0.01
                    self.progress2.setValue(0)
                else:
                    QMessageBox.information(self, 'Informacion',
                                            'Problema en la tabla', QMessageBox.Ok)
            except:
                QMessageBox.critical(self, 'Fallo', 'Fallo al conectar con la DB', QMessageBox.Ok)
        else:
            row = self.tab3.currentRow()
            codigo = self.tab3.item(row, 0).text()
            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                if (query.execute("DELETE FROM Almacen WHERE ID= '" + codigo + "'")):
                    db.commit()
                    QMessageBox.information(self, 'Informacion',
                                            'Se elimino correctamente.', QMessageBox.Ok)
                    for i in reversed(range(self.tab3.rowCount())):
                        self.tab3.removeRow(i)
                        self.contar = 0
                else:
                    QMessageBox.warning(
                        self, 'Alerta', 'Error al momento de eliminar el codigo: ' + codigo, QMessageBox.Ok)
            except:
                QMessageBox.critical(self, 'Fallo', 'Error  al conectar con la DB.', QMessageBox.Ok)
# ---------------------------------------------------------------------------------------
# Diagrama

    def diagramaLineal(self):

        grop = QGroupBox("DIA INICIAL", self.page6)
        grop.setGeometry(40, 10, 330, 190)

        cal = QCalendarWidget(self.page6)
        cal.setGridVisible(True)
        cal.move(50, 40)
        cal.setFirstDayOfWeek(Qt.Monday)
        date1 = cal.selectedDate()
        cal.clicked[QDate].connect(self.showDate1)

        grop = QGroupBox("DIA FINAL", self.page6)
        grop.setGeometry(390, 10, 330, 190)

        cal1 = QCalendarWidget(self.page6)
        cal1.setGridVisible(True)
        cal1.move(400, 40)
        cal1.setFirstDayOfWeek(Qt.Monday)
        date2 = cal1.selectedDate()
        cal1.clicked[QDate].connect(self.showDate2)

        self.lineaG = QTextEdit(self.page6)
        self.lineaG.setEnabled(True)
        self.lineaG.setGeometry(730, 10, 220, 180)

        self.botonG = QPushButton(self.page6)
        self.botonG.setGeometry(840, 200, 30, 30)
        self.botonG.setIcon(QIcon(QPixmap("icon/Plots-48.png")))
        self.botonG.setIconSize(QSize(30, 30))
        self.botonG.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.botonG.setFlat(True)
        self.botonG.clicked.connect(self.buscarPlots)

        self.botonG = QPushButton(self.page6)
        self.botonG.setGeometry(800, 200, 30, 30)
        self.botonG.setIcon(QIcon(QPixmap("icon/Bar-48.png")))
        self.botonG.setIconSize(QSize(30, 30))
        self.botonG.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.botonG.setFlat(True)
        self.botonG.clicked.connect(self.buscarBar)

        self.botonEX = QPushButton(self.page6)
        self.botonEX.setGeometry(880, 200, 30, 30)
        self.botonEX.setIcon(QIcon(QPixmap("icon/excel.png")))
        self.botonEX.setIconSize(QSize(30, 30))
        self.botonEX.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.botonEX.setFlat(True)
        self.botonEX.clicked.connect(self.generarExcel)

        self.tabV = QTableWidget(0, 25, self.page6)
        self.tabV.setGeometry(40, 240, 920, 250)
        self.tabV.setHorizontalHeaderLabels(['GUIA', 'N.VENTA', 'CLIENTE', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7',
                                             'M8', 'M9', 'M10', 'M11', 'M12', 'M13', 'M14', 'M15', 'M16', 'M17', 'M18', 'M19', 'M20', 'TOTAL', 'FECHA'])
        self.tabV.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tabV.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tabV.horizontalHeader().setStretchLastSection(True)
        self.tabV.verticalHeader().setDefaultSectionSize(18)                 # Vertical height of rows
        self.tabV.setShowGrid(True)
        self.tabV.setDragDropOverwriteMode(True)
        self.tabV.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tabV.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tabV.setStyleSheet(
            "QTableView {background-color: #CEECF5;selection-background-color: #FF0000;}")

    def showDate1(self, date1):
        self.time1 = str(date1.year()) + "-" + str(date1.month()) + "-" + str(date1.day())

    def showDate2(self, date2):
        self.time2 = str(date2.year()) + "-" + str(date2.month()) + "-" + str(date2.day())

    def buscarPlots(self):
        try:
            db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
            query = db.cursor()
            query = "SELECT Fecha, Total FROM Recibo where Fecha BETWEEN '" + \
                self.time1 + "' AND '" + self.time2 + "' ORDER BY Fecha ASC"
            df = pandas.read_sql(query, db, index_col=['Fecha'])
            self.lineaG.setText((str(df)))
            fig, ax = plt.subplots()
            df.plot(ax=ax)
            plt.title("Sistema de Ventas")
            plt.ylabel('Dinero')
            plt.show()
            db.close()
            try:
                db = pymysql.connect(host="localhost", user="root",
                                     passwd="Lb104572", db="inventario")
                query = db.cursor()
                for i in reversed(range(self.tabV.rowCount())):
                    self.tabV.removeRow(i)
                    self.contar = 0
                if (query.execute("SELECT * FROM Recibo where Fecha BETWEEN '" + self.time1 + "' AND '" + self.time2 + "' ORDER BY Fecha ASC")):
                    for row in query:
                        self.tabV.insertRow(self.tabV.rowCount())
                        self.tabV.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                        self.tabV.setItem(self.contar, 1, QTableWidgetItem(str(row[1])))
                        self.tabV.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                        self.tabV.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                        self.tabV.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                        self.tabV.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                        self.tabV.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                        self.tabV.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                        self.tabV.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                        self.tabV.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                        self.tabV.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                        self.tabV.setItem(self.contar, 11, QTableWidgetItem(row[11]))
                        self.tabV.setItem(self.contar, 12, QTableWidgetItem(row[12]))
                        self.tabV.setItem(self.contar, 13, QTableWidgetItem(row[13]))
                        self.tabV.setItem(self.contar, 14, QTableWidgetItem(row[14]))
                        self.tabV.setItem(self.contar, 15, QTableWidgetItem(row[15]))
                        self.tabV.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                        self.tabV.setItem(self.contar, 17, QTableWidgetItem(row[17]))
                        self.tabV.setItem(self.contar, 18, QTableWidgetItem(row[18]))
                        self.tabV.setItem(self.contar, 19, QTableWidgetItem(row[19]))
                        self.tabV.setItem(self.contar, 20, QTableWidgetItem(row[20]))
                        self.tabV.setItem(self.contar, 21, QTableWidgetItem(row[21]))
                        self.tabV.setItem(self.contar, 22, QTableWidgetItem(row[22]))
                        self.tabV.setItem(self.contar, 23, QTableWidgetItem(str(row[23])))
                        self.tabV.setItem(self.contar, 24, QTableWidgetItem(str(row[24])))
                        self.contar += 1

                else:
                    QMessageBox.warning(
                        self, 'Warning', 'Problema al momento de mostrar la inforamcion', QMessageBox.Ok)
            except:
                QMessageBox.critical(
                    self, 'Fallo', 'Problema al conectar con la DB.', QMessageBox.Ok)
        except:
            QMessageBox.critical(
                self, 'Fallo', 'Hay problemas al mostrar al cuadro de lineas dinamicas', QMessageBox.Ok)

    def buscarBar(self):
        try:
            db2 = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
            query2 = db2.cursor()
            if (query2.execute("SELECT SUM(Total) AS suma FROM Recibo where Fecha BETWEEN '" + self.time1 + "' AND '" + self.time2 + "'")):
                for row2 in query2:
                    dinero = int(row2[0])

            db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
            query = db.cursor()
            if (query.execute("SELECT Fecha, Total FROM Recibo where Fecha BETWEEN '" + self.time1 + "' AND '" + self.time2 + "' ORDER BY Fecha ASC")):

                for r, row in enumerate(query, start=1):
                    x = [int(row[1])]

                    plt.axes((0.1, 0.3, 0.8, 0.6))
                    plt.bar(r, x)
                    plt.ylim(50, dinero)
                    plt.title('Sistema de ventas')
                    plt.ylabel('Dinero')
                plt.show()
                query = "SELECT Fecha, Total FROM Recibo where Fecha BETWEEN '" + \
                    self.time1 + "' AND '" + self.time2 + "' ORDER BY Fecha ASC"
                df = pandas.read_sql(query, db, index_col=['Fecha'])
                self.lineaG.setText((str(df)))

                try:
                    db = pymysql.connect(host="localhost", user="root",
                                         passwd="Lb104572", db="inventario")
                    query = db.cursor()
                    for i in reversed(range(self.tabV.rowCount())):
                        self.tabV.removeRow(i)
                        self.contar = 0
                    if (query.execute("SELECT * FROM Recibo where Fecha BETWEEN '" + self.time1 + "' AND '" + self.time2 + "' ORDER BY Fecha ASC")):
                        for row in query:
                            self.tabV.insertRow(self.tabV.rowCount())
                            self.tabV.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                            self.tabV.setItem(self.contar, 1, QTableWidgetItem(str(row[1])))
                            self.tabV.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                            self.tabV.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                            self.tabV.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                            self.tabV.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                            self.tabV.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                            self.tabV.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                            self.tabV.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                            self.tabV.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                            self.tabV.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                            self.tabV.setItem(self.contar, 11, QTableWidgetItem(row[11]))
                            self.tabV.setItem(self.contar, 12, QTableWidgetItem(row[12]))
                            self.tabV.setItem(self.contar, 13, QTableWidgetItem(row[13]))
                            self.tabV.setItem(self.contar, 14, QTableWidgetItem(row[14]))
                            self.tabV.setItem(self.contar, 15, QTableWidgetItem(row[15]))
                            self.tabV.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                            self.tabV.setItem(self.contar, 17, QTableWidgetItem(row[17]))
                            self.tabV.setItem(self.contar, 18, QTableWidgetItem(row[18]))
                            self.tabV.setItem(self.contar, 19, QTableWidgetItem(row[19]))
                            self.tabV.setItem(self.contar, 20, QTableWidgetItem(row[20]))
                            self.tabV.setItem(self.contar, 21, QTableWidgetItem(row[21]))
                            self.tabV.setItem(self.contar, 22, QTableWidgetItem(row[22]))
                            self.tabV.setItem(self.contar, 23, QTableWidgetItem(str(row[23])))
                            self.tabV.setItem(self.contar, 24, QTableWidgetItem(str(row[24])))
                            self.contar += 1

                    else:
                        QMessageBox.warning(
                            self, 'Warning', 'Problema al momento de mostrar la inforamcion', QMessageBox.Ok)
                except:
                    QMessageBox.critical(
                        self, 'Fallo', 'Problema al conectar con la DB.', QMessageBox.Ok)
        except:
            QMessageBox.warning(
                self, 'Warning', 'Hay problemas al mostar el cuadro de barras dinamicas.', QMessageBox.Ok)

    def generarExcel(self):
        db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
        query = db.cursor()
        if (query.execute("SELECT * FROM Recibo where Fecha BETWEEN '" + self.time1 + "' AND '" + self.time2 + "'")):
            db.commit()

        db2 = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
        query2 = db2.cursor()
        if (query2.execute("SELECT SUM(Total) AS suma FROM Recibo where Fecha BETWEEN '" + self.time1 + "' AND '" + self.time2 + "'")):
            db2.commit()

        workbook = xlsxwriter.Workbook("excel/" + self.time1 + "-" + self.time2 + ".xlsx")
        worksheet = workbook.add_worksheet()
        cyan = workbook.add_format({'bold': True, 'font_name': 'Arial',
                                    'font_color': 'white', 'fg_color': 'green', 'align': 'center', })
        green = workbook.add_format({'bold': True, 'font_name': 'Arial',
                                     'font_color': 'white', 'fg_color': 'green', 'align': 'center', })
        worksheet.write(0, 0, 'GUIA', cyan)
        worksheet.write(0, 1, 'N.VENTA', cyan)
        worksheet.write(0, 2, 'CLIENTE', cyan)
        worksheet.write(0, 3, 'M1', cyan)
        worksheet.write(0, 4, 'M2', cyan)
        worksheet.write(0, 5, 'M3', cyan)
        worksheet.write(0, 6, 'M4', cyan)
        worksheet.write(0, 7, 'M5', cyan)
        worksheet.write(0, 8, 'M6', cyan)
        worksheet.write(0, 9, 'M7', cyan)
        worksheet.write(0, 10, 'M8', cyan)
        worksheet.write(0, 11, 'M9', cyan)
        worksheet.write(0, 12, 'M10', cyan)
        worksheet.write(0, 13, 'M11', cyan)
        worksheet.write(0, 14, 'M12', cyan)
        worksheet.write(0, 15, 'M13', cyan)
        worksheet.write(0, 16, 'M14', cyan)
        worksheet.write(0, 17, 'M15', cyan)
        worksheet.write(0, 18, 'M16', cyan)
        worksheet.write(0, 19, 'M17', cyan)
        worksheet.write(0, 20, 'M18', cyan)
        worksheet.write(0, 21, 'M19', cyan)
        worksheet.write(0, 22, 'M20', cyan)
        worksheet.write(0, 23, 'PRESIO', cyan)
        worksheet.set_column(0, 25, 20)
        worksheet.write(0, 24, 'FECHA', cyan)
        worksheet.write(0, 25, 'TOTAL', green)
        for r, row in enumerate(query, start=1):
            worksheet.write(r, 0, int(row[0]))
            worksheet.write(r, 1, int(row[1]))
            worksheet.write(r, 2, str(row[2]))
            worksheet.write(r, 3, str(row[3]))
            worksheet.write(r, 4, str(row[4]))
            worksheet.write(r, 5, str(row[5]))
            worksheet.write(r, 6, str(row[6]))
            worksheet.write(r, 7, str(row[7]))
            worksheet.write(r, 8, str(row[8]))
            worksheet.write(r, 9, str(row[9]))
            worksheet.write(r, 10, str(row[10]))
            worksheet.write(r, 11, str(row[11]))
            worksheet.write(r, 12, str(row[12]))
            worksheet.write(r, 13, str(row[13]))
            worksheet.write(r, 14, str(row[14]))
            worksheet.write(r, 15, str(row[15]))
            worksheet.write(r, 16, str(row[16]))
            worksheet.write(r, 17, str(row[17]))
            worksheet.write(r, 18, str(row[18]))
            worksheet.write(r, 19, str(row[19]))
            worksheet.write(r, 20, str(row[20]))
            worksheet.write(r, 21, str(row[21]))
            worksheet.write(r, 22, str(row[22]))
            worksheet.write(r, 23, int(row[23]))
            worksheet.write(r, 24, str(row[24]))
            for row2 in query2:
                worksheet.write(1, 25, row2[0], green)
        workbook.close()
        try:
            x = os.path.abspath("excel")
            os.startfile(x+"/" + self.time1 + "-" + self.time2 + ".xlsx")
        except:
            os.system("localc excel/"+self.time1 + "-" + self.time2 + ".xlsx")
# ---------------------------------------------------------------------------------------
# Libro Auxiliar

    def libroControl(self):
        self.laFe = QLabel("Fecha:", self.page7)
        self.laFe.setGeometry(90, 20, 120, 13)

        self.liFe = QLineEdit(self.page7)
        self.liFe.setAlignment(Qt.AlignCenter)
        self.liFe.setPlaceholderText("dia/mes/año")
        self.liFe.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.liFe.setGeometry(60, 35, 100, 20)
        self.fecha = time.strftime("%d/%m/%y")
        self.liFe.setText(self.fecha)
        self.liFe.setDisabled(True)

        self.laGu = QLabel("Guia", self.page7)
        self.laGu.setGeometry(220, 20, 120, 13)

        self.liGu = QLineEdit(self.page7)
        self.liGu.setAlignment(Qt.AlignCenter)
        self.liGu.setPlaceholderText("Numero de guia")
        self.liGu.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.liGu.setGeometry(180, 35, 100, 20)
        self.liGu.textChanged.connect(self.validarGuia)

        self.laVn = QLabel("Codigo", self.page7)
        self.laVn.setGeometry(328, 17, 100, 15)

        self.liVn = QLineEdit(self.page7)
        self.liVn.setAlignment(Qt.AlignCenter)
        self.liVn.setPlaceholderText("NIT")
        self.liVn.setStyleSheet("color: #252485;  background: #f3f3f3; ")
        self.liVn.setGeometry(300, 35, 100, 20)
        self.liVn.textChanged.connect(self.validarVendedor)

        self.bo1 = QPushButton(self.page7)
        self.bo1.setGeometry(420, 26, 30, 30)
        self.bo1.setIcon(QIcon(QPixmap("icon/Insert_Row_Below-48.png")))
        self.bo1.setIconSize(QSize(30, 60))
        self.bo1.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bo1.setFlat(True)
        self.bo1.clicked.connect(self.validar_Formularios)

        self.bo2 = QPushButton(self.page7)
        self.bo2.setGeometry(940, 100, 30, 30)
        self.bo2.setIcon(QIcon(QPixmap("icon/Search-48.png")))
        self.bo2.setIconSize(QSize(30, 60))
        self.bo2.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bo2.setFlat(True)
        self.bo2.clicked.connect(self.buscar_Por_Guia)

        self.bo3 = QPushButton(self.page7)
        self.bo3.setGeometry(940, 140, 30, 30)
        self.bo3.setIcon(QIcon(QPixmap("icon/Edit_Row-48.png")))
        self.bo3.setIconSize(QSize(30, 60))
        self.bo3.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bo3.setFlat(True)
        self.bo3.clicked.connect(self.modificar_Guia)

        self.bo4 = QPushButton(self.page7)
        self.bo4.setGeometry(940, 180, 30, 30)
        self.bo4.setIcon(QIcon(QPixmap("icon/Delete_Row-48.png")))
        self.bo4.setIconSize(QSize(30, 60))
        self.bo4.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.bo4.setFlat(True)
        self.bo4.clicked.connect(self.eliminar_Guia)

        self.tab4 = QTableWidget(0, 24, self.page7)
        self.tab4.setGeometry(60, 100, 870, 400)
        self.tab4.setHorizontalHeaderLabels(['GUIA', 'VENDEDOR', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8',
                                             'M9', 'M10', 'M11', 'M12', 'M13', 'M14', 'M15', 'M16', 'M17', 'M18', 'M19', 'M20', 'TOTAL', 'FECHA'])
        self.tab4.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tab4.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tab4.horizontalHeader().setStretchLastSection(True)
        self.tab4.verticalHeader().setDefaultSectionSize(18)                 # Vertical height of rows
        self.tab4.setShowGrid(True)
        self.tab4.setDragDropOverwriteMode(True)
        self.tab4.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tab4.setStyleSheet(
            "QTableView {background-color: #CEECF5;selection-background-color: #FF0000;}")

        self.progress3 = QProgressBar(self.page7)
        self.progress3.setGeometry(60, 90, 870, 10)

        self.l = QLabel(self.page7)
        self.l.setGeometry(60, 73, 370, 20)

    def validarGuia(self):
        validar = re.match('^[0-9\sáéíóúàèìòùäëïöüñ]+$', self.liGu.text(), re.I)
        if self.liGu.text() == "":
            self.liGu.setStyleSheet("border: 2px solid yellow;")
            self.l.setText("")
            return False
        elif self.liGu.text() == "all":
            self.liGu.setStyleSheet("border: 2px solid green;")
            self.l.setText("")
            return False
        elif not validar:
            self.liGu.setStyleSheet("border: 2px solid red;")
            self.l.setText("La casilla de codigo de guia solo se permite numeros.")
            self.l.setStyleSheet("color: red")
            return False
        else:
            self.liGu.setStyleSheet("border: 2px solid cyan;")
            self.l.setText("")
            return True

    def validarVendedor(self):
        validar = re.match('^[0-9\sáéíóúàèìòùäëïöüñ]+$', self.liVn.text(), re.I)
        if self.liVn.text() == "":
            self.liVn.setStyleSheet("border: 2px solid yellow;")
            self.l.setText("")
            return False
        elif not validar:
            self.liVn.setStyleSheet("border: 2px solid red;")
            self.l.setText("La casilla de codigo del vendedor solo se permite numeros.")
            self.l.setStyleSheet("color: red")
            return False
        else:
            self.liVn.setStyleSheet("border: 2px solid cyan;")
            self.l.setText("")
            return True

    def validar_Formularios(self):
        guia = self.liGu.text()
        db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="admin")
        query = db.cursor()
        if (query.execute("SELECT Nombre, Apellido FROM Registros WHERE Codigo='" + self.liVn.text() + "'")):
            for row1 in query:
                nombre = row1[0] + " " + row1[1]
        try:
            db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
            query = db.cursor()
            if (query.execute("SELECT * FROM Recibo WHERE CodigoG='" + self.liGu.text() + "'")):
                for row2 in query:
                    M1 = row2[3]
                    M2 = row2[4]
                    M3 = row2[5]
                    M4 = row2[6]
                    M5 = row2[7]
                    M6 = row2[8]
                    M7 = row2[9]
                    M8 = row2[10]
                    M9 = row2[11]
                    M10 = row2[12]
                    M11 = row2[13]
                    M12 = row2[14]
                    M13 = row2[15]
                    M14 = row2[16]
                    M15 = row2[17]
                    M16 = row2[18]
                    M17 = row2[19]
                    M18 = row2[20]
                    M19 = row2[21]
                    M20 = row2[22]
                    total = str(row2[23])
                    fecha = str(row2[24])
        except:
            QMessageBox.critical(self, 'Fallo', 'Fallo al conectar con la DB.', QMessageBox.Ok)

        if self.validarGuia() and self.validarVendedor():
            try:
                db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="control")
                query = db.cursor()
                if (query.execute("""INSERT INTO Libro (CodigoG, Vendedor, M1, M2, M3, M4, M5, M6, M7, M8, M9, M10, M11, M12, M13, M14, M15, M16, M17, M18, M19, M20, Total, Fecha) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                                  (guia, nombre, M1, M2, M3, M4, M5, M6, M7, M8, M9, M10, M11, M12, M13, M14, M15, M16, M17, M18, M19, M20, total, fecha))):
                    db.commit()
                    QMessageBox.information(self, 'Informacion',
                                            'Registro Completo', QMessageBox.Ok)
                    self.liGu.setText("")
                    self.liVn.setText("")
                else:
                    self.activar = QMessageBox.warning(
                        self, 'Warning', 'Fallo al ingresar inforamcion.', QMessageBox.Ok)
            except:
                QMessageBox.critical(self, 'Fallo', 'Fallo al conectar con la DB.', QMessageBox.Ok)

            try:
                db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="control")
                query = db.cursor()
                if (query.execute("SELECT * FROM Libro")):
                    for i in reversed(range(self.tab4.rowCount())):
                        self.tab4.removeRow(i)
                        self.contar = 0
                    for contador in range(110):
                        self.progress3.setValue(contador)
                    for row in query:
                        self.tab4.insertRow(self.tab4.rowCount())
                        self.tab4.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                        self.tab4.setItem(self.contar, 1, QTableWidgetItem(row[1]))
                        self.tab4.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                        self.tab4.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                        self.tab4.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                        self.tab4.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                        self.tab4.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                        self.tab4.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                        self.tab4.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                        self.tab4.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                        self.tab4.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                        self.tab4.setItem(self.contar, 11, QTableWidgetItem(row[11]))
                        self.tab4.setItem(self.contar, 12, QTableWidgetItem(row[12]))
                        self.tab4.setItem(self.contar, 13, QTableWidgetItem(row[13]))
                        self.tab4.setItem(self.contar, 14, QTableWidgetItem(row[14]))
                        self.tab4.setItem(self.contar, 15, QTableWidgetItem(row[15]))
                        self.tab4.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                        self.tab4.setItem(self.contar, 17, QTableWidgetItem(row[17]))
                        self.tab4.setItem(self.contar, 18, QTableWidgetItem(row[18]))
                        self.tab4.setItem(self.contar, 19, QTableWidgetItem(row[19]))
                        self.tab4.setItem(self.contar, 20, QTableWidgetItem(row[20]))
                        self.tab4.setItem(self.contar, 21, QTableWidgetItem(row[21]))
                        self.tab4.setItem(self.contar, 22, QTableWidgetItem(str(row[22])))
                        self.tab4.setItem(self.contar, 23, QTableWidgetItem(row[23]))

                        self.contar += 1
                    self.progress3.setValue(0)
                else:
                    QMessageBox.warning(
                        self, 'Alerta', 'Fallo al mostrar la inforamcion.', QMessageBox.Ok)
            except:
                QMessageBox.critical(self, 'Fallo', 'Fallo al conectar con la DB.', QMessageBox.Ok)

    def buscar_Por_Guia(self):
        print(self.tab4.rowCount())
        if self.liGu.text() == "":
            QMessageBox.information(self, 'Informacion',
                                    'Se encuentra vacia la casilla de guia.', QMessageBox.Ok)
        elif self.liGu.text() == "all":
            try:
                db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="control")
                query = db.cursor()
                if (query.execute("SELECT * FROM Libro")):
                    for i in reversed(range(self.tab4.rowCount())):
                        self.tab4.removeRow(i)
                        self.contar = 0
                    contador = 0
                    while contador < 100:
                        contador += 0.01
                        self.progress3.setValue(contador)
                    for row in query:
                        self.tab4.insertRow(self.tab4.rowCount())
                        self.tab4.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                        self.tab4.setItem(self.contar, 1, QTableWidgetItem(row[1]))
                        self.tab4.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                        self.tab4.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                        self.tab4.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                        self.tab4.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                        self.tab4.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                        self.tab4.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                        self.tab4.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                        self.tab4.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                        self.tab4.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                        self.tab4.setItem(self.contar, 11, QTableWidgetItem(row[11]))
                        self.tab4.setItem(self.contar, 12, QTableWidgetItem(row[12]))
                        self.tab4.setItem(self.contar, 13, QTableWidgetItem(row[13]))
                        self.tab4.setItem(self.contar, 14, QTableWidgetItem(row[14]))
                        self.tab4.setItem(self.contar, 15, QTableWidgetItem(row[15]))
                        self.tab4.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                        self.tab4.setItem(self.contar, 17, QTableWidgetItem(row[17]))
                        self.tab4.setItem(self.contar, 18, QTableWidgetItem(row[18]))
                        self.tab4.setItem(self.contar, 19, QTableWidgetItem(row[19]))
                        self.tab4.setItem(self.contar, 20, QTableWidgetItem(row[20]))
                        self.tab4.setItem(self.contar, 21, QTableWidgetItem(row[21]))
                        self.tab4.setItem(self.contar, 22, QTableWidgetItem(str(row[22])))
                        self.tab4.setItem(self.contar, 23, QTableWidgetItem(str(row[23])))
                        self.contar += 1
                    self.progress3.setValue(0)
                else:
                    QMessageBox.warning(
                        self, 'Warning', 'Codigo de guia no existe.', QMessageBox.Ok)
            except:
                QMessageBox.critical(self, 'Fallo', 'Fallo al conectar con la DB', QMessageBox.Ok)

        elif self.liGu.text() == self.liGu.text():
            try:
                db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="control")
                query = db.cursor()
                if (query.execute("SELECT * FROM Libro WHERE  CodigoG = '" + self.liGu.text() + "'")):
                    for i in reversed(range(self.tab4.rowCount())):
                        self.tab4.removeRow(i)
                        self.contar = 0
                    contador = 0
                    while contador < 100:
                        contador += 0.01
                        self.progress3.setValue(contador)
                    for row in query:
                        self.tab4.insertRow(self.tab4.rowCount())
                        self.tab4.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                        self.tab4.setItem(self.contar, 1, QTableWidgetItem(row[1]))
                        self.tab4.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                        self.tab4.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                        self.tab4.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                        self.tab4.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                        self.tab4.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                        self.tab4.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                        self.tab4.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                        self.tab4.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                        self.tab4.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                        self.tab4.setItem(self.contar, 11, QTableWidgetItem(row[11]))
                        self.tab4.setItem(self.contar, 12, QTableWidgetItem(row[12]))
                        self.tab4.setItem(self.contar, 13, QTableWidgetItem(row[13]))
                        self.tab4.setItem(self.contar, 14, QTableWidgetItem(row[14]))
                        self.tab4.setItem(self.contar, 15, QTableWidgetItem(row[15]))
                        self.tab4.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                        self.tab4.setItem(self.contar, 17, QTableWidgetItem(row[17]))
                        self.tab4.setItem(self.contar, 18, QTableWidgetItem(row[18]))
                        self.tab4.setItem(self.contar, 19, QTableWidgetItem(row[19]))
                        self.tab4.setItem(self.contar, 20, QTableWidgetItem(row[20]))
                        self.tab4.setItem(self.contar, 21, QTableWidgetItem(row[21]))
                        self.tab4.setItem(self.contar, 22, QTableWidgetItem(str(row[22])))
                        self.tab4.setItem(self.contar, 23, QTableWidgetItem(row[23]))

                        self.contar += 1
                    self.progress3.setValue(0)
                else:
                    QMessageBox.warning(
                        self, 'Warning', 'Codigo de guia no existe a un.', QMessageBox.Ok)
            except:
                QMessageBox.critical(self, 'Fallo', 'Fallo al conectar con la DB', QMessageBox.Ok)
        else:
            QMessageBox.warning(self, 'Warning', 'El codigo de guia no existe.', QMessageBox.Ok)

    def modificar_Guia(self):
        print(self.tab4.rowCount())
        if self.tab4.rowCount() > 2:
            row = self.tab4.currentRow()
            column = self.tab4.currentColumn()
            codigo = self.tab4.item(row, 0).text()
            valor = self.tab4.currentItem().text()
            columnas = ['CodigoG', 'Vendedor', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10',
                        'M11', 'M12', 'M13', 'M14', 'M15', 'M16', 'M17', 'M18', 'M19', 'M20', 'Total', 'Fecha']
            if codigo == 0:
                QMessageBox.warning(
                    self, 'Warning', 'El codigo de Guia no es modificable.', QMessageBox.Ok)
            else:
                try:
                    db = pymysql.connect(host="localhost", user="root",
                                         passwd="Lb104572", db="control")
                    query = db.cursor()
                    if (query.execute("UPDATE Libro SET " + columnas[column] + " = '" + valor + "' WHERE CodigoG='" + codigo + "'")):
                        db.commit()
                        QMessageBox.information(self, 'Informacion',
                                                'Se ha modificado correctamente.', QMessageBox.Ok)
                    else:
                        QMessageBox.warning(
                            self, 'Warning', 'Error al momento de modificar la columna.', QMessageBox.Ok)
                except:
                    QMessageBox.critical(self, 'Fallo', 'Error en la DB.', QMessageBox.Ok)

                try:
                    db = pymysql.connect(host="localhost", user="root",
                                         passwd="Lb104572", db="control")
                    query = db.cursor()
                    if (query.execute("SELECT * FROM Libro")):
                        for i in reversed(range(self.tab4.rowCount())):
                            self.tab4.removeRow(i)
                            self.contar = 0
                        contador = 0
                        while contador < 100:
                            contador += 0.01
                            self.progress3.setValue(contador)
                        for row in query:
                            self.tab4.insertRow(self.tab4.rowCount())
                            self.tab4.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                            self.tab4.setItem(self.contar, 1, QTableWidgetItem(row[1]))
                            self.tab4.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                            self.tab4.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                            self.tab4.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                            self.tab4.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                            self.tab4.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                            self.tab4.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                            self.tab4.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                            self.tab4.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                            self.tab4.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                            self.tab4.setItem(self.contar, 11, QTableWidgetItem(row[11]))
                            self.tab4.setItem(self.contar, 12, QTableWidgetItem(row[12]))
                            self.tab4.setItem(self.contar, 13, QTableWidgetItem(row[13]))
                            self.tab4.setItem(self.contar, 14, QTableWidgetItem(row[14]))
                            self.tab4.setItem(self.contar, 15, QTableWidgetItem(row[15]))
                            self.tab4.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                            self.tab4.setItem(self.contar, 17, QTableWidgetItem(row[17]))
                            self.tab4.setItem(self.contar, 18, QTableWidgetItem(row[18]))
                            self.tab4.setItem(self.contar, 19, QTableWidgetItem(row[19]))
                            self.tab4.setItem(self.contar, 20, QTableWidgetItem(row[20]))
                            self.tab4.setItem(self.contar, 21, QTableWidgetItem(row[21]))
                            self.tab4.setItem(self.contar, 22, QTableWidgetItem(str(row[22])))
                            self.tab4.setItem(self.contar, 23, QTableWidgetItem(row[23]))

                            self.contar += 1
                        self.progress3.setValue(0)

                    else:
                        QMessageBox.warning(self, 'Alerta', 'Problema', QMessageBox.Ok)
                except:
                    QMessageBox.critical(
                        self, 'Fallo', 'Fallo al conectar con la DB', QMessageBox.Ok)
        else:
            row = self.tab4.currentRow()
            column = self.tab4.currentColumn()
            codigo = self.tab4.item(row, 0).text()
            valor = self.tab4.currentItem().text()
            print(row)
            columnas = ['''CodigoG', 'Vendedor', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11', 'M12', 'M13', 'M14', 'M15', 'M16', 'M17', 'M18', 'M19', 'M20', 'Total', 'Fecha''']
            if codigo == 0:
                QMessageBox.warning(
                    self, 'Warning', 'El codigo de Guia no es modificable.', QMessageBox.Ok)
            else:
                try:
                    db = pymysql.connect(host="localhost", user="root",
                                         passwd="Lb104572", db="control")
                    query = db.cursor()
                    if (query.execute("UPDATE Libro SET " + columnas[column] + " = '" + valor + "' WHERE CodigoG='" + codigo + "'")):
                        db.commit()
                        QMessageBox.information(self, 'Informacion',
                                                'Se ha modificado correctamente', QMessageBox.Ok)
                    else:
                        QMessageBox.warning(
                            self, 'Warning', 'Error al momento modificarlo', QMessageBox.Ok)
                except:
                    QMessageBox.critical(
                        self, 'Fallo', 'Error al conectar con la DB.', QMessageBox.Ok)

    def eliminar_Guia(self):
        if self.tab4.rowCount() == 2:
            row = self.tab4.currentRow()
            codigo = self.tab4.item(row, 0).text()
            try:
                db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="control")
                query = db.cursor()
                if (query.execute("DELETE FROM Libro WHERE CodigoG= '" + codigo + "'")):
                    db.commit()
                    QMessageBox.information(self, 'Informacion',
                                            'Se elimino correctamente', QMessageBox.Ok)
                else:
                    QMessageBox.warning(
                        self, 'Fallo', 'Error al momento de eliminar el codigo: ' + codigo, QMessageBox.Ok)
            except:
                QMessageBox.critical(self, 'Fallo', 'Error en la DB.: ', QMessageBox.Ok)
            try:
                db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="control")
                query = db.cursor()
                if (query.execute("SELECT * FROM Libro")):
                    for i in reversed(range(self.tab4.rowCount())):
                        self.tab4.removeRow(i)
                        self.contar = 0
                    contador = 0
                    while contador < 100:
                        contador += 0.01
                        self.progress3.setValue(contador)
                    for row in query:
                        self.tab4.insertRow(self.tab4.rowCount())
                        self.tab4.setItem(self.contar, 0, QTableWidgetItem(str(row[0])))
                        self.tab4.setItem(self.contar, 1, QTableWidgetItem(row[1]))
                        self.tab4.setItem(self.contar, 2, QTableWidgetItem(row[2]))
                        self.tab4.setItem(self.contar, 3, QTableWidgetItem(row[3]))
                        self.tab4.setItem(self.contar, 4, QTableWidgetItem(row[4]))
                        self.tab4.setItem(self.contar, 5, QTableWidgetItem(row[5]))
                        self.tab4.setItem(self.contar, 6, QTableWidgetItem(row[6]))
                        self.tab4.setItem(self.contar, 7, QTableWidgetItem(row[7]))
                        self.tab4.setItem(self.contar, 8, QTableWidgetItem(row[8]))
                        self.tab4.setItem(self.contar, 9, QTableWidgetItem(row[9]))
                        self.tab4.setItem(self.contar, 10, QTableWidgetItem(row[10]))
                        self.tab4.setItem(self.contar, 11, QTableWidgetItem(row[11]))
                        self.tab4.setItem(self.contar, 12, QTableWidgetItem(row[12]))
                        self.tab4.setItem(self.contar, 13, QTableWidgetItem(row[13]))
                        self.tab4.setItem(self.contar, 14, QTableWidgetItem(row[14]))
                        self.tab4.setItem(self.contar, 15, QTableWidgetItem(row[15]))
                        self.tab4.setItem(self.contar, 16, QTableWidgetItem(row[16]))
                        self.tab4.setItem(self.contar, 17, QTableWidgetItem(row[17]))
                        self.tab4.setItem(self.contar, 18, QTableWidgetItem(row[18]))
                        self.tab4.setItem(self.contar, 19, QTableWidgetItem(row[19]))
                        self.tab4.setItem(self.contar, 20, QTableWidgetItem(row[20]))
                        self.tab4.setItem(self.contar, 21, QTableWidgetItem(row[21]))
                        self.tab4.setItem(self.contar, 22, QTableWidgetItem(str(row[22])))
                        self.tab4.setItem(self.contar, 23, QTableWidgetItem(row[23]))

                        self.contar += 1
                    self.progress3.setValue(0)

                else:
                    QMessageBox.warning(self, 'Alerta', 'Problema', QMessageBox.Ok)
            except:
                QMessageBox.critical(self, 'Fallo', 'Fallo al conectar con la DB', QMessageBox.Ok)
        else:
            row = self.tab4.currentRow()
            codigo = self.tab4.item(row, 0).text()
            try:
                db = pymysql.connect(host="localhost", user="root", passwd="Lb104572", db="control")
                query = db.cursor()
                if (query.execute("DELETE FROM Libro WHERE CodigoG= '" + codigo + "'")):
                    db.commit()
                    QMessageBox.information(self, 'Informacion',
                                            'Se elimino correctamente', QMessageBox.Ok)
                else:
                    QMessageBox.warning(
                        self, 'Fallo', 'Error al momento de Eliminar el codigo: ' + codigo, QMessageBox.Ok)

            except:
                QMessageBox.critical(
                    self, 'Fallo', 'Error al momento de Eliminar el codigo: ' + codigo, QMessageBox.Ok)
            for i in reversed(range(self.tab4.rowCount())):
                self.tab4.removeRow(i)
                self.contar = 0
# ---------------------------------------------------------------------------------------
# Registrar Usuario

    def registerU(self):
        vru = registroUSER.RUVentana(self)
        vru.show()
# ---------------------------------------------------------------------------------------
# StackWidget

    def stackwidgetPRO(self):
        self.stackedWidget = QStackedWidget(self)
        self.stackedWidget.setGeometry(QRect(40, 30, 1024, 640))

        self.page = QWidget()
        self.stackedWidget.addWidget(self.page)

        self.page2 = QWidget()
        self.stackedWidget.addWidget(self.page2)

        self.page3 = QWidget()
        self.stackedWidget.addWidget(self.page3)

        self.page4 = QWidget()
        self.stackedWidget.addWidget(self.page4)

        self.page5 = QWidget()
        self.stackedWidget.addWidget(self.page5)

        self.page6 = QWidget()
        self.stackedWidget.addWidget(self.page6)

        self.page7 = QWidget()
        self.stackedWidget.addWidget(self.page7)

    def pagina1(self):
        self.stackedWidget.setCurrentIndex(0)
        QMetaObject.connectSlotsByName(self)
        self.setWindowTitle("Sistema de ventas")
        pass

    def pagina2(self):
        self.stackedWidget.setCurrentIndex(1)
        QMetaObject.connectSlotsByName(self)
        self.setWindowTitle("Informacion de medicamentos")
        pass

    def pagina3(self):
        self.stackedWidget.setCurrentIndex(2)
        QMetaObject.connectSlotsByName(self)
        self.setWindowTitle("Resivo")
        pass

    def pagina4(self):
        self.stackedWidget.setCurrentIndex(3)
        QMetaObject.connectSlotsByName(self)
        self.setWindowTitle("Mayorista")
        pass

    def pagina5(self):
        self.stackedWidget.setCurrentIndex(4)
        QMetaObject.connectSlotsByName(self)
        self.setWindowTitle("Inventario")
        pass

    def pagina6(self):
        self.stackedWidget.setCurrentIndex(5)
        QMetaObject.connectSlotsByName(self)
        self.setWindowTitle("Diagrama de ventas")
        pass

    def pagina7(self):
        self.stackedWidget.setCurrentIndex(6)
        QMetaObject.connectSlotsByName(self)
        self.setWindowTitle("Libro de Control")
        pass
# ---------------------------------------------------------------------------------------
# Desconectar

    def desconectar(self):
        self.close()
        v2 = login.VLogin(self)
        v2.show()
# ---------------------------------------------------------------------------------------


if __name__ == "__main__":
    app = QApplication(sys.argv)
    x = Ventana()
    x.show()
    sys.exit(app.exec_())
